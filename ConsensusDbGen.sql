
USE [Concensus]
GO
/****** Object:  Table [dbo].[BodyParts]    Script Date: 3/28/2016 8:07:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BodyParts](
	[BodyPartId] [bigint] IDENTITY(1,1) NOT NULL,
	[ScientificName] [nvarchar](max) NOT NULL,
	[CommonName] [nvarchar](max) NOT NULL,
	[CultureName] [nvarchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[BodyPartId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CaseRankings]    Script Date: 3/28/2016 8:07:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CaseRankings](
	[CaseRankingId] [uniqueidentifier] NOT NULL,
	[CreatorId] [bigint] NOT NULL,
	[MedicalCaseId] [uniqueidentifier] NOT NULL,
	[UserGameId] [uniqueidentifier] NOT NULL,
	[Rating] [decimal](6, 2) NOT NULL,
	[WeightModifier] [decimal](6, 2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[CaseRankingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DemographicInformation]    Script Date: 3/28/2016 8:07:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DemographicInformation](
	[DemographicInformationId] [uniqueidentifier] NOT NULL,
	[DemographicType] [int] NOT NULL,
	[Weight] [decimal](6, 2) NOT NULL,
	[PatientId] [uniqueidentifier] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[DemographicInformationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DemographicInformationAudit]    Script Date: 3/28/2016 8:07:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DemographicInformationAudit](
	[DemographicInformationAuditId] [uniqueidentifier] NOT NULL,
	[DemographicInformationId] [uniqueidentifier] NOT NULL,
	[DemographicType] [int] NOT NULL,
	[LastModified] [datetime] NOT NULL,
	[Weight] [decimal](6, 2) NOT NULL,
	[PatientId] [uniqueidentifier] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[DemographicInformationAuditId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Diseases]    Script Date: 3/28/2016 8:07:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Diseases](
	[DiseaseId] [bigint] IDENTITY(1,1) NOT NULL,
	[CommonName] [nvarchar](max) NULL,
	[ScientificName] [nvarchar](max) NULL,
	[CultureName] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[DiseaseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DiseasesToMedicalTests]    Script Date: 3/28/2016 8:07:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DiseasesToMedicalTests](
	[DiseasesToMedicalTestsId] [uniqueidentifier] NOT NULL,
	[MedicalTestId] [bigint] NOT NULL,
	[DiseaseId] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[DiseasesToMedicalTestsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DiseasesToTestResults]    Script Date: 3/28/2016 8:07:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DiseasesToTestResults](
	[DiseasesToTestResultsId] [uniqueidentifier] NOT NULL,
	[TestResultId] [bigint] NOT NULL,
	[DiseaseId] [bigint] NOT NULL,
	[RandomAccuracyModifier] [decimal](6, 2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[DiseasesToTestResultsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Media]    Script Date: 3/28/2016 8:07:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Media](
	[MediaId] [bigint] IDENTITY(1,1) NOT NULL,
	[FileName] [nvarchar](1000) NULL,
	[FilePath] [nvarchar](max) NULL,
	[FileType] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[MediaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MediaToBodyParts]    Script Date: 3/28/2016 8:07:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MediaToBodyParts](
	[MediaToBodyPartsId] [uniqueidentifier] NOT NULL,
	[BodyPartId] [bigint] NOT NULL,
	[MediaId] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MediaToBodyPartsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MediaToUsers]    Script Date: 3/28/2016 8:07:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MediaToUsers](
	[MediaToUsers] [bigint] IDENTITY(1,1) NOT NULL,
	[MediaId] [bigint] NOT NULL,
	[UserId] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MediaToUsers] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MedicalCases]    Script Date: 3/28/2016 8:07:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MedicalCases](
	[MedicalCaseId] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[Description] [nvarchar](max) NULL,
	[CaseName] [nvarchar](max) NOT NULL,
	[CultureName] [nvarchar](max) NOT NULL,
	[CreatedDate] [datetime] NOT NULL DEFAULT (getdate()),
	[PatientId] [uniqueidentifier] NOT NULL,
	[CreatorId] [bigint] NOT NULL,
	[DiseaseId] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MedicalCaseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MedicalCasesToBodyParts]    Script Date: 3/28/2016 8:07:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MedicalCasesToBodyParts](
	[MedicalCasesToBodyPartsId] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[MedicalCaseId] [uniqueidentifier] NOT NULL,
	[BodyPartId] [bigint] NOT NULL,
	[Accuracy] [decimal](6, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[MedicalCasesToBodyPartsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MedicalCasesToMedicalTests]    Script Date: 3/28/2016 8:07:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MedicalCasesToMedicalTests](
	[MedicalCasesToMedicalTestsId] [uniqueidentifier] NOT NULL,
	[MedicalCaseId] [uniqueidentifier] NOT NULL,
	[MedicalTestId] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MedicalCasesToMedicalTestsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MedicalCasesToSymptoms]    Script Date: 3/28/2016 8:07:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MedicalCasesToSymptoms](
	[MedicalCasesToSymptomsId] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[MedicalCaseId] [uniqueidentifier] NOT NULL,
	[SymptomId] [bigint] NOT NULL,
	[Accuracy] [decimal](6, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[MedicalCasesToSymptomsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MedicalTests]    Script Date: 3/28/2016 8:07:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MedicalTests](
	[MedicalTestId] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[AffectiveModifier] [decimal](6, 2) NOT NULL DEFAULT ((1)),
PRIMARY KEY CLUSTERED 
(
	[MedicalTestId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Patients]    Script Date: 3/28/2016 8:07:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Patients](
	[PatientId] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[FirstName] [nvarchar](max) NOT NULL,
	[MiddleName] [nvarchar](max) NULL,
	[LastName] [nvarchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PatientId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Symptoms]    Script Date: 3/28/2016 8:07:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Symptoms](
	[SymptomId] [bigint] IDENTITY(1,1) NOT NULL,
	[CommonName] [nvarchar](max) NULL,
	[ScientificName] [nvarchar](max) NULL,
	[CultureName] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[SymptomId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TestResults]    Script Date: 3/28/2016 8:07:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TestResults](
	[TestResultId] [bigint] IDENTITY(1,1) NOT NULL,
	[Accuracy] [decimal](6, 2) NOT NULL,
	[MedicalTestId] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[TestResultId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserGames]    Script Date: 3/28/2016 8:07:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserGames](
	[UserGameId] [uniqueidentifier] NOT NULL DEFAULT (newid()),
	[PlayerId] [bigint] NOT NULL,
	[GameLevel] [int] NOT NULL DEFAULT ((1)),
	[GameName] [nvarchar](max) NOT NULL,
	[Score] [decimal](6, 2) NULL,
	[IsFinished] [bit] NOT NULL DEFAULT ((0)),
	[MedicalCaseId] [uniqueidentifier] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserGameId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserGamesToMedicalTests]    Script Date: 3/28/2016 8:07:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserGamesToMedicalTests](
	[UserGamesToMedicalTestsId] [uniqueidentifier] NOT NULL,
	[UserGameId] [uniqueidentifier] NOT NULL,
	[MedicalTestId] [bigint] NOT NULL,
	[TestResultId] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserGamesToMedicalTestsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserRankings]    Script Date: 3/28/2016 8:07:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRankings](
	[UserRankingId] [uniqueidentifier] NOT NULL,
	[UserRankingValue] [decimal](6, 2) NOT NULL,
	[UserRankedId] [bigint] NOT NULL,
	[CreatorId] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[UserRankingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 3/28/2016 8:07:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[UserId] [bigint] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](max) NOT NULL,
	[Password] [nvarchar](max) NOT NULL,
	[SecurityQuestion] [nvarchar](max) NOT NULL,
	[SecurityAnswer] [nvarchar](max) NULL,
	[UserLevel] [int] NOT NULL DEFAULT ((0)),
	[CultureName] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[CaseRankings] ADD  DEFAULT (newid()) FOR [CaseRankingId]
GO
ALTER TABLE [dbo].[DemographicInformation] ADD  DEFAULT (newid()) FOR [DemographicInformationId]
GO
ALTER TABLE [dbo].[DemographicInformation] ADD  DEFAULT ((3)) FOR [Weight]
GO
ALTER TABLE [dbo].[DemographicInformationAudit] ADD  DEFAULT (newid()) FOR [DemographicInformationAuditId]
GO
ALTER TABLE [dbo].[DemographicInformationAudit] ADD  DEFAULT (getdate()) FOR [LastModified]
GO
ALTER TABLE [dbo].[DemographicInformationAudit] ADD  DEFAULT ((3)) FOR [Weight]
GO
ALTER TABLE [dbo].[DiseasesToMedicalTests] ADD  DEFAULT (newid()) FOR [DiseasesToMedicalTestsId]
GO
ALTER TABLE [dbo].[DiseasesToTestResults] ADD  DEFAULT (newid()) FOR [DiseasesToTestResultsId]
GO
ALTER TABLE [dbo].[DiseasesToTestResults] ADD  DEFAULT ((1)) FOR [RandomAccuracyModifier]
GO
ALTER TABLE [dbo].[MediaToBodyParts] ADD  DEFAULT (newid()) FOR [MediaToBodyPartsId]
GO
ALTER TABLE [dbo].[MedicalCasesToMedicalTests] ADD  DEFAULT (newid()) FOR [MedicalCasesToMedicalTestsId]
GO
ALTER TABLE [dbo].[TestResults] ADD  DEFAULT ((1)) FOR [Accuracy]
GO
ALTER TABLE [dbo].[UserGamesToMedicalTests] ADD  DEFAULT (newid()) FOR [UserGamesToMedicalTestsId]
GO
ALTER TABLE [dbo].[UserRankings] ADD  DEFAULT (newid()) FOR [UserRankingId]
GO
ALTER TABLE [dbo].[CaseRankings]  WITH CHECK ADD FOREIGN KEY([CreatorId])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[CaseRankings]  WITH CHECK ADD FOREIGN KEY([MedicalCaseId])
REFERENCES [dbo].[MedicalCases] ([MedicalCaseId])
GO
ALTER TABLE [dbo].[CaseRankings]  WITH CHECK ADD FOREIGN KEY([UserGameId])
REFERENCES [dbo].[UserGames] ([UserGameId])
GO
ALTER TABLE [dbo].[DemographicInformation]  WITH CHECK ADD FOREIGN KEY([PatientId])
REFERENCES [dbo].[Patients] ([PatientId])
GO
ALTER TABLE [dbo].[DemographicInformationAudit]  WITH CHECK ADD FOREIGN KEY([PatientId])
REFERENCES [dbo].[Patients] ([PatientId])
GO
ALTER TABLE [dbo].[DiseasesToMedicalTests]  WITH CHECK ADD FOREIGN KEY([DiseaseId])
REFERENCES [dbo].[Diseases] ([DiseaseId])
GO
ALTER TABLE [dbo].[DiseasesToMedicalTests]  WITH CHECK ADD FOREIGN KEY([MedicalTestId])
REFERENCES [dbo].[MedicalTests] ([MedicalTestId])
GO
ALTER TABLE [dbo].[DiseasesToTestResults]  WITH CHECK ADD FOREIGN KEY([DiseaseId])
REFERENCES [dbo].[Diseases] ([DiseaseId])
GO
ALTER TABLE [dbo].[DiseasesToTestResults]  WITH CHECK ADD FOREIGN KEY([TestResultId])
REFERENCES [dbo].[TestResults] ([TestResultId])
GO
ALTER TABLE [dbo].[MediaToBodyParts]  WITH CHECK ADD FOREIGN KEY([BodyPartId])
REFERENCES [dbo].[BodyParts] ([BodyPartId])
GO
ALTER TABLE [dbo].[MediaToBodyParts]  WITH CHECK ADD FOREIGN KEY([MediaId])
REFERENCES [dbo].[Media] ([MediaId])
GO
ALTER TABLE [dbo].[MediaToUsers]  WITH CHECK ADD FOREIGN KEY([MediaId])
REFERENCES [dbo].[Media] ([MediaId])
GO
ALTER TABLE [dbo].[MediaToUsers]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[MedicalCases]  WITH CHECK ADD FOREIGN KEY([CreatorId])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[MedicalCases]  WITH CHECK ADD FOREIGN KEY([DiseaseId])
REFERENCES [dbo].[Diseases] ([DiseaseId])
GO
ALTER TABLE [dbo].[MedicalCases]  WITH CHECK ADD FOREIGN KEY([PatientId])
REFERENCES [dbo].[Patients] ([PatientId])
GO
ALTER TABLE [dbo].[MedicalCasesToBodyParts]  WITH CHECK ADD FOREIGN KEY([BodyPartId])
REFERENCES [dbo].[BodyParts] ([BodyPartId])
GO
ALTER TABLE [dbo].[MedicalCasesToBodyParts]  WITH CHECK ADD FOREIGN KEY([MedicalCaseId])
REFERENCES [dbo].[MedicalCases] ([MedicalCaseId])
GO
ALTER TABLE [dbo].[MedicalCasesToMedicalTests]  WITH CHECK ADD FOREIGN KEY([MedicalTestId])
REFERENCES [dbo].[MedicalTests] ([MedicalTestId])
GO
ALTER TABLE [dbo].[MedicalCasesToMedicalTests]  WITH CHECK ADD FOREIGN KEY([MedicalCaseId])
REFERENCES [dbo].[MedicalCases] ([MedicalCaseId])
GO
ALTER TABLE [dbo].[MedicalCasesToSymptoms]  WITH CHECK ADD FOREIGN KEY([MedicalCaseId])
REFERENCES [dbo].[MedicalCases] ([MedicalCaseId])
GO
ALTER TABLE [dbo].[MedicalCasesToSymptoms]  WITH CHECK ADD FOREIGN KEY([SymptomId])
REFERENCES [dbo].[Symptoms] ([SymptomId])
GO
ALTER TABLE [dbo].[TestResults]  WITH CHECK ADD FOREIGN KEY([MedicalTestId])
REFERENCES [dbo].[MedicalTests] ([MedicalTestId])
GO
ALTER TABLE [dbo].[UserGames]  WITH CHECK ADD FOREIGN KEY([MedicalCaseId])
REFERENCES [dbo].[MedicalCases] ([MedicalCaseId])
GO
ALTER TABLE [dbo].[UserGames]  WITH CHECK ADD FOREIGN KEY([PlayerId])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[UserGamesToMedicalTests]  WITH CHECK ADD FOREIGN KEY([MedicalTestId])
REFERENCES [dbo].[MedicalTests] ([MedicalTestId])
GO
ALTER TABLE [dbo].[UserGamesToMedicalTests]  WITH CHECK ADD FOREIGN KEY([TestResultId])
REFERENCES [dbo].[TestResults] ([TestResultId])
GO
ALTER TABLE [dbo].[UserGamesToMedicalTests]  WITH CHECK ADD FOREIGN KEY([UserGameId])
REFERENCES [dbo].[UserGames] ([UserGameId])
GO
ALTER TABLE [dbo].[UserRankings]  WITH CHECK ADD FOREIGN KEY([CreatorId])
REFERENCES [dbo].[Users] ([UserId])
GO
ALTER TABLE [dbo].[UserRankings]  WITH CHECK ADD FOREIGN KEY([UserRankedId])
REFERENCES [dbo].[Users] ([UserId])
GO
USE [master]
GO
ALTER DATABASE [Concensus] SET  READ_WRITE 
GO
