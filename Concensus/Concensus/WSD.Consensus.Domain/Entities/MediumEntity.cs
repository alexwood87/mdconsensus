
namespace WSD.Consensus.Domain.Entities
{
    using System.Collections.Generic;
    
    public class MediumEntity
    {
        public MediumEntity()
        {

        }
    
        public long Id { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public int? FileType { get; set; }
    
        public virtual ICollection<MediaToBodyPartEntity> MediaToBodyParts { get; set; }
        public virtual ICollection<MediaToUserEntity> MediaToUsers { get; set; }
    }
}
