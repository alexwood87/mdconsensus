
namespace WSD.Consensus.Domain.Entities
{
    using System;
    
    public partial class MedicalCasesToBodyPartEntity
    {
        public Guid Id { get; set; }
        public Guid MedicalCaseId { get; set; }
        public long BodyPartId { get; set; }
        public decimal? Accuracy { get; set; }
    
        public virtual BodyPartEntity BodyPart { get; set; }
        public virtual MedicalCaseEntity MedicalCase { get; set; }
    }
}
