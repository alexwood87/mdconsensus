
namespace WSD.Consensus.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    
    public class MedicalTestEntity
    {
        public MedicalTestEntity()
        {
        
        }
    
        public long Id { get; set; }
        public string Name { get; set; }
        public decimal AffectiveModifier { get; set; }
    
        public virtual ICollection<DiseasesToMedicalTestEntity> DiseasesToMedicalTests { get; set; }
        public virtual ICollection<MedicalCasesToMedicalTestEntity> MedicalCasesToMedicalTests { get; set; }
        public virtual ICollection<TestResultEntity> TestResults { get; set; }
        public virtual ICollection<UserGamesToMedicalTestEntity> UserGamesToMedicalTests { get; set; }
    }
}
