
namespace WSD.Consensus.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    
    public class TestResultEntity
    {
        public TestResultEntity()
        {
     
        }
    
        public long Id { get; set; }
        public decimal Accuracy { get; set; }
        public long? MedicalTestId { get; set; }
    
        public virtual ICollection<DiseasesToTestResultEntity> DiseasesToTestResults { get; set; }
        public virtual MedicalTestEntity MedicalTest { get; set; }
        public virtual ICollection<UserGamesToMedicalTestEntity> UserGamesToMedicalTests { get; set; }
    }
}
