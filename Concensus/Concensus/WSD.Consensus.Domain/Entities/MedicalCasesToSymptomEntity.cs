
using System;

namespace WSD.Consensus.Domain.Entities
{  
    public class MedicalCasesToSymptomEntity
    {
        public Guid Id { get; set; }
        public Guid MedicalCaseId { get; set; }
        public long SymptomId { get; set; }
        public decimal? Accuracy { get; set; }
    
        public virtual MedicalCaseEntity MedicalCase { get; set; }
        public virtual SymptomEntity Symptom { get; set; }
    }
}
