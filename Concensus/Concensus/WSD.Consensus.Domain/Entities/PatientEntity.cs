
namespace WSD.Consensus.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    
    public class PatientEntity
    {
        public PatientEntity()
        {

        }
    
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
    
        public virtual ICollection<DemographicInformationEntity> DemographicInformations { get; set; }
        public virtual ICollection<MedicalCaseEntity> MedicalCases { get; set; }
    }
}
