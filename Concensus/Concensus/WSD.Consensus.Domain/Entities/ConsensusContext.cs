﻿
namespace WSD.Consensus.Domain.Entities
{

    using Microsoft.EntityFrameworkCore;
    
    public class ConsensusContext : DbContext
    { 
        public ConsensusContext(DbContextOptions options) : base(options)
        {
           
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BodyPartEntity>().ForSqlServerHasIndex(s => s.Id).ForSqlServerIsClustered();
            modelBuilder.Entity<BodyPartEntity>().HasMany(s => s.MediaToBodyParts).WithOne(s => s.BodyPart)
                .HasForeignKey(s => s.BodyPartId).OnDelete(DeleteBehavior.ClientSetNull);
            modelBuilder.Entity<PatientEntity>().ForSqlServerHasIndex(s => s.Id).ForSqlServerIsClustered();
            modelBuilder.Entity<PatientEntity>().HasMany(s => s.MedicalCases).WithOne(s => s.Patient)
                .HasForeignKey(s => s.PatientId).OnDelete(DeleteBehavior.ClientSetNull);
            modelBuilder.Entity<PatientEntity>().HasMany(s => s.DemographicInformations).WithOne(s => s.Patient)
                .HasForeignKey(s => s.PatientId).OnDelete(DeleteBehavior.ClientSetNull);
            modelBuilder.Entity<DemographicInformationEntity>().HasKey(s => s.Id).ForSqlServerIsClustered();
            modelBuilder.Entity<DemographicInformationAuditEntity>().HasKey(s => s.Id).ForSqlServerIsClustered();
            modelBuilder.Entity<UserEntity>().ForSqlServerHasIndex(s => s.Id).ForSqlServerIsClustered();
            modelBuilder.Entity<UserEntity>().HasMany(s => s.UserGames).WithOne(s => s.User)
                .HasForeignKey(s => s.PlayerId).OnDelete(DeleteBehavior.ClientSetNull);
            modelBuilder.Entity<UserEntity>().HasMany(s => s.CaseRankings)
                .WithOne(s => s.User).HasForeignKey(s => s.CreatorId)
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<UserEntity>().HasMany(s => s.MediaToUsers)
                .WithOne(s => s.User).HasForeignKey(d => d.UserId).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<UserEntity>().HasMany(s => s.MedicalCases)
                .WithOne(s => s.User).HasForeignKey(s => s.CreatorId).OnDelete(DeleteBehavior.ClientSetNull);
            modelBuilder.Entity<UserEntity>().HasMany(s => s.UserRankingsFrom).WithOne(s => s.UserRatedBy)
                .HasForeignKey(s => s.CreatorId).OnDelete(DeleteBehavior.ClientSetNull);
            modelBuilder.Entity<UserEntity>().HasMany(s => s.UserRankingTo).WithOne(s => s.UserRatedTo)
                .OnDelete(DeleteBehavior.ClientSetNull);
            modelBuilder.Entity<UserGameEntity>().HasKey(s => s.Id).ForSqlServerIsClustered();
            modelBuilder.Entity<UserGameEntity>().HasMany(s => s.CaseRankings).WithOne(s => s.UserGame)
                .HasForeignKey(s => s.UserGameId).OnDelete(DeleteBehavior.ClientSetNull);
            modelBuilder.Entity<UserGameEntity>().HasMany(s => s.UserGamesToMedicalTests)
                .WithOne(d => d.UserGame).HasForeignKey(d => d.UserGameId).OnDelete(DeleteBehavior.ClientSetNull);
            modelBuilder.Entity<MedicalCaseEntity>().HasKey(d => d.Id).ForSqlServerIsClustered();
            modelBuilder.Entity<MedicalCaseEntity>().HasMany(s => s.CaseRankings).WithOne(d => d.MedicalCase)
                .HasForeignKey(d => d.MedicalCaseId).OnDelete(DeleteBehavior.ClientSetNull);
            modelBuilder.Entity<MedicalCaseEntity>().HasMany(s => s.MedicalCasesToBodyParts)
                .WithOne(s => s.MedicalCase).HasForeignKey(s => s.MedicalCaseId).OnDelete(DeleteBehavior.ClientSetNull);
            modelBuilder.Entity<MedicalCaseEntity>().HasMany(s => s.MedicalCasesToMedicalTests)
                .WithOne(s => s.MedicalCase).HasForeignKey(s => s.MedicalCaseId).OnDelete(DeleteBehavior.ClientSetNull);
            modelBuilder.Entity<MedicalTestEntity>().ForSqlServerHasIndex(s => s.Id).ForSqlServerIsClustered();
            modelBuilder.Entity<MedicalTestEntity>().HasMany(s => s.MedicalCasesToMedicalTests)
                .WithOne(s => s.MedicalTest).HasForeignKey(s => s.MedicalTestId).OnDelete(DeleteBehavior.ClientSetNull);
            modelBuilder.Entity<MediumEntity>().ForSqlServerHasIndex(s => s.Id).ForSqlServerIsClustered();
            modelBuilder.Entity<MediumEntity>().HasMany(s => s.MediaToBodyParts).WithOne(s => s.Medium)
                .HasForeignKey(s => s.MediaId).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<MediumEntity>().HasMany(s => s.MediaToUsers).WithOne(s => s.Medium)
                .HasForeignKey(s => s.MediaId).OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<DiseaseEntity>().ForSqlServerHasIndex(s => s.Id).ForSqlServerIsClustered();
            modelBuilder.Entity<DiseaseEntity>().HasMany(s => s.MedicalCases).WithOne(s => s.Disease)
                .HasForeignKey(s => s.DiseaseId).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<DiseaseEntity>().HasMany(s => s.DiseasesToMedicalTests)
                .WithOne(s => s.Disease).HasForeignKey(s => s.DiseaseId).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<DiseaseEntity>().HasMany(s => s.DiseasesToTestResults)
                .WithOne(s => s.Disease).HasForeignKey(s => s.DiseaseId).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<SymptomEntity>().ForSqlServerHasIndex(s => s.Id).ForSqlServerIsClustered();
            modelBuilder.Entity<SymptomEntity>().HasMany(s => s.MedicalCasesToSymptoms)
                .WithOne(s => s.Symptom).HasForeignKey(s => s.SymptomId).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<TestResultEntity>().ForSqlServerHasIndex(s => s.Id).ForSqlServerIsClustered();
            modelBuilder.Entity<TestResultEntity>().HasMany(s => s.DiseasesToTestResults)
                .WithOne(s => s.TestResult).HasForeignKey(s => s.TestResultId).OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<UserRankingEntity>().HasKey(s => s.Id).ForSqlServerIsClustered();
            modelBuilder.Entity<CaseRankingEntity>().HasKey(s => s.Id).ForSqlServerIsClustered();
            modelBuilder.Entity<UserGamesToMedicalTestEntity>().HasKey(s => s.Id).ForSqlServerIsClustered();
            modelBuilder.Entity<MediaToUserEntity>().ForSqlServerHasIndex(s => s.Id).ForSqlServerIsClustered();
            modelBuilder.Entity<MediaToBodyPartEntity>().HasKey(s => s.Id).ForSqlServerIsClustered();
            modelBuilder.Entity<MedicalCasesToSymptomEntity>().HasKey(s => s.Id).ForSqlServerIsClustered();
            modelBuilder.Entity<MedicalCasesToBodyPartEntity>().HasKey(s => s.Id).ForSqlServerIsClustered();
            modelBuilder.Entity<MedicalCasesToMedicalTestEntity>().HasKey(s => s.Id).ForSqlServerIsClustered();
            modelBuilder.Entity<DiseasesToTestResultEntity>().HasKey(s => s.Id).ForSqlServerIsClustered();
            modelBuilder.Entity<DiseasesToMedicalTestEntity>().HasKey(s => s.Id).ForSqlServerIsClustered();



        }

        public virtual DbSet<BodyPartEntity> BodyParts { get; set; }
        public virtual DbSet<CaseRankingEntity> CaseRankings { get; set; }
        public virtual DbSet<DemographicInformationEntity> DemographicInformations { get; set; }
        public virtual DbSet<DiseaseEntity> Diseases { get; set; }
        public virtual DbSet<DiseasesToMedicalTestEntity> DiseasesToMedicalTests { get; set; }
        public virtual DbSet<DiseasesToTestResultEntity> DiseasesToTestResults { get; set; }
        public virtual DbSet<MediumEntity> Media { get; set; }
        public virtual DbSet<MediaToBodyPartEntity> MediaToBodyParts { get; set; }
        public virtual DbSet<MediaToUserEntity> MediaToUsers { get; set; }
        public virtual DbSet<MedicalCaseEntity> MedicalCases { get; set; }
        public virtual DbSet<MedicalCasesToBodyPartEntity> MedicalCasesToBodyParts { get; set; }
        public virtual DbSet<MedicalCasesToMedicalTestEntity> MedicalCasesToMedicalTests { get; set; }
        public virtual DbSet<MedicalCasesToSymptomEntity> MedicalCasesToSymptoms { get; set; }
        public virtual DbSet<MedicalTestEntity> MedicalTests { get; set; }
        public virtual DbSet<PatientEntity> Patients { get; set; }
        public virtual DbSet<SymptomEntity> Symptoms { get; set; }
        public virtual DbSet<TestResultEntity> TestResults { get; set; }
        public virtual DbSet<UserGameEntity> UserGames { get; set; }
        public virtual DbSet<UserGamesToMedicalTestEntity> UserGamesToMedicalTests { get; set; }
        public virtual DbSet<UserRankingEntity> UserRankings { get; set; }
        public virtual DbSet<UserEntity> Users { get; set; }
    }
}
