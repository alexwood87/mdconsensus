
namespace WSD.Consensus.Domain.Entities
{
    using System.Collections.Generic;
    
    public class UserEntity
    {      
        public long Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string SecurityQuestion { get; set; }
        public string SecurityAnswer { get; set; }
        public int UserLevel { get; set; }
        public string CultureName { get; set; }
    
        public virtual ICollection<CaseRankingEntity> CaseRankings { get; set; }
        public virtual ICollection<MediaToUserEntity> MediaToUsers { get; set; }
        public virtual ICollection<MedicalCaseEntity> MedicalCases { get; set; }
        public virtual ICollection<UserGameEntity> UserGames { get; set; }
        public virtual ICollection<UserRankingEntity> UserRankingTo { get; set; }
        public virtual ICollection<UserRankingEntity> UserRankingsFrom { get; set; }
    }
}
