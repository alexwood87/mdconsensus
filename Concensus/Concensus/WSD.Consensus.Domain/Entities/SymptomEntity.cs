
namespace WSD.Consensus.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    
    public class SymptomEntity
    {
        public SymptomEntity()
        {

        }
    
        public long Id { get; set; }
        public string CommonName { get; set; }
        public string ScientificName { get; set; }
        public string CultureName { get; set; }
    
        public virtual ICollection<MedicalCasesToSymptomEntity> MedicalCasesToSymptoms { get; set; }
    }
}
