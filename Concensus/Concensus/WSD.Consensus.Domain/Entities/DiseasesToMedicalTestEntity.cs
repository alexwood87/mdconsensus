
namespace WSD.Consensus.Domain.Entities
{
    using System;

    public class DiseasesToMedicalTestEntity
    {
        public Guid Id { get; set; }
        public long MedicalTestId { get; set; }
        public long DiseaseId { get; set; }
    
        public virtual DiseaseEntity Disease { get; set; }
        public virtual MedicalTestEntity MedicalTest { get; set; }
    }
}
