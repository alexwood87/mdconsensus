
namespace WSD.Consensus.Domain.Entities
{     
    public class MediaToUserEntity
    {
        public long Id { get; set; }
        public long MediaId { get; set; }
        public long UserId { get; set; }
    
        public virtual MediumEntity Medium { get; set; }
        public virtual UserEntity User { get; set; }
    }
}
