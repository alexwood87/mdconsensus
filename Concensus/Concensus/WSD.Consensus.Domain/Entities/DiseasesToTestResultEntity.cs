
namespace WSD.Consensus.Domain.Entities
{
    using System;
    
    public partial class DiseasesToTestResultEntity
    {
        public Guid Id { get; set; }
        public long TestResultId { get; set; }
        public long DiseaseId { get; set; }
        public decimal RandomAccuracyModifier { get; set; }
    
        public virtual DiseaseEntity Disease { get; set; }
        public virtual TestResultEntity TestResult { get; set; }
    }
}
