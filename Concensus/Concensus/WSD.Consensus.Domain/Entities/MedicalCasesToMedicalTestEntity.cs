
namespace WSD.Consensus.Domain.Entities
{
    using System;
    
    public class MedicalCasesToMedicalTestEntity
    {
        public Guid Id { get; set; }
        public Guid MedicalCaseId { get; set; }
        public long MedicalTestId { get; set; }
    
        public virtual MedicalCaseEntity MedicalCase { get; set; }
        public virtual MedicalTestEntity MedicalTest { get; set; }
    }
}
