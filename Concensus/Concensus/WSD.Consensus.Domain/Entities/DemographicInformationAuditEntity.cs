
namespace WSD.Consensus.Domain.Entities
{
    using System;
    
    public class DemographicInformationAuditEntity
    {
        public Guid Id { get; set; }
        public Guid DemographicInformationId { get; set; }
        public int DemographicType { get; set; }
        public DateTime LastModified { get; set; }
        public decimal Weight { get; set; }
        public Guid PatientId { get; set; }
    
        public virtual PatientEntity Patient { get; set; }
    }
}
