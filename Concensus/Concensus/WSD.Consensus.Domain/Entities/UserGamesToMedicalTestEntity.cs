
namespace WSD.Consensus.Domain.Entities
{
    using System;
    
    public class UserGamesToMedicalTestEntity
    {
        public Guid Id { get; set; }
        public Guid UserGameId { get; set; }
        public long MedicalTestId { get; set; }
        public long TestResultId { get; set; }
    
        public virtual MedicalTestEntity MedicalTest { get; set; }
        public virtual TestResultEntity TestResult { get; set; }
        public virtual UserGameEntity UserGame { get; set; }
    }
}
