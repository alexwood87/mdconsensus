
namespace WSD.Consensus.Domain.Entities
{
    using System;
    
    public class UserRankingEntity
    {
        public Guid Id { get; set; }
        public decimal UserRankingValue { get; set; }
        public long UserRankedId { get; set; }
        public long CreatorId { get; set; }
    
        public virtual UserEntity UserRatedBy { get; set; }
        public virtual UserEntity UserRatedTo { get; set; }
    }
}
