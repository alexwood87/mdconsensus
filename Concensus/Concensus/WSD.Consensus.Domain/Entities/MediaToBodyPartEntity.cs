
namespace WSD.Consensus.Domain.Entities
{
    using System;
   
    public class MediaToBodyPartEntity
    {
        public Guid Id { get; set; }
        public long BodyPartId { get; set; }
        public long MediaId { get; set; }
    
        public virtual BodyPartEntity BodyPart { get; set; }
        public virtual MediumEntity Medium { get; set; }
    }
}
