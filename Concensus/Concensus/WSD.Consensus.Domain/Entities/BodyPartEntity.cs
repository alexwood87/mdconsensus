
namespace WSD.Consensus.Domain.Entities
{
    using System.Collections.Generic;
    
    public class BodyPartEntity
    {
  
        public long Id { get; set; }
        public string ScientificName { get; set; }
        public string CommonName { get; set; }
        public string CultureName { get; set; }
    
        public virtual ICollection<MediaToBodyPartEntity> MediaToBodyParts { get; set; }

        public virtual ICollection<MedicalCasesToBodyPartEntity> MedicalCasesToBodyParts { get; set; }
    }
}
