
namespace WSD.Consensus.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    
    public class UserGameEntity
    {
       
        public Guid Id { get; set; }
        public long PlayerId { get; set; }
        public int GameLevel { get; set; }
        public string GameName { get; set; }
        public decimal? Score { get; set; }
        public bool IsFinished { get; set; }
        public Guid MedicalCaseId { get; set; }
    
        public virtual ICollection<CaseRankingEntity> CaseRankings { get; set; }
        public virtual MedicalCaseEntity MedicalCase { get; set; }
        public virtual UserEntity User { get; set; }
        public virtual ICollection<UserGamesToMedicalTestEntity> UserGamesToMedicalTests { get; set; }
    }
}
