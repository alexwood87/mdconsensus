
namespace WSD.Consensus.Domain.Entities
{
    using System;
   
    public class CaseRankingEntity
    {
        public Guid Id { get; set; }
        public long CreatorId { get; set; }
        public Guid MedicalCaseId { get; set; }
        public Guid UserGameId { get; set; }
        public decimal Rating { get; set; }
        public decimal WeightModifier { get; set; }
    
        public virtual UserEntity User { get; set; }
        public virtual MedicalCaseEntity MedicalCase { get; set; }
        public virtual UserGameEntity UserGame { get; set; }
    }
}
