
namespace WSD.Consensus.Domain.Entities
{
    using System.Collections.Generic;
    
    public class DiseaseEntity
    {
        public DiseaseEntity()
        {

        }
    
        public long Id { get; set; }
        public string CommonName { get; set; }
        public string ScientificName { get; set; }
        public string CultureName { get; set; }
    
        public virtual ICollection<DiseasesToTestResultEntity> DiseasesToTestResults { get; set; }
        public virtual ICollection<DiseasesToMedicalTestEntity> DiseasesToMedicalTests { get; set; }
        public virtual ICollection<MedicalCaseEntity> MedicalCases { get; set; }
    }
}
