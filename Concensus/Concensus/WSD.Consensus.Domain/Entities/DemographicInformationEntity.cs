
namespace WSD.Consensus.Domain.Entities
{
    using System;
 
    public partial class DemographicInformationEntity
    {
        public Guid Id { get; set; }
        public int DemographicType { get; set; }
        public decimal Weight { get; set; }
        public Guid PatientId { get; set; }
        public int? Value { get; set; }
    
        public virtual PatientEntity Patient { get; set; }
    }
}
