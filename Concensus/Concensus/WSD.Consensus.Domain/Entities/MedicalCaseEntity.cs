
namespace WSD.Consensus.Domain.Entities
{
    using System;
    using System.Collections.Generic;
    
    public class MedicalCaseEntity
    {
        public MedicalCaseEntity()
        {

        }
    
        public Guid Id { get; set; }
        public string Description { get; set; }
        public string CaseName { get; set; }
        public string CultureName { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid PatientId { get; set; }
        public long CreatorId { get; set; }
        public long DiseaseId { get; set; }
    
        public virtual ICollection<CaseRankingEntity> CaseRankings { get; set; }
        public virtual DiseaseEntity Disease { get; set; }
        public virtual UserEntity User { get; set; }
        public virtual ICollection<MedicalCasesToMedicalTestEntity> MedicalCasesToMedicalTests { get; set; }
        public virtual ICollection<MedicalCasesToSymptomEntity> MedicalCasesToSymptoms { get; set; }
        public virtual ICollection<MedicalCasesToBodyPartEntity> MedicalCasesToBodyParts { get; set; }
        public virtual PatientEntity Patient { get; set; }
        public virtual ICollection<UserGameEntity> UserGames { get; set; }
    }
}
