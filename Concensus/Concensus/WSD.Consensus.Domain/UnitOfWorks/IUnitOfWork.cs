﻿using WSD.Consensus.Domain.Repositories;

namespace WSD.Consensus.Domain.UnitOfWorks
{
    public interface IUnitOfWork
    {
        IMedicalCaseRatingRepository MedicalCaseRatingRepository { get;}
        IMedicalCaseRepository MedicalCaseRepository { get;}
        IUserRepository UserRepository { get;}
        IUserRatingRepository UserRatingRepository { get;}
        IUserGameRepository UserGameRepository { get;}
        IBodyPartRepository BodyPartRepository { get;}
        IDiseaseRepository DiseaseRepository { get;}
        ISymptomRepository SymptomRepository { get;}
        IMediaRepository MediaRepository { get;}
        IMedicalTestRepository MedicalTestRepository { get;}    
    }
}
