﻿using System;
using System.Linq;
using System.Collections.Generic;
using AutoMapper;
using WSD.Consensus.Domain.Entities;
using WSD.Consensus.Domain.Models;

namespace WSD.Consensus.Domain.Helpers
{
    public class EntityMapper
    {
        static EntityMapper()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<DiseaseModel, DiseaseEntity>();
                cfg.CreateMap<DiseaseEntity, DiseaseModel>();
                cfg.CreateMap<UserModel, UserEntity>();
                cfg.CreateMap<UserEntity, UserModel>();
                cfg.CreateMap<UserGameModel, UserGameEntity>();
                cfg.CreateMap<UserGameEntity, UserGameModel>();
                cfg.CreateMap<SymptomModel, SymptomEntity>();
                cfg.CreateMap<SymptomEntity, SymptomModel>();
                cfg.CreateMap<MediaModel, MediumEntity>()
                      .ForMember(s => s.FileType, opt => opt.MapFrom(t => (FileType?)t.FileType)); 
                cfg.CreateMap<MediumEntity, MediaModel>()
                      .ForMember(s => s.FileType, opt => opt.MapFrom(t => (int?)t.FileType)); 
                cfg.CreateMap<PatientModel, PatientEntity>();
                cfg.CreateMap<PatientEntity, PatientModel>();
                cfg.CreateMap<BodyPartModel, BodyPartEntity>();
                cfg.CreateMap<BodyPartEntity, BodyPartModel>();
                cfg.CreateMap<CaseRatingModel, CaseRankingEntity>();
                cfg.CreateMap<CaseRankingEntity, CaseRankingEntity>();
                cfg.CreateMap<TestResultModel, TestResultEntity>();
                cfg.CreateMap<TestResultEntity, TestResultModel>();
                cfg.CreateMap<MedicalCaseModel, MedicalCaseEntity>();
                cfg.CreateMap<MedicalCaseEntity, MedicalCaseModel>();
                cfg.CreateMap<UserRatingModel, UserRankingEntity>();
                cfg.CreateMap<UserRankingEntity, UserRatingModel>();
                cfg.CreateMap<DemographicInformationModel, DemographicInformationEntity>()
                .ForMember(s => s.DemographicType, opt => opt.MapFrom(t => (int)t.DemographicType));
                cfg.CreateMap<DemographicInformationEntity, DemographicInformationModel>()
                      .ForMember(s => s.DemographicType, opt => opt.MapFrom(t => (DemographicType)t.DemographicType));

            });
        }

        public static E Map<T, E>(T mapFromModel) where E : class where T : class
        {
            if (mapFromModel == null)
                return null;
            return Mapper.Map<T, E>(mapFromModel);
        }
        public static CaseRatingModel MapCaseRatingToModel(CaseRankingEntity c)
        {
            if(c == null)
            {
                return null;
            }
            return
                new CaseRatingModel
                {
                    Id = c.Id,
                    Rating = c.Rating,
                    WeightModifier = c.WeightModifier,
                    Creator = new UserModel
                    {
                        Email = c.User.Email,
                        Id = c.User.Id                     
                    },
                    Game = new UserGameModel
                    {
                        IsFinished = c.UserGame.IsFinished,
                        Id = c.UserGameId
                    }                
                };
        }
        public static MedicalCaseModel MapMedicalCase(MedicalCaseEntity medicalCase)
        {
            var m =
                new MedicalCaseModel
                {
                    CreatedDate = medicalCase.CreatedDate,
                    Creator = Map<UserEntity, UserModel>(medicalCase.User),
                    CultureName = medicalCase.CultureName,
                    Description = medicalCase.Description,
                    Id = medicalCase.Id,
                    MedicalCaseName = medicalCase.CaseName,
                    Patient = Map<PatientEntity, PatientModel>(medicalCase.Patient),
                    MedicalTests = medicalCase.MedicalCasesToMedicalTests?.Select(x => Map<MedicalTestEntity, MedicalTestModel>(x.MedicalTest)).ToList() ?? new List<MedicalTestModel>(),
                    TestResults = medicalCase.MedicalCasesToMedicalTests?.Select(x => x.MedicalTest.TestResults.Any() ? Map<TestResultEntity, TestResultModel>(x.MedicalTest.TestResults.First()) : null).ToList() ?? new List<TestResultModel>(),
                     Disease = new DiseaseModel {
                        CommonName =  medicalCase.Disease.CommonName,
                        CultureName = medicalCase.CultureName,
                         Id = medicalCase.DiseaseId,
                        ScientificName = medicalCase.Disease.ScientificName
                     }

                };
            var bps = medicalCase.MedicalCasesToBodyParts.Select(x => Map<BodyPartEntity, BodyPartModel>(x.BodyPart)).ToList();
            var bps2 = medicalCase.MedicalCasesToBodyParts.Select(x => x.Id).ToArray()??new Guid[0];
            var index = 0;
            m.BodyPartToMedicalCaseModels = new List<BodyPartToMedicalCaseModel>();
            foreach (var bp in bps)
            {
                m.BodyPartToMedicalCaseModels.Add(new BodyPartToMedicalCaseModel
                {
                    BodyPartModel = bp,
                    Accurracy = 1,
                    Id = bps2[index]
                });
                index++;
            }
            index = 0;
            var syms = medicalCase.MedicalCasesToSymptoms.Select(x => Map<SymptomEntity, SymptomModel>(x.Symptom)).ToArray();
            var symIds = medicalCase.MedicalCasesToSymptoms.Select(x => x.Id).ToArray();
            m.SymptomToMedicalCaseModels = new List<SymptomToMedicalCaseModel>();
            foreach (var sym in syms)
            {
                m.SymptomToMedicalCaseModels.Add(new SymptomToMedicalCaseModel
                {
                    Accurracy = 1,
                    SymptomModel = sym,
                    Id = symIds[index]
                });
                index++;
            }
            m.Patient.DemographicInformation = MapDemosToModel(medicalCase.Patient.DemographicInformations);
            return m;
        }
        public static List<DemographicInformationModel> MapDemosToModel(ICollection<DemographicInformationEntity> demos)
        {
            if(demos == null)
            {
                return
                    null;
            }
            return
                demos.Select(x => new DemographicInformationModel
                {
                    Id = x.Id,
                    DemographicType = (DemographicType) x.DemographicType,
                    Weight = (int)x.Weight,
                    Value = x.Value.GetValueOrDefault(0)
                }).ToList();
        }
        public static MedicalCaseEntity MapMedicalCaseToEntity(MedicalCaseModel medicalCase)
        {
            var m = new MedicalCaseEntity
            {              
                CaseName = medicalCase.MedicalCaseName,
                CreatedDate = medicalCase.CreatedDate,
                Description = medicalCase.Description,
                CreatorId = medicalCase.Creator.Id,
                User = new UserEntity
                {
                    Id = medicalCase.Creator.Id,
                    UserLevel = (int)medicalCase.Creator.UserLevel,
                    Email = medicalCase.Creator.Email,
                    CultureName = medicalCase.CultureName,
                    Password = medicalCase.Creator.Password,
                    SecurityAnswer = medicalCase.Creator.Answer,
                    SecurityQuestion = medicalCase.Creator.Question
                },
                MedicalCasesToSymptoms = medicalCase.SymptomToMedicalCaseModels?.Select(x => new MedicalCasesToSymptomEntity
                {
                    SymptomId = x.SymptomModel.Id,
                    Accuracy = x.Accurracy,
                    Symptom = Map<SymptomModel, SymptomEntity>(x.SymptomModel),
                    MedicalCaseId = medicalCase.Id

                }).ToList(),
                MedicalCasesToBodyParts = medicalCase.BodyPartToMedicalCaseModels?.Select(x => new MedicalCasesToBodyPartEntity
                {
                    BodyPart = Map<BodyPartModel, BodyPartEntity>(x.BodyPartModel),
                    BodyPartId = x.BodyPartModel.Id,
                    MedicalCaseId = medicalCase.Id,
                    Accuracy = x.Accurracy
                }).ToList() ?? new List<MedicalCasesToBodyPartEntity>(),
                CultureName = medicalCase.CultureName,
                DiseaseId = medicalCase.Disease?.Id ?? 0,
                Disease = Map<DiseaseModel, DiseaseEntity>(medicalCase.Disease),
                Id = medicalCase.Id == Guid.Empty ? Guid.NewGuid() : medicalCase.Id,
                Patient = new PatientEntity {
                   FirstName =  medicalCase.Patient.FirstName,
                   LastName = medicalCase.Patient.LastName,
                   MiddleName = medicalCase.Patient.MiddleName,
                   DemographicInformations = medicalCase.Patient.DemographicInformation.Select(x => 
                    new DemographicInformationEntity {
                       DemographicType = (int) x.DemographicType,
                       Id = x.Id == Guid.Empty ? Guid.NewGuid() : x.Id,
                       Weight = x.Weight,
                       Value = x.Value
                   }).ToList()

                },
                PatientId = medicalCase.Patient.Id,
                MedicalCasesToMedicalTests = medicalCase.MedicalTests?.Select(x => new MedicalCasesToMedicalTestEntity
                {
                    MedicalTestId = x.Id,
                    MedicalTest = Map<MedicalTestModel, MedicalTestEntity>(x)
                }).ToList(),
                CaseRankings = medicalCase.Ratings?.Select(Map<CaseRatingModel, CaseRankingEntity>).ToList()
            };
            return m;
        }

        public static UserGameEntity MapGameModelToEntity(UserGameModel model)
        {
            var rnd = new Random();
            return
                new UserGameEntity
                {
                    GameLevel = model.GameLevel,
                    GameName = model.GameName,
                    IsFinished = model.IsFinished,
                    MedicalCaseId = model.MedicalCase.Id,
                    PlayerId = model.Player.Id,
                    Id = model.Id,
                    User = Map<UserModel, UserEntity>(model.Player),
                    Score = model.Score,
                    MedicalCase = MapMedicalCaseToEntity(model.MedicalCase)
                };
        }

        public static UserGameModel MapGameEntityToModel(UserGameEntity userGame)
        {
            var rnd = new Random();
            return
                new UserGameModel
                {
                    IsFinished = userGame.IsFinished,
                    CaseRating = new CaseRatingModel
                    {
                        Id = userGame.CaseRankings.Any() ? userGame.CaseRankings.First().Id : Guid.Empty,
                        Rating = userGame.CaseRankings.Sum(x => x.Rating) / (userGame.CaseRankings.Count == 0 ? 1 : userGame.CaseRankings.Count),
                        Creator = MapUserToModel(userGame.User),
                        MedicalCase = MapMedicalCase(userGame.MedicalCase),
                        WeightModifier = 1

                    },
                    Player = MapUserToModel(userGame.User),
                    Score = userGame.Score.GetValueOrDefault(0),
                    GameLevel = userGame.GameLevel,
                    GameName = userGame.GameName,
                    MedicalCase = MapMedicalCase(userGame.MedicalCase),
                    TestResultsFromTestsDone = userGame.UserGamesToMedicalTests.Select(x => MapTestResultEntityToModel(x.TestResult)).ToList(),
                    Id = userGame.Id,
                    TestsDone = userGame.UserGamesToMedicalTests.Select(x => new MedicalTestModel
                    {
                        AffectiveModifer = (decimal)Math.Abs(rnd.NextDouble()) % 101,
                        Name = x.MedicalTest.Name,
                        Id = x.MedicalTestId,
                        DiseasesToProvePresenceOf = userGame.UserGamesToMedicalTests.Select(y => Map<DiseaseEntity, DiseaseModel>(y.MedicalTest.DiseasesToMedicalTests.FirstOrDefault()?.Disease)).ToList()
                    }).ToList()
                };
        }

        private static UserModel MapUserToModel(UserEntity user)
        {
            return
                new UserModel
                {
                    Id = user.Id,
                    UserLevel = (UserLevel)user.UserLevel,
                    Answer = user.SecurityAnswer,
                    Email = user.Email,
                    CultureName = user.CultureName,
                    Password = user.Password,
                    Question = user.SecurityQuestion,
                     UserRatings = user.UserRankingsFrom.Select(c => new UserRatingModel
                     {
                         Id = c.Id,
                         Rating = c.UserRankingValue,
                         Weight = (int) c.UserRatedBy.UserLevel
                     }).ToList()

                };
        }
        public static UserRatingModel MapUserRatingToModel(UserRankingEntity r)
        {
            if(r == null)
            {
                return null;
            }
            return
                new UserRatingModel
                {
                    AppliedToUser = new UserModel
                    {
                        Id = r.UserRankedId
                    },
                    Id = r.Id,
                    Creator = new UserModel
                    {
                        Id = r.CreatorId
                    },
                    Rating = r.UserRankingValue
                };
        }
        public static TestResultModel MapTestResultEntityToModel(TestResultEntity testResult)
        {
            var rnd = new Random();

            return
                new TestResultModel
                {
                    Acurracy = testResult.Accuracy,
                    DiseasesPresent = testResult.DiseasesToTestResults.Where(x => x != null && x.Disease != null).
                        Select(x => EntityMapper.Map<DiseaseEntity, DiseaseModel>(x.Disease)).ToList(),
                    Id = testResult.Id,
                    RandomWeightModifier = (decimal)Math.Abs(rnd.NextDouble()) % 101
                };
        }
    }
}
