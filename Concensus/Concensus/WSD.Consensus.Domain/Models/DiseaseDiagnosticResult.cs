﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
namespace WSD.Consensus.Domain.Models
{
    [DataContract]
    public class DiseaseDiagnosticResult
    {
        [DataMember]
        public MethodMessage Message { get; set; }
        [DataMember]
        public DiseaseModel Disease { get; set; }
        [DataMember]
        public double Likelihood { get; set; }
    }
}
