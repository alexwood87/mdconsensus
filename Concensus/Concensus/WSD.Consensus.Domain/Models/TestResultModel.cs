﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System;
namespace WSD.Consensus.Domain.Models
{
    [DataContract]
    public class TestResultModel
    {
        [DataMember]
        public MethodMessage Message { get; set; }
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public decimal RandomWeightModifier { get; set; }
        [DataMember]
        public decimal Acurracy { get; set; }
        [DataMember]
        public List<DiseaseModel> DiseasesPresent { get; set; }
        [DataMember]
        public MedicalTestModel Test { get; set; }
    }
}
