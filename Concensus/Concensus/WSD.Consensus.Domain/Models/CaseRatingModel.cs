﻿using System;
using System.Runtime.Serialization;
namespace WSD.Consensus.Domain.Models
{
    [DataContract]
    public class CaseRatingModel
    {
        [DataMember]
        public MethodMessage Message { get; set; }
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public decimal Rating { get; set; }
        [DataMember]
        public decimal WeightModifier { get; set; }
        [DataMember]
        public MedicalCaseModel MedicalCase { get; set; }
        [DataMember]
        public UserModel Creator { get; set; }
        [DataMember]
        public UserGameModel Game { get; set; } 
    }
}
