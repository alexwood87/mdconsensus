﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
namespace WSD.Consensus.Domain.Models
{
    [DataContract]
    public class MediaSearchQuery
    {
        [DataMember]
        public long? MediaId { get; set; }
        [DataMember]
        public string FileName { get; set; }
        [DataMember]
        public FileType? FileType { get; set; }
        [DataMember]
        public long? UserId { get; set; }
        [DataMember]
        public long? BodyPartId { get; set; } 
    }
}
