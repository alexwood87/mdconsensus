﻿using System.Runtime.Serialization;
namespace WSD.Consensus.Domain.Models
{
    [DataContract]
    public class DiseaseModel
    {
        [DataMember]
        public MethodMessage Message { get; set; }
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string ScientificName { get; set; }
        [DataMember]
        public string CultureName { get; set; }
        [DataMember]
        public string CommonName { get; set; }
    }
}
