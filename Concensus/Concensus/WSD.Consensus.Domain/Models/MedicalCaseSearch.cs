﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WSD.Consensus.Domain.Models
{
    [DataContract]
    public class MedicalCaseSearch
    {
        [DataMember]
        public long? UserIdWhosePlayingToIgnore { get; set; }
        [DataMember]
        public Guid? MedicalCaseId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public List<SymptomModel> Symptoms { get; set; }
        [DataMember]
        public List<BodyPartModel> BodyParts { get; set; }
        [DataMember]
        public List<DemographicInformationModel> DemoGraphicInfo { get; set; }
        [DataMember]
        public string DiseaseName { get; set; }
        [DataMember]
        public long? CreatorId { get; set; }
    }
}
