﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WSD.Consensus.Domain.Models
{
    [DataContract]
    public class UserModel
    {
        [DataMember]
        public MethodMessage Message { get; set; }
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public string Question { get; set; }
        [DataMember]
        public string Answer { get; set; }
        [DataMember]
        public List<UserRatingModel> MyAppliedToRatings { get; set; }
        [DataMember]
        public List<UserRatingModel> UserRatings { get; set; }
        [DataMember]
        public List<UserGameModel> GamesPlayed { get; set; }
        [DataMember]
        public List<MedicalCaseModel> MedicalCasesSubmitted { get; set; }
        [DataMember]
        public UserLevel UserLevel { get; set; }
        [DataMember]
        public string CultureName { get; set; }
    }
}
