﻿using System;
using System.Runtime.Serialization;
namespace WSD.Consensus.Domain.Models
{
    [DataContract]
    public class MediaModel
    {
        [DataMember]
        public long? BodyPartId { get; set; }
        [DataMember]
        public Guid? Id { get; set; }
        [DataMember]
        public string FilePath { get; set; }
        [DataMember]
        public int FileType { get; set; }
        [DataMember]
        public long? UserId { get; set; }
    }
}
