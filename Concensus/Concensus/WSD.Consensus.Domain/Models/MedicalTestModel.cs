﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WSD.Consensus.Domain.Models
{
    [DataContract]
    public class MedicalTestModel
    {
        [DataMember]
        public MethodMessage Message { get; set; }
        [DataMember]
        public long Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public List<DiseaseModel> DiseasesToProvePresenceOf { get; set; }
        [DataMember]
        public decimal AffectiveModifer { get; set; }
    }
}
