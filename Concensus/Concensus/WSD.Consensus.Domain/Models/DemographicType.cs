﻿namespace WSD.Consensus.Domain.Models
{
    public enum DemographicType : int
    {
        Race = 1,
        Country,
        Gender,
        Weight,
        Age
    }
}
