﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
namespace WSD.Consensus.Domain.Models
{
    [DataContract]
    public class UserGameSearch
    {
        [DataMember]
        public Guid? UserGameId { get; set; }
        [DataMember]
        public string UseGameName { get; set; }
        [DataMember]
        public long? UserGameUserId { get; set; }
    }
}
