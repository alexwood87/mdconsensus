﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
namespace WSD.Consensus.Domain.Models
{
    [DataContract]
    public enum FileType
    {
        [EnumMember]
        Skin =1,
        [EnumMember]
        Sound
    }
}
