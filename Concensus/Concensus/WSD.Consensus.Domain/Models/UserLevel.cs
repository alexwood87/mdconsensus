﻿using System.Runtime.Serialization;

namespace WSD.Consensus.Domain.Models
{
    [DataContract]
    public enum UserLevel : int
    {
        [EnumMember]
        Player=0,
        [EnumMember]
        AlliedHealthStudent,
        [EnumMember]
        NursingStudent,
        [EnumMember]
        MedicalStudent,
        [EnumMember]
        AlliedHealthProfessional,
        [EnumMember]
        RegisteredNurse,
        [EnumMember]
        Doctor
    }
}
