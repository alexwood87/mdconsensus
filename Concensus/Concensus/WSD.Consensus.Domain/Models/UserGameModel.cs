﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WSD.Consensus.Domain.Models
{
    [DataContract]
    public class UserGameModel
    {
        [DataMember]
        public MethodMessage Message { get; set; }
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public string GameName { get; set; }
       [DataMember]
        public int GameLevel { get; set; }
        [DataMember]
        public MedicalCaseModel MedicalCase { get; set; }
        [DataMember]
        public UserModel Player { get; set; }
        [DataMember]
        public CaseRatingModel CaseRating { get; set; }
        [DataMember]
        public bool IsFinished { get; set; }
        [DataMember]
        public decimal Score { get; set; }
        [DataMember]
        public List<MedicalTestModel> TestsDone { get; set; }
        [DataMember]
        public List<TestResultModel> TestResultsFromTestsDone { get; set; } 
    
    }
}
