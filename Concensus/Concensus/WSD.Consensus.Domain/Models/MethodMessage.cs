﻿using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Runtime.CompilerServices;
namespace WSD.Consensus.Domain.Models
{
    [DataContract]
    public class MethodMessage
    {
        private string _callingMethod;
        public MethodMessage([CallerMemberName]string methodName = "")
        {
            _callingMethod = methodName;
        }
        [DataMember]
        public string CallingMethod
        {
            get
            {
                return _callingMethod;
            }
            set
            {

            }
        }
        [DataMember]
        private bool _wasSuccess;
        [DataMember]
        public List<MethodMessage> SubMessages { get; set; }
        [DataMember]
        public string FriendlyMessage { get; set; }
        [DataMember]
        public string DeveloperMessage { get; set; }
        [DataMember]
        public bool CallWasSuccess
        {
            get
            {
                return _wasSuccess && (SubMessages == null || SubMessages.All(x => x.CallWasSuccess));
            }
            set
            {
                _wasSuccess = value;
            }
        }
    }
}
