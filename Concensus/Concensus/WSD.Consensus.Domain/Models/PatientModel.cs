﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WSD.Consensus.Domain.Models
{
    [DataContract]
    public class PatientModel
    {
        [DataMember]
        public MethodMessage Message { get; set; }
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string MiddleName { get; set; }
        [DataMember]
        public List<DemographicInformationModel> DemographicInformation { get; set; }
 
    }
}
