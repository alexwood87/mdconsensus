﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
namespace WSD.Consensus.Domain.Models
{
    [DataContract]
    public class MetaDataSearchQuery
    {
        [DataMember]
        public long? Id { get; set; }
        [DataMember]
        public string ScientificName { get; set; }
        [DataMember]
        public string CommonName { get; set; }
        [DataMember]
        public string CultureName { get; set; }
        [DataMember]
        public bool DoDbLookUpIfFails { get; set; }
        [DataMember]
        public int PageIndex { get; set; }
        [DataMember]
        public int NumRowsReturned { get; set; }
    }
}
