﻿using System;
using System.Runtime.Serialization;

namespace WSD.Consensus.Domain.Models
{
    [DataContract]
    public class UserRatingModel
    {
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public decimal Weight { get; set; }
        [DataMember]
        public decimal Rating { get; set; }
        [DataMember]
        public UserModel Creator { get; set; }
        [DataMember]
        public UserModel AppliedToUser { get; set; }
        [DataMember]
        public MethodMessage Message { get; set; }
    }
}
