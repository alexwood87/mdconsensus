﻿using System;
using System.Runtime.Serialization;

namespace WSD.Consensus.Domain.Models
{
    [DataContract]
    public class BodyPartToMedicalCaseModel
    {
        [DataMember]
        public MethodMessage Message { get; set; }
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public BodyPartModel BodyPartModel { get; set; }
        [DataMember]
        public decimal Accurracy { get; set; }
    }
}
