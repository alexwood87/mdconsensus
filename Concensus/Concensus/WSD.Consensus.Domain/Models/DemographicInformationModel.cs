﻿using System;
using System.Runtime.Serialization;
namespace WSD.Consensus.Domain.Models
{
    [DataContract]
    public class DemographicInformationModel
    {
        [DataMember]
        public MethodMessage Message { get; set; }
        [DataMember]
        public DemographicType  DemographicType { get; set; }
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public int Weight { get; set; }
        [DataMember]
        public int Value { get; set; }
        [DataMember]
        public PatientModel Patient { get; set; }
    }
}
