﻿using System;
using System.Runtime.Serialization;

namespace WSD.Consensus.Domain.Models
{
    [DataContract]
    public class SymptomToMedicalCaseModel
    {
        [DataMember]
        public MethodMessage Message { get; set; }
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public SymptomModel SymptomModel { get; set; }
        [DataMember]
        public decimal Accurracy { get; set; }
    }
}
