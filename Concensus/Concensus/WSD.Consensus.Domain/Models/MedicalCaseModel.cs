﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace WSD.Consensus.Domain.Models
{
    [DataContract]
    public class MedicalCaseModel
    {
        [DataMember]
        public MethodMessage Message { get; set; }
        [DataMember]
        public List<CaseRatingModel> Ratings { get; set; }
        [DataMember]
        public DiseaseModel Disease { get; set; }
        [DataMember]
        public UserModel Creator { get; set; }
        [DataMember]
        public PatientModel Patient { get; set; }
        [DataMember]
        public List<SymptomToMedicalCaseModel> SymptomToMedicalCaseModels { get; set; }
        [DataMember]
        public List<MedicalTestModel> MedicalTests { get; set; }
        [DataMember]
        public List<TestResultModel> TestResults { get; set; }
        [DataMember]
        public List<BodyPartToMedicalCaseModel> BodyPartToMedicalCaseModels { get; set; }
        [DataMember]
        public Guid Id { get; set; }
        [DataMember]
        public string MedicalCaseName { get; set; }
        [DataMember]
        public DateTime CreatedDate { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public string CultureName { get; set; }
    }
}
