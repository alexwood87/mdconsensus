﻿namespace WSD.Consensus.Domain.Repositories
{
    public interface IGenericRepository<TE> where TE : class
    {
        TE Find(object id);
        TE Delete(TE id);
        TE Add(TE entity);
        TE Update(TE entity);
    }
}
