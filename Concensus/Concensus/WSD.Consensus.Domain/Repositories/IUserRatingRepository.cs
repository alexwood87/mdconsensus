﻿using WSD.Consensus.Domain.Models;
using System;
using System.Collections.Generic;
using WSD.Consensus.Domain.Entities;

namespace WSD.Consensus.Domain.Repositories
{
    public interface IUserRatingRepository : IGenericRepository<UserRankingEntity>
    {
        UserRatingModel CreateUserRating(long creatorId, long userToRankId, decimal userRatingValue);
        UserRatingModel EditUserRating(long ratingId, long creatorId, long userToRankId, decimal userRatingValue);
        List<UserRatingModel> FindByCreatorId(long creatorId);
        UserRatingModel FindByUserRatingId(Guid ratingId);
        List<UserRatingModel> FindByUserRankedId(long userRankedId);
        void Save();
    }
}
