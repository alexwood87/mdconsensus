﻿using WSD.Consensus.Domain.Entities;
using WSD.Consensus.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSD.Consensus.Domain.Repositories
{
    public interface IBodyPartRepository : IGenericRepository<BodyPartEntity>
    {
        BodyPartModel CreateBodyPart(string scientificName, string commonName, string cultureName);
        BodyPartModel EditBodyPart(long id, string scientificName, string commonName, string cultureName);
        List<BodyPartModel> LoadAll();
        BodyPartModel FindById(long id);
        BodyPartModel FindByScientificName(string sciName, string cultureName);
        List<BodyPartModel> FindByScientificNameContains(string sciName, string cultureName);
    }
}
