﻿using WSD.Consensus.Domain.Models;
using System.Collections.Generic;
using WSD.Consensus.Domain.Entities;

namespace WSD.Consensus.Domain.Repositories
{
    public interface ISymptomRepository : IGenericRepository<SymptomEntity>
    {
        SymptomModel CreateSymptom(string scientificName, string commonName, string cultureName);
        SymptomModel EditSymptom(long id, string scientificName, string commonName, string cultureName);
        SymptomModel FindById(long id);
        SymptomModel FindByScientificName(string scientificName, string cultureName);
        List<SymptomModel> FindByScientificNameContains(string scientificName, string cultureName);
        List<SymptomModel> LoadAll();
    }
}
