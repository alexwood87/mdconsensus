﻿using System;
using System.Collections.Generic;
using WSD.Consensus.Domain.Entities;
using WSD.Consensus.Domain.Models;
namespace WSD.Consensus.Domain.Repositories
{
    public interface IMediaRepository : IGenericRepository<MediumEntity>
    {
        List<MediaModel> SearchMedia(MediaSearchQuery query);
        bool CreateMediaForUser(long userId, string fileName, int ft, string filePath);
        bool CreateMediaForBodyPart(long bodyPartId, string fileName, int ft, string filePath);
        bool EditMediaForUser(long id, long userId, string fileName, int ft, string filePath);
        bool EditMediaForBodyPart(Guid id,long bodyPartId, string fileName, int ft, string filePath);        
    }
}
