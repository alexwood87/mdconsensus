﻿using WSD.Consensus.Domain.Models;
using System.Collections.Generic;
using WSD.Consensus.Domain.Entities;

namespace WSD.Consensus.Domain.Repositories
{
    public interface IUserRepository : IGenericRepository<UserEntity>
    {
        UserModel CreateUser(string email, string password, string question, string answer, int userLevel, string cultureName);
        UserModel EditUser(long id, string email, string password, string question, string answer, int userLevel, string cultureName);
       
        UserModel ValidateUser(string email, string password);
        bool ChangePassword(string email, string oldPassword, string newPassword);
        List<UserModel> SearchUsers(long? id, string email); 
        bool GenerateUserPassword(string answer, string email);
        bool DeActivate(long id);
    }
}
