﻿using System;
using System.Collections.Generic;
using WSD.Consensus.Domain.Entities;
using WSD.Consensus.Domain.Models;

namespace WSD.Consensus.Domain.Repositories
{
    public interface IMedicalTestRepository : IGenericRepository<MedicalTestEntity>
    {
        List<MedicalTestModel> SearchMedicalTests(long? id, string name, string culture);
        MedicalTestModel CreateMedicalTest(Guid? medCaseId, MedicalTestModel model, TestResultModel result, DiseaseModel disease);

    }
}
