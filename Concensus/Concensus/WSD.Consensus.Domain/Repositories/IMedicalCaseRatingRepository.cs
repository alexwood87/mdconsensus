﻿using WSD.Consensus.Domain.Models;
using System;
using System.Collections.Generic;
using WSD.Consensus.Domain.Entities;

namespace WSD.Consensus.Domain.Repositories
{
    public interface IMedicalCaseRatingRepository : IGenericRepository<CaseRankingEntity>
    {
        CaseRatingModel CreateMedicalCaseRating(long creatorId, Guid userGameId, Guid medicalCaseId, decimal medicalCaseRatingValue);
        CaseRatingModel EditMedicalCaseRating(Guid ratingId, long creatorId, Guid medicalCaseId, decimal medicalCaseRatingValue);
        List<CaseRatingModel> FindByCreatorId(long creatorId);
        CaseRatingModel FindByRatingId(Guid ratingId);
        List<CaseRatingModel> FindByMedicalCaseId(Guid medicalCaseId);
    }
}
