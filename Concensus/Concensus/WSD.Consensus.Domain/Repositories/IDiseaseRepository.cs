﻿using WSD.Consensus.Domain.Models;
using System.Collections.Generic;
using WSD.Consensus.Domain.Entities;

namespace WSD.Consensus.Domain.Repositories
{
    public interface IDiseaseRepository : IGenericRepository<DiseaseEntity>
    {
        DiseaseModel CreateDisease(string scientificName, string commonName, string cultureName);
        DiseaseModel EditDisease(long id, string scientificName, string commonName, string cultureName);
        List<DiseaseModel> LoadAll();
        DiseaseModel FindById(long id);
        DiseaseModel FindByScientificName(string scientificName, string cultureName);
        List<DiseaseModel> FindByScientificNameContains(string scientificName, string cultureName);
    }
}
