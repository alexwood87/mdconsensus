﻿using WSD.Consensus.Domain.Models;
using System;
using System.Collections.Generic;
using WSD.Consensus.Domain.Entities;

namespace WSD.Consensus.Domain.Repositories
{
    public interface IUserGameRepository : IGenericRepository<UserGameEntity>
    {
        Guid? CreateGame(UserGameModel model);
        bool UpdateGame(UserGameModel model);
        UserGameModel FindGame(Guid id);
        TestResultModel MakeMove(Guid gameId, long medicalTestId);
        List<UserGameModel> SearchUserGames(UserGameSearch query, int page, int numGames);
    }
}
