﻿using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using WSD.Consensus.Common.Http;
using WSD.Consensus.Common.Http.Implementations;
using WSD.Consensus.Common.Models;

namespace WSD.Consensus.BusinessLogic.Helpers
{
    public class RefreshHelper
    {
        private readonly IHttpIssuer _httpIssuer;
        public RefreshHelper()
        {
            _httpIssuer = new HttpIssuer();
        }

        private static readonly List<string> ServersList = new List<string>
        {
            "http://www.RoseWood.com/Services/ConcensusRefresher",
            "http://www.RoseWood.com:8080/Services/ConcensusRefresher",
            "http://www.RoseWood.com:6060/Services/ConcensusRefresher"
        };
        public List<string> Servers => ServersList;
   
        public void ResetServers(string cacheKey, long key)
        {
            Task.Factory.StartNew(() =>
            {
                foreach (var server in Servers)
                {
                    _httpIssuer.IssueHttpRequest(server, HttpVerbModel.POST,
                        new Dictionary<string, string> { { "cacheKey", cacheKey }, { "key", key.ToString(CultureInfo.InvariantCulture) } }
                        , null, null, null);
                }
            });
        }
    }
}
