﻿using WSD.Consensus.Domain.Models;
using WSD.Consensus.Core.Contracts;
using WSD.Consensus.Domain.UnitOfWorks;
using System;
using System.Collections.Generic;
using WSD.Consensus.Common.Extensions;

namespace WSD.Consensus.BusinessLogic
{
    public class UserGameBusinessLogic : IUserGameBusinessLogic
    {
        private readonly IUnitOfWork _unitOfWork;
        public UserGameBusinessLogic(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public Guid? CreateGame(UserGameModel model)
        {
            try
            {
                return
                    _unitOfWork.UserGameRepository.CreateGame(model);
            }
            catch(Exception ex)
            {
                ex.LogException();
                return null;
            }
        }

        public UserGameModel FindGame(Guid id)
        {
            try
            {
                var res = _unitOfWork.UserGameRepository.FindGame(id);
                if(res == null)
                {
                    res = new UserGameModel
                    {
                        Message = new MethodMessage
                        {
                            CallWasSuccess = false,
                            FriendlyMessage = "Game Not Found",
                            DeveloperMessage = "Game Find Error"
                        }
                    };
                }
                else
                {
                    res.Message = new MethodMessage
                    {
                        FriendlyMessage = "Game Found",
                        CallWasSuccess = true
                    };
                }
                return res;
            }
            catch(Exception ex)
            {
                return new UserGameModel
                {
                    Message = new MethodMessage
                    {
                        DeveloperMessage = ex.ToString(),
                        CallWasSuccess = false,
                        FriendlyMessage = "Error Loading Game"
                    }
                };
            }
        }

        public TestResultModel MakeMove(Guid gameId, long medicalTestId)
        {
            try
            {
                var testRes = _unitOfWork.UserGameRepository.MakeMove(gameId, medicalTestId);
                if(testRes == null)
                {
                    testRes = new TestResultModel
                    {
                        Message = new MethodMessage
                        {
                            CallWasSuccess = false,
                            DeveloperMessage = "Error Making Move For MedicalTestId: " + medicalTestId + " GameId: " + gameId,
                            FriendlyMessage = "Illegal Move"
                        }
                    };
                }
                else
                {
                    testRes.Message = new MethodMessage
                    {
                        FriendlyMessage = "Move Made",
                        CallWasSuccess = true
                    };
                }
                return testRes;
            }
            catch(Exception ex)
            {
                return
                    new TestResultModel
                    {
                        Message = new MethodMessage
                        {
                            CallWasSuccess = false,
                            DeveloperMessage = ex.ToString(),
                            FriendlyMessage = "Illegal Move"
                        }
                    };
            }
        }

        public List<UserGameModel> SearchUserGames(UserGameSearch query, int page, int numGames)
        {
                return
                    _unitOfWork.UserGameRepository.SearchUserGames(query, page, numGames);          
        }

        public bool UpdateGame(UserGameModel model)
        {
            try
            {
                return _unitOfWork.UserGameRepository.UpdateGame(model);
            }
            catch(Exception ex)
            {
                ex.LogException();
                return false;
            }
        }
    }
}
