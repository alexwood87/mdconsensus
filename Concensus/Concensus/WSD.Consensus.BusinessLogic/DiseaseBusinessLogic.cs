﻿using WSD.Consensus.Domain.Models;
using WSD.Consensus.Core.Contracts;
using WSD.Consensus.Domain.UnitOfWorks;
using WSD.Consensus.Infrastructure.UnitOfWorks;
using System;
using System.Collections.Generic;
using WSD.Consensus.BusinessLogic.Helpers;
using WSD.Consensus.Common.Extensions;
using Microsoft.Extensions.Configuration;

namespace WSD.Consensus.BusinessLogic
{
    public class DiseaseBusinessLogic : IDiseaseBusinessLogic
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly RefreshHelper _refreshHelper;
        public DiseaseBusinessLogic(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _refreshHelper = new RefreshHelper();
        }

        public List<DiseaseModel> FindByScientificNameContains(string scientificName, string cultureName)
        {
            try
            {
                return
                    _unitOfWork.DiseaseRepository.FindByScientificNameContains(scientificName, cultureName);
            }
            catch (Exception ex)
            {
                ex.LogException();
                return null;
            }

        }

        public long CreateDisease(string scientificName, string commonName, string cultureName)
        {
            try
            {
                return
                    _unitOfWork.DiseaseRepository.CreateDisease(scientificName, commonName, cultureName).Id;
            }
            catch (Exception ex)
            {
                ex.LogException();
                return 0;
            }
        }
        public DiseaseModel EditDisease(long id, string scientificName, string commonName, string cultureName)
        {
            try{
                var res = _unitOfWork.DiseaseRepository.EditDisease(id, scientificName, commonName, cultureName);
                if(res == null)
                {
                    res = new DiseaseModel
                    {
                        Message = new MethodMessage
                        {
                             CallWasSuccess = false,
                             DeveloperMessage = "Error Editing Disease",
                             FriendlyMessage = "Disease Already Exists"
                        }
                    };
                }
                else
                {
                    res.Message = new MethodMessage
                    {
                        FriendlyMessage = "Successfully Editted Disease",
                        CallWasSuccess = true
                    };
                    _refreshHelper.ResetServers("Diseases", id);
                }
                return res;
            }
            catch(Exception ex)
            {
                return
                    new DiseaseModel
                    {
                        Message = new MethodMessage
                        {
                            CallWasSuccess = false,
                            FriendlyMessage = "Error Updating Disease",
                            DeveloperMessage = ex.ToString()
                        }
                    };
            }
        }
        public DiseaseModel FindById(long id)
        {
            try
            {
                var res = _unitOfWork.DiseaseRepository.FindById(id);
                if (res == null)
                {
                    res = new DiseaseModel
                    {
                        Message = new MethodMessage
                        {
                            CallWasSuccess = false,
                            DeveloperMessage = "Error Finding Disease",
                            FriendlyMessage = "Disease Not Found"
                        }
                    };
                }
                else
                {
                    res.Message = new MethodMessage
                    {
                        FriendlyMessage = "Disease Found",
                        CallWasSuccess = true
                    };
                }
                return res;
            }
            catch (Exception ex)
            {
                return
                    new DiseaseModel
                    {
                        Message = new MethodMessage
                        {
                            CallWasSuccess = false,
                            FriendlyMessage = "Error Finding Disease",
                            DeveloperMessage = ex.ToString()
                        }
                    };
            }
        }

        public DiseaseModel FindByScientificName(string scientificName, string cultureName)
        {
            try
            {
                var res = _unitOfWork.DiseaseRepository.FindByScientificName(scientificName,cultureName);
                if (res == null)
                {
                    res = new DiseaseModel
                    {
                        Message = new MethodMessage
                        {
                            CallWasSuccess = false,
                            DeveloperMessage = "Error Finding Disease",
                            FriendlyMessage = "Disease Not Found"
                        }
                    };
                }
                else
                {
                    res.Message = new MethodMessage
                    {
                        FriendlyMessage = "Disease Found",
                        CallWasSuccess = true
                    };
                }
                return res;
            }
            catch (Exception ex)
            {
                return
                    new DiseaseModel
                    {
                        Message = new MethodMessage
                        {
                            CallWasSuccess = false,
                            FriendlyMessage = "Error Finding Disease",
                            DeveloperMessage = ex.ToString()
                        }
                    };
            }
        }

        public List<DiseaseModel> LoadAll(out MethodMessage msg)
        {
            msg = new MethodMessage();
            try
            {
                var res = _unitOfWork.DiseaseRepository.LoadAll();                
                msg.CallWasSuccess = true;
                msg.FriendlyMessage = "Successfully Loaded Diseases";
                return res;
            }
            catch(Exception ex)
            {
                msg.DeveloperMessage = ex.ToString();
                msg.FriendlyMessage = "Error Loading Diseases";
                msg.CallWasSuccess = false;
                return null;
            }
        }
    }
}
