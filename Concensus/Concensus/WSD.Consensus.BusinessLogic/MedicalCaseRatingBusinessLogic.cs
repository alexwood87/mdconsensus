﻿using WSD.Consensus.Domain.Models;
using WSD.Consensus.Core.Contracts;
using WSD.Consensus.Domain.UnitOfWorks;
using System;
using System.Collections.Generic;
using WSD.Consensus.Common.Extensions;

namespace WSD.Consensus.BusinessLogic
{
    public class MedicalCaseRatingBusinessLogic : IMedicalCaseRatingBusinessLogic
    {
        private readonly IUnitOfWork _unitOfWork;
        public MedicalCaseRatingBusinessLogic(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork; ;
        }
        public Guid CreateMedicalCaseRating(long creatorId, Guid userGameId, Guid medicalCaseId, 
            decimal medicalCaseRatingValue)
        {
            try
            {
                return _unitOfWork.MedicalCaseRatingRepository.CreateMedicalCaseRating(creatorId, userGameId, 
                    medicalCaseId, medicalCaseRatingValue).Id;
            }
            catch(Exception ex)
            {
                ex.LogException();
                throw;            
            }
        }

        public CaseRatingModel EditMedicalCaseRating(Guid ratingId, long creatorId, Guid medicalCaseId, decimal medicalCaseRatingValue)
        {
            try
            {
                var res = _unitOfWork.MedicalCaseRatingRepository.EditMedicalCaseRating(ratingId, creatorId, medicalCaseId, medicalCaseRatingValue);
                res.Message = new MethodMessage
                {
                    CallWasSuccess = true,
                    FriendlyMessage = "Successfully Updated Medical Case Rating"
                };
                return res;
            }
            catch(Exception ex)
            {
                return
                    new CaseRatingModel
                    {
                        Message = new MethodMessage
                        {
                            CallWasSuccess = false,
                            DeveloperMessage = ex.ToString(),
                            FriendlyMessage = "An Error Ocurred Editing Your Rating"
                        }
                    };
            }
        }

        public List<CaseRatingModel> FindByCreatorId(long creatorId)
        {
            try {
                return _unitOfWork.MedicalCaseRatingRepository.FindByCreatorId(creatorId);
            }
            catch(Exception ex)
            {
                ex.LogException();
                return null;
            }
        }

        public List<CaseRatingModel> FindByMedicalCaseId(Guid medicalCaseId)
        {
            try
            {
                return _unitOfWork.MedicalCaseRatingRepository.FindByMedicalCaseId(medicalCaseId);
            }
            catch(Exception ex)
            {
                ex.LogException();
                return null;
            }
        }

        public CaseRatingModel FindByRatingId(Guid ratingId)
        {
            try
            {
                var res = _unitOfWork.MedicalCaseRatingRepository.FindByRatingId(ratingId);
                res.Message = new MethodMessage
                {
                    CallWasSuccess = true,
                    FriendlyMessage = "Rating Found" 
                };
                return res;
            }
            catch(Exception ex)
            {
                return
                    new CaseRatingModel
                    {
                        Message = new MethodMessage
                        {
                            CallWasSuccess = false,
                            DeveloperMessage = ex.ToString(),
                            FriendlyMessage = "Error Finding Rating"
                        }
                    };
            }
        }
    }
}
