﻿using WSD.Consensus.Domain.Models;
using WSD.Consensus.Core.Contracts;
using WSD.Consensus.Domain.UnitOfWorks;
using System;
using System.Collections.Generic;
using WSD.Consensus.Common.Extensions;
using WSD.Consensus.BusinessLogic.Helpers;

namespace WSD.Consensus.BusinessLogic
{
    public class SymptomBusinessLogic : ISymptomBusinessLogic
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly RefreshHelper _refreshHelper;
        public SymptomBusinessLogic(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _refreshHelper = new RefreshHelper();
        }
   
            
        public long CreateSymptom(string scientificName, string commonName, string cultureName)
        {
            try
            {
                return
                    _unitOfWork.SymptomRepository.CreateSymptom(scientificName, commonName, cultureName).Id;
            }
            catch(Exception ex)
            {
                ex.LogException();
                return 0;
            }
        }

        public SymptomModel EditSymptom(long id, string scientificName, string commonName, string cultureName)
        {
            try
            {
                var sym = _unitOfWork.SymptomRepository.EditSymptom(id, scientificName, commonName, cultureName);
                if(sym == null)
                {
                    sym = new SymptomModel
                    {
                        Message = new MethodMessage
                        {
                            CallWasSuccess = false,
                            DeveloperMessage = "Symptom Not Updated Or Not Found",
                            FriendlyMessage = "Error Updating Symptom"
                        }
                    };
                }
                else
                {
                    sym.Message = new MethodMessage
                    {
                        CallWasSuccess = true,
                        FriendlyMessage = "Symptom Updated"
                    };
                    _refreshHelper.ResetServers("Symptoms", id);
                }
                return sym;
            }
            catch (Exception ex)
            {
                return
                    new SymptomModel
                    {
                        Message = new MethodMessage
                        {
                            DeveloperMessage = ex.ToString(),
                            FriendlyMessage = "Error Updating Symptom",
                            CallWasSuccess = false
                        }
                    };
            }
        }

        public SymptomModel FindById(long id)
        {
            try
            {
                var sym = _unitOfWork.SymptomRepository.FindById(id);
                if (sym == null)
                {
                    sym = new SymptomModel
                    {
                        Message = new MethodMessage
                        {
                            CallWasSuccess = false,
                            DeveloperMessage = "Symptom Not Updated Or Not Found",
                            FriendlyMessage = "Error Updating Symptom"
                        }
                    };
                }
                else
                {
                    sym.Message = new MethodMessage
                    {
                        CallWasSuccess = true,
                        FriendlyMessage = "Symptom Found"
                    };
                }
                return sym;
            }
            catch (Exception ex)
            {
                return
                    new SymptomModel
                    {
                        Message = new MethodMessage
                        {
                            DeveloperMessage = ex.ToString(),
                            FriendlyMessage = "Error Finding Symptom",
                            CallWasSuccess = false
                        }
                    };
            }
        }

        public SymptomModel FindByScientificName(string scientificName, string cultureName)
        {
            try
            {
                var sym = _unitOfWork.SymptomRepository.FindByScientificName(scientificName,cultureName);
                if (sym == null)
                {
                    sym = new SymptomModel
                    {
                        Message = new MethodMessage
                        {
                            CallWasSuccess = false,
                            DeveloperMessage = "Symptom Not Updated Or Not Found",
                            FriendlyMessage = "Error Updating Symptom"
                        }
                    };
                }
                else
                {
                    sym.Message = new MethodMessage
                    {
                        CallWasSuccess = true,
                        FriendlyMessage = "Symptom Found"
                    };
                }
                return sym;
            }
            catch (Exception ex)
            {
                return
                    new SymptomModel
                    {
                        Message = new MethodMessage
                        {
                            DeveloperMessage = ex.ToString(),
                            FriendlyMessage = "Error Finding Symptom",
                            CallWasSuccess = false
                        }
                    };
            }
        }

        public List<SymptomModel> LoadAll(out MethodMessage msg)
        {
            try
            {
                var res = _unitOfWork.SymptomRepository.LoadAll();
                msg = new MethodMessage
                {
                    CallWasSuccess = true,
                    FriendlyMessage = "Successfully Loaded Symptoms"
                };
                return res;
            }
            catch(Exception ex)
            {
                msg = new MethodMessage
                {
                    CallWasSuccess = false,
                    DeveloperMessage = ex.ToString(),
                    FriendlyMessage = "Error Loading Symptoms"
                };
                return null;
            }
        }

        public List<SymptomModel> FindByScientificNameContains(string scientificName, string cultureName)
        {
            return
                _unitOfWork.SymptomRepository.FindByScientificNameContains(scientificName, cultureName);
        }
    }
}
