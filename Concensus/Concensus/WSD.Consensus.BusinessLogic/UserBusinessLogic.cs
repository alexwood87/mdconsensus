﻿using System;
using System.Collections.Generic;
using WSD.Consensus.Domain.Models;
using WSD.Consensus.Core.Contracts;
using WSD.Consensus.Domain.UnitOfWorks;
using WSD.Consensus.Common.Extensions;

namespace WSD.Consensus.BusinessLogic
{
    public class UserBusinessLogic : IUserBusinessLogic
    {
        private readonly IUnitOfWork _unitOfWork;
        public UserBusinessLogic(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public long CreateUser(string email, string password, string question, string answer, int userLevel, string cultureName)
        {
            try
            {
                return
                    _unitOfWork.UserRepository.CreateUser(email, password, question, answer, userLevel, cultureName).Id;
            }
            catch(Exception ex)
            {
                ex.LogException();
                return 0;
            }
        }

        public List<UserModel> SearchUsers(long? id, string email)
        {
            return _unitOfWork.UserRepository.SearchUsers(id, email);
        }

        public bool DeActivate(long id)
        {
            try
            {
                return
                    _unitOfWork.UserRepository.DeActivate(id);
            }
            catch (Exception ex)
            {
                ex.LogException();
                return false;
            }
        }

        public bool GenerateUserPassword(string answer, string email)
        {
            return _unitOfWork.UserRepository.GenerateUserPassword(answer, email);
        }

        public UserModel EditUser(long id, string email, string password, string question, string answer, int userLevel, string cultureName)
        {
            try
            {
                var user = _unitOfWork.UserRepository.EditUser(id, email, password, question, answer, userLevel, cultureName);
                if(user == null)
                {
                    user = new UserModel
                    {
                        Message = new MethodMessage
                        {
                            CallWasSuccess = false,
                            DeveloperMessage = "Error Editting User",
                            FriendlyMessage = "Error Editting User"
                        }
                    };
                }
                else
                {
                    user.Message = new MethodMessage
                    {
                        FriendlyMessage = "Account Updated",
                        CallWasSuccess = true
                    };
                }
                return user;
            }
            catch(Exception ex)
            {
                return
                    new UserModel
                    {
                        Message = new MethodMessage
                        {
                            CallWasSuccess = false,
                            DeveloperMessage = ex.ToString(),
                            FriendlyMessage = "Error Updating User"
                        }
                    };
            }
        }

        public UserModel ValidateUser(string email, string password)
        {
            try
            {
                var user = _unitOfWork.UserRepository.ValidateUser(email, password);
                if (user == null)
                {
                    user = new UserModel
                    {
                        Message = new MethodMessage
                        {
                            CallWasSuccess = false,
                            DeveloperMessage = "Error Authenticating User",
                            FriendlyMessage = "Account Credentitals Invalid"
                        }
                    };
                }
                else
                {
                    user.Message = new MethodMessage
                    {
                        FriendlyMessage = "Account Authenticated",
                        CallWasSuccess = true
                    };
                }
                return user;
            }
            catch (Exception ex)
            {
                return
                    new UserModel
                    {
                        Message = new MethodMessage
                        {
                            CallWasSuccess = false,
                            DeveloperMessage = ex.ToString(),
                            FriendlyMessage = "Error Authenticating Your Account"
                        }
                    };
            }
        }

        public bool ChangePassword(string email, string oldPassword, string newPassword)
        {
            return _unitOfWork.UserRepository.ChangePassword(email, oldPassword, newPassword);
        }
    }
}
