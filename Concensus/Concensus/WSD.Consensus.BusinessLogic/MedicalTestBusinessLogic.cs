﻿using System;
using System.Collections.Generic;
using WSD.Consensus.Common.Extensions;
using WSD.Consensus.Core.Contracts;
using WSD.Consensus.Domain.Models;
using WSD.Consensus.Domain.UnitOfWorks;

namespace WSD.Consensus.BusinessLogic
{
    public class MedicalTestBusinessLogic : IMedicalTestBusinessLogic
    {
        private readonly IUnitOfWork _unitOfWork;
        public MedicalTestBusinessLogic(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public List<MedicalTestModel> SearchMedicalTests(long? id, string name, string culture)
        {
            return _unitOfWork.MedicalTestRepository.SearchMedicalTests(id, name, culture);
        }

        public long CreateMedicalTest(Guid? medCaseId, MedicalTestModel model, TestResultModel result,
            DiseaseModel disease)
        {
            try
            {
                return _unitOfWork.MedicalTestRepository.CreateMedicalTest(medCaseId, model, result, disease).Id;
            }
            catch (Exception ex)
            {
                ex.LogException();
                return 0;
            }
        }

    }
}
