﻿using WSD.Consensus.Domain.Models;
using WSD.Consensus.Core.Contracts;
using WSD.Consensus.Domain.UnitOfWorks;
using System;
using System.Collections.Generic;
using WSD.Consensus.Common.Extensions;

namespace WSD.Consensus.BusinessLogic
{
    public class MedicalCaseBusinessLogic : IMedicalCaseBusinessLogic
    {
        private readonly IUnitOfWork _unitOfWork;
        public MedicalCaseBusinessLogic(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public Guid? CreateMedicalCase(MedicalCaseModel medicalCase)
        {
            try
            {
                var res = _unitOfWork.MedicalCaseRepository.CreateMedicalCase(medicalCase);
                if(res == null)
                {
                   //Add Logging

                }
                return res;
            }
            catch(Exception ex)
            {
                ex.LogException();
                return null;
            }
        }
        public MedicalCaseModel FindMedicalCase(Guid id)
        {
            try
            {
                var res = _unitOfWork.MedicalCaseRepository.FindMedicalCase(id);
                if(res == null)
                {
                    res = new MedicalCaseModel
                    {
                        Message = new MethodMessage
                        {
                            CallWasSuccess = false,
                            DeveloperMessage = "Medical Case Not Found",
                            FriendlyMessage = "Medical Case Not Found"
                        }
                    };
                }
                return res;
            }
            catch(Exception ex)
            {
                return
                    new MedicalCaseModel
                    {
                        Message = new MethodMessage
                        {
                            CallWasSuccess = false,
                            DeveloperMessage = ex.ToString(),
                            FriendlyMessage = "Medical Case Not Found"
                        }
                    };
            }
        }
        public bool EditMedicalCase(MedicalCaseModel medicalCase)
        {
            try{
                return _unitOfWork.MedicalCaseRepository.EditMedicalCase(medicalCase);
            }
            catch(Exception ex)
            {       
                ex.LogException();
                return false;
            }
        }
        public List<DiseaseDiagnosticResult> DiagnoseDisease(List<SymptomModel> symptoms, List<BodyPartModel> bodyParts, List<DemographicInformationModel> demographicInformation, double userWeight = 1, double caseWeight = 2, double demographicWeight = 1)
        {
            try
            {
                return
                    _unitOfWork.MedicalCaseRepository.DiagnoseDisease(symptoms, bodyParts, demographicInformation);
            }
            catch(Exception ex)
            {
                ex.LogException();
                return null;
            }
        }

        public List<MedicalCaseModel> Search(MedicalCaseSearch query, int numRecords, int page)
        {
            try
            {
                return _unitOfWork.MedicalCaseRepository.Search(query, numRecords, page);
            }
            catch(Exception ex)
            {
                ex.LogException();
                return null;
            }
        }
    }
}
