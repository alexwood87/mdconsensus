﻿using System;
using System.Collections.Generic;
using WSD.Consensus.Domain.UnitOfWorks;
using WSD.Consensus.Domain.Models;
using WSD.Consensus.Core.Contracts;
namespace WSD.Consensus.BusinessLogic
{
    public class MediaBusinessLogic : IMediaBusinessLogic
    {
        private readonly IUnitOfWork _unitOfWork;

        public MediaBusinessLogic(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public List<MediaModel> SearchMedia(MediaSearchQuery query)
        {
            return _unitOfWork.MediaRepository.SearchMedia(query);
        }

        public bool CreateMediaForUser(long userId, string fileName, FileType ft, string filePath)
        {
            return _unitOfWork.MediaRepository.CreateMediaForUser(userId, fileName, (int) ft, filePath);
        }

        public bool CreateMediaForBodyPart(long bodyPartId, string fileName, FileType ft, string filePath)
        {
            return _unitOfWork.MediaRepository.CreateMediaForBodyPart(bodyPartId, fileName, (int) ft, filePath);
        }

        public bool EditMediaForUser(long id, long userId, string fileName, FileType ft, string filePath)
        {
            return _unitOfWork.MediaRepository.EditMediaForUser(id, userId, fileName, (int) ft, filePath);
        }

        public bool EditMediaForBodyPart(Guid id, long bodyPartId, string fileName, FileType ft, string filePath)
        {
            return _unitOfWork.MediaRepository.EditMediaForBodyPart(id, bodyPartId, fileName, (int) ft, filePath);
        }
    }
}
