﻿using WSD.Consensus.Domain.Models;
using WSD.Consensus.Core.Contracts;
using WSD.Consensus.Domain.UnitOfWorks;
using System;
using System.Collections.Generic;
using WSD.Consensus.Common.Extensions;
using System.Transactions;

namespace WSD.Consensus.BusinessLogic
{
    public class UserRatingBusinessLogic : IUserRatingBusinessLogic
    {
        private readonly IUnitOfWork _unitOfWork;
        public UserRatingBusinessLogic(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public Guid CreateUserRating(long creatorId, long userToRankId, decimal userRatingValue)
        {
            var r = _unitOfWork.UserRatingRepository.CreateUserRating(creatorId, userToRankId, userRatingValue);
            if(r == null)
            {
                return
                    Guid.Empty;
            }
            return
                   r.Id;            
        }

        public UserRatingModel EditUserRating(Guid ratingId, long creatorId, long userToRankId, decimal userRatingValue)
        {
            var rating = _unitOfWork.UserRatingRepository.Find(ratingId);
            rating.CreatorId = creatorId;
            rating.UserRankedId = userToRankId;
            rating.UserRankingValue = userRatingValue;
            using (var transaction = new CommittableTransaction(new TimeSpan(0, 1, 0)))
            {
                try
                {
                    _unitOfWork.UserRatingRepository.Update(rating);
                    _unitOfWork.UserRatingRepository.Save();
                    transaction.Commit();
                    return
                        new UserRatingModel
                        {
                            Id = ratingId,
                            Creator = new UserModel
                            {
                                Id = creatorId
                            },
                            Rating = userRatingValue,
                            Weight = 1,
                            AppliedToUser = new UserModel
                            {
                                Id = userToRankId
                            }
                        };

                }
                catch (Exception ex)
                {
                    ex.LogException();
                    transaction.Rollback(ex);
                }
            }
            return null;
        }

        public List<UserRatingModel> FindByCreatorId(long creatorId)
        {
            try
            {
                return _unitOfWork.UserRatingRepository.FindByCreatorId(creatorId);
            }
            catch(Exception ex)
            {
                ex.LogException();
                return null;
            }
        }

        public List<UserRatingModel> FindByUserRankedId(long userRankedId)
        {
            try
            {
                return _unitOfWork.UserRatingRepository.FindByUserRankedId(userRankedId);
            }
            catch (Exception ex)
            {
                ex.LogException();
                return null;
            }
        }

        public UserRatingModel FindByUserRatingId(Guid ratingId)
        {
            try
            {
                var rating = _unitOfWork.UserRatingRepository.FindByUserRatingId(ratingId);
                if(rating == null)
                {
                    rating = new UserRatingModel
                    {
                        Message = new MethodMessage
                        {
                            CallWasSuccess = false,
                            DeveloperMessage = "Rating Was Null",
                            FriendlyMessage = "Error Finding Rating"
                        }
                    };
                }
                else
                {
                    rating.Message = new MethodMessage
                    {
                        CallWasSuccess = true,
                        FriendlyMessage = "Rating Found"
                    };
                }
                return rating;
            }
            catch (Exception ex)
            {
                return new UserRatingModel {
                    Message = new MethodMessage
                    {
                        CallWasSuccess = false,
                        DeveloperMessage = ex.ToString(),
                        FriendlyMessage = "Error Finding User Rating"
                    }
                };
            }
        }
    }
}
