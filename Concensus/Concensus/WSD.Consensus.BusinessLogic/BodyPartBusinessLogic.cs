﻿using WSD.Consensus.Domain.Models;
using System;
using System.Collections.Generic;
using WSD.Consensus.Domain.UnitOfWorks;
using WSD.Consensus.Core.Contracts;
using WSD.Consensus.BusinessLogic.Helpers;

namespace WSD.Consensus.BusinessLogic
{
    public class BodyPartBusinessLogic : IBodyPartBusinessLogic 
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly RefreshHelper _refreshHelper;
        public BodyPartBusinessLogic(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _refreshHelper = new RefreshHelper();
        }

        public List<BodyPartModel> FindByScientificNameContains(string sciName, string cultureName)
        {
            return
                _unitOfWork.BodyPartRepository.FindByScientificNameContains(sciName, cultureName);
        }
        public long CreateBodyPart(string scientificName, string commonName, string cultureName)
        {
            var msg = new MethodMessage();
            try {
                var res = _unitOfWork.BodyPartRepository.CreateBodyPart(scientificName, commonName, cultureName);
                
                if (res == null)
                {                   
                    msg.CallWasSuccess = false;
                    msg.DeveloperMessage = "Error Creating BodyPart";
                    msg.FriendlyMessage = "Body Part Already Exists";
                   
                }
                else
                {
                    msg.CallWasSuccess = true;
                    msg.FriendlyMessage = "Body Part Created Successful";
                    
                }
                
                return res.Id;
            }
            catch(Exception ex)
            {
                msg.DeveloperMessage = ex.ToString();
                msg.FriendlyMessage = "Error Creating New Body Part";
                msg.CallWasSuccess = false;
                return 0;
            }
        }

      
        public BodyPartModel EditBodyPart(long id, string scientificName, string commonName, string cultureName)
        {
            MethodMessage msg;
            try { 
                var res = _unitOfWork.BodyPartRepository.EditBodyPart(id,scientificName, commonName, cultureName);
                msg = new MethodMessage();
                if (res == null)
                {
                    msg.CallWasSuccess = false;
                    msg.DeveloperMessage = "Error Creating BodyPart";
                    msg.FriendlyMessage = "Body Part Already Exists";

                }
                else
                {
                    msg.CallWasSuccess = true;
                    msg.FriendlyMessage = "Body Part Created Successful";
                    _refreshHelper.ResetServers("BodyParts", res.Id);
                }
                res.Message = msg;
                return res;
            }
            catch(Exception ex)
            {
                msg = new MethodMessage
                {
                    DeveloperMessage = ex.ToString(),
                    FriendlyMessage = "Error Creating New Body Part"
                };
                return new BodyPartModel
                {
                    Message = msg
                };
            }
        }

        public BodyPartModel FindById(long id)
        {
            var bp = _unitOfWork.BodyPartRepository.FindById(id);
            if (bp == null)
            {
                    bp = new BodyPartModel
                    {
                        Message = new MethodMessage
                        {
                            CallWasSuccess = false,
                            DeveloperMessage = "Error Finding Id",
                            FriendlyMessage = "Body Part Not Found"
                        }
                    };
                }
                return bp;
            
        }

        public BodyPartModel FindByScientificName(string sciName, string cultureName)
        {
            var bp = _unitOfWork.BodyPartRepository.FindByScientificName(sciName, cultureName);
            if (bp == null)
            {
                bp = new BodyPartModel
                {
                    Message = new MethodMessage
                    {
                        CallWasSuccess = false,
                        DeveloperMessage = "Error Finding Id",
                        FriendlyMessage = "Body Part Not Found"
                    }
                };
            }
            return bp;
    }

        public List<BodyPartModel> LoadAll(out MethodMessage message)
        {
            message = new MethodMessage();
            try {
                message.CallWasSuccess = true;
                message.FriendlyMessage = "Successfully Loaded Body Parts";
                return
                    _unitOfWork.BodyPartRepository.LoadAll();
            }
            catch(Exception ex)
            {
                message.DeveloperMessage = ex.ToString();
                message.FriendlyMessage = "Error Loading All BodyParts";
                message.CallWasSuccess = false;
                return null;
            }
        }
    }
}
