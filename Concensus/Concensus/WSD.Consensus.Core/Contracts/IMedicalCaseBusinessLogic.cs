﻿using WSD.Consensus.Domain.Models;
using System;
using System.Collections.Generic;

namespace WSD.Consensus.Core.Contracts
{
    public interface IMedicalCaseBusinessLogic
    {
        Guid? CreateMedicalCase(MedicalCaseModel medicalCaseout);
        MedicalCaseModel FindMedicalCase(Guid id);
        bool EditMedicalCase(MedicalCaseModel medicalCase);
        List<DiseaseDiagnosticResult> DiagnoseDisease(List<SymptomModel> symptoms, List<BodyPartModel> bodyParts, List<DemographicInformationModel> demographicInformation, double userWeight = 1, double caseWeight = 2, double demographicWeight = 1);
        List<MedicalCaseModel> Search(MedicalCaseSearch query, int numRecords, int page);
    }
}
