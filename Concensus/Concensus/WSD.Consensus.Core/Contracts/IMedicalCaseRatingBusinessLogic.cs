﻿using WSD.Consensus.Domain.Models;
using System;
using System.Collections.Generic;

namespace WSD.Consensus.Core.Contracts
{
    public interface IMedicalCaseRatingBusinessLogic
    {
        Guid CreateMedicalCaseRating(long creatorId, Guid userGameId, Guid medicalCaseId, decimal medicalCaseRatingValue);
        CaseRatingModel EditMedicalCaseRating(Guid ratingId, long creatorId, Guid medicalCaseId, decimal medicalCaseRatingValue);
        List<CaseRatingModel> FindByCreatorId(long creatorId);
        CaseRatingModel FindByRatingId(Guid ratingId);
        List<CaseRatingModel> FindByMedicalCaseId(Guid medicalCaseId);
    }
}
