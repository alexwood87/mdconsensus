﻿using WSD.Consensus.Domain.Models;
using System;
using System.Collections.Generic;

namespace WSD.Consensus.Core.Contracts
{
    public interface IUserRatingBusinessLogic
    {
        Guid CreateUserRating(long creatorId, long userToRankId, decimal userRatingValue);
        UserRatingModel EditUserRating(Guid ratingId, long creatorId, long userToRankId, decimal userRatingValue);
        List<UserRatingModel> FindByCreatorId(long creatorId);
        UserRatingModel FindByUserRatingId(Guid ratingId);
        List<UserRatingModel> FindByUserRankedId(long userRankedId);
    }
}
