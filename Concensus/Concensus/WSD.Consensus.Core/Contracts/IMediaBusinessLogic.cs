﻿using System;
using System.Collections.Generic;
using WSD.Consensus.Domain.Models;

namespace WSD.Consensus.Core.Contracts
{
    public interface IMediaBusinessLogic
    {
        List<MediaModel> SearchMedia(MediaSearchQuery query);
        bool CreateMediaForUser(long userId, string fileName, FileType ft, string filePath);
        bool CreateMediaForBodyPart(long bodyPartId, string fileName, FileType ft, string filePath);
        bool EditMediaForUser(long id, long userId, string fileName, FileType ft, string filePath);
        bool EditMediaForBodyPart(Guid id, long bodyPartId, string fileName, FileType ft, string filePath);        
    }
}
