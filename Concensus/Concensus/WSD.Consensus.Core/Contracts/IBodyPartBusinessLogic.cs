﻿using WSD.Consensus.Domain.Models;
using System.Collections.Generic;

namespace WSD.Consensus.Core.Contracts
{
    public interface IBodyPartBusinessLogic
    {
        long CreateBodyPart(string scientificName, string commonName, string cultureName);
        BodyPartModel EditBodyPart(long id, string scientificName, string commonName, string cultureName);
        List<BodyPartModel> LoadAll(out MethodMessage msg);
        BodyPartModel FindById(long id);
        BodyPartModel FindByScientificName(string sciName, string cultureName);
        List<BodyPartModel> FindByScientificNameContains(string sciName, string cultureName);

    }
}
