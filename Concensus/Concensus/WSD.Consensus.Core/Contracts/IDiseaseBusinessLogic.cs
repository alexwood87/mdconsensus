﻿using WSD.Consensus.Domain.Models;
using System.Collections.Generic;

namespace WSD.Consensus.Core.Contracts
{
    public interface IDiseaseBusinessLogic
    {
        long CreateDisease(string scientificName, string commonName, string cultureName);
        DiseaseModel EditDisease(long id, string scientificName, string commonName, string cultureName);
        DiseaseModel FindById(long id);
        DiseaseModel FindByScientificName(string scientificName, string cultureName);
        List<DiseaseModel> LoadAll( out MethodMessage msg);
        List<DiseaseModel> FindByScientificNameContains(string scientificName, string cultureName);
    }
}
