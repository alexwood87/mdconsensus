﻿using WSD.Consensus.Domain.Models;
using System.Collections.Generic;

namespace WSD.Consensus.Core.Contracts
{
    public interface ISymptomBusinessLogic
    {
        long CreateSymptom(string scientificName, string commonName, string cultureName);
        SymptomModel EditSymptom(long id, string scientificName, string commonName, string cultureName);
        SymptomModel FindById(long id);
        SymptomModel FindByScientificName(string scientificName, string cultureName);
        List<SymptomModel> LoadAll(out MethodMessage msg);
        List<SymptomModel> FindByScientificNameContains(string scientificName, string cultureName);
    }
}
