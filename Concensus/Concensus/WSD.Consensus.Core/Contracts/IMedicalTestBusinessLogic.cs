﻿using System;
using System.Collections.Generic;
using WSD.Consensus.Domain.Models;

namespace WSD.Consensus.Core.Contracts
{
    public interface IMedicalTestBusinessLogic
    {
        List<MedicalTestModel> SearchMedicalTests(long? id, string name, string culture);
        long CreateMedicalTest(Guid? medCaseId, MedicalTestModel model, TestResultModel result, DiseaseModel disease);

    }
}
