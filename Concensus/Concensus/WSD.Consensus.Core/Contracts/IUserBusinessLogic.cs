﻿using System.Collections.Generic;
using WSD.Consensus.Domain.Models;
namespace WSD.Consensus.Core.Contracts
{
    public interface IUserBusinessLogic
    {
        long CreateUser(string email, string password, string question, string answer, int userLevel, string cultureName);
        UserModel EditUser(long id, string email, string password, string question, string answer, int userLevel, string cultureName);

        UserModel ValidateUser(string email, string password);
        bool ChangePassword(string email, string oldPassword, string newPassword);
        List<UserModel> SearchUsers(long? id, string email);
        bool DeActivate(long id);
        bool GenerateUserPassword(string answer, string email);
    }
}
