﻿using WSD.Consensus.Domain.Models;
using System;
using System.Collections.Generic;

namespace WSD.Consensus.Core.Contracts
{
    public interface IUserGameBusinessLogic
    {
        Guid? CreateGame(UserGameModel model);
        bool UpdateGame(UserGameModel model);
        UserGameModel FindGame(Guid id);
        TestResultModel MakeMove(Guid gameId, long medicalTestId);
        List<UserGameModel> SearchUserGames(UserGameSearch query, int page, int numGames);
    }
}
