﻿using System.Collections.Generic;
using WSD.Consensus.Presentation.Web.Authentication;

namespace WSD.Consensus.ConsensusApi.Helpers.Authentication
{
    public class ConcensusApiAuthorizedValidator : ConcensusAuthorizedValidator
    {
        public ConcensusApiAuthorizedValidator() : 
            base(new List<KeyValuePair<string, string>> { 
            new KeyValuePair<string, string>("concensususer", "2poiuytrewqqwertyuiop1"),
             new KeyValuePair<string, string>("concensuseduuser", "2lkjhgfdsaasdfghjkl1"),
              new KeyValuePair<string, string>("concensusmobileuser", "1mnbvcxzzxcvbnm2")
            })
        {
        }
    }
}
