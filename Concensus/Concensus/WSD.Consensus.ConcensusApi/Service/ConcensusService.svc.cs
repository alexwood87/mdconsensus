﻿using WSD.Consensus.Configuration;
using System;
using System.Collections.Generic;
using WSD.Consensus.Core.Contracts;
using WSD.Consensus.Domain.Models;
using System.Configuration;
using WSD.Consensus.ConsensusApi.Helpers.Authentication;
using WSD.Consensus.Configuration.Helpers;
using System.ServiceModel;

namespace WSD.Consensus.WebServices.ConsensusService
{
   
    public class ConsensusService : IConsensusService
    {       
       
        public ConsensusService()
        {
            ValidateClient();        
        }

        private readonly ConcensusApiAuthorizedValidator _validator = new ConcensusApiAuthorizedValidator();

        private void ValidateClient()
        {
            var validator = new ConcensusApiAuthorizedValidator();
            if (ConfigurationManager.AppSettings["DevMode"] == "Production")
            {
                try
                {                   
                    var authHeader = OperationContext.Current.IncomingMessageHeaders.GetHeader<string>("Authentication",
                        "AuthenticationHeader");
                    if(authHeader == null)
                    {
                        throw new UnauthorizedAccessException("Username Or Password Not Found");
                    }
                    var parts = authHeader.Split(':');
                    if(parts.Length <= 1)
                    {
                        throw new UnauthorizedAccessException("Username Or Password Not Found");
                    }
                    _validator.Validate(parts[0], parts[1]);
                }
                catch(Exception ex)
                {
                    throw new UnauthorizedAccessException("Username Or Password Not Found", ex);
                }
               
            }
         }

        public bool ChangePassword(string email, string oldPassword, string newPassword)
        {
            return DependencyInjectorModule.NewBusinessLogicInstance<IUserBusinessLogic>()
                .ChangePassword(email, oldPassword, newPassword);
        }

        public List<UserModel> SearchUsers(long? id, string email)
        {
            return DependencyInjectorModule.NewBusinessLogicInstance<IUserBusinessLogic>().SearchUsers(id, email);
        }

        public bool GenerateUsersPassword(string answer, string email)
        {
            return DependencyInjectorModule.NewBusinessLogicInstance<IUserBusinessLogic>().GenerateUserPassword(answer, email);
        }

        public List<SymptomModel> SearchSymptoms(MetaDataSearchQuery query)
        {
            return
                ConsensusCache.SearchSymptomsByQuery(query);
        }

        public List<BodyPartModel> SearchBodyParts(MetaDataSearchQuery query)
        {
            return
                ConsensusCache.SearchBodyPartsByQuery(query);
        }

        public List<DiseaseModel> SearchDiseases(MetaDataSearchQuery query)
        {
            return
                ConsensusCache.SearchDiseasesByQuery(query);
        }

        public long CreateBodyPart(string scientificName, string commonName, string cultureName)
        {
            return
                DependencyInjectorModule.NewBusinessLogicInstance<IBodyPartBusinessLogic>()
                    .CreateBodyPart(scientificName, commonName, cultureName);
        }

        public BodyPartModel EditBodyPart(long id, string scientificName, string commonName, string cultureName)
        {
            return
                DependencyInjectorModule.NewBusinessLogicInstance<IBodyPartBusinessLogic>()
                    .EditBodyPart(id, scientificName, commonName, cultureName);
        }

        public long CreateDisease(string scientificName, string commonName, string cultureName)
        {
            return
                DependencyInjectorModule.NewBusinessLogicInstance<IDiseaseBusinessLogic>().CreateDisease(scientificName,commonName,cultureName);
        }

        public Guid? CreateMedicalCase(MedicalCaseModel medicalCaseout)
        {
            return
                DependencyInjectorModule.NewBusinessLogicInstance<IMedicalCaseBusinessLogic>().CreateMedicalCase(medicalCaseout);
        }

        public MedicalCaseModel FindMedicalCase(Guid id)
        {
            return
                DependencyInjectorModule.NewBusinessLogicInstance<IMedicalCaseBusinessLogic>().FindMedicalCase(id);
        }

        public bool EditMedicalCase(MedicalCaseModel medicalCase)
        {
            return
                DependencyInjectorModule.NewBusinessLogicInstance<IMedicalCaseBusinessLogic>().EditMedicalCase(medicalCase);
        }

        public List<DiseaseDiagnosticResult> DiagnoseDisease(List<SymptomModel> symptoms, List<BodyPartModel> bodyParts, List<DemographicInformationModel> demographicInformation)
        {
            return 
                DependencyInjectorModule.NewBusinessLogicInstance<IMedicalCaseBusinessLogic>().DiagnoseDisease(symptoms,bodyParts,demographicInformation);
        }

        public List<MedicalCaseModel> SearchMedicalCases(MedicalCaseSearch query, int numRecords, int page)
        {
            return
                DependencyInjectorModule.NewBusinessLogicInstance<IMedicalCaseBusinessLogic>().Search(query, numRecords, page);
        }

        public Guid CreateMedicalCaseRating(long creatorId, Guid userGameId, Guid medicalCaseId,
            decimal medicalCaseRatingValue)
        {
            return
                DependencyInjectorModule.NewBusinessLogicInstance<IMedicalCaseRatingBusinessLogic>()
                    .CreateMedicalCaseRating(creatorId, userGameId, medicalCaseId, medicalCaseRatingValue);
        }

        public CaseRatingModel EditMedicalCaseRating(Guid ratingId, long creatorId, Guid medicalCaseId, decimal medicalCaseRatingValue)
        {
            return
                DependencyInjectorModule.NewBusinessLogicInstance<IMedicalCaseRatingBusinessLogic>()
                    .EditMedicalCaseRating(ratingId, creatorId, medicalCaseId, medicalCaseRatingValue);

        }

        public List<CaseRatingModel> FindMedicalCaseRatingsByCreatorId(long creatorId)
        {
            return
                DependencyInjectorModule.NewBusinessLogicInstance<IMedicalCaseRatingBusinessLogic>().FindByCreatorId(creatorId);

        }

        public CaseRatingModel FindMedicalCaseByRatingId(Guid ratingId)
        {
            return
                DependencyInjectorModule.NewBusinessLogicInstance<IMedicalCaseRatingBusinessLogic>().FindByRatingId(ratingId);

        }

        public List<CaseRatingModel> FindMedicalCaseRatingsByMedicalCaseId(Guid medicalCaseId)
        {
            return
                DependencyInjectorModule.NewBusinessLogicInstance<IMedicalCaseRatingBusinessLogic>().FindByMedicalCaseId(medicalCaseId);


        }

        public long CreateSymptom(string scientificName, string commonName, string cultureName)
        {
            return
                DependencyInjectorModule.NewBusinessLogicInstance<ISymptomBusinessLogic>()
                    .CreateSymptom(scientificName, commonName, cultureName);
        }

        public SymptomModel EditSymptom(long id, string scientificName, string commonName, string cultureName)
        {
            return
                DependencyInjectorModule.NewBusinessLogicInstance<ISymptomBusinessLogic>()
                    .EditSymptom(id, scientificName, commonName, cultureName);
        }

        public long CreateUser(string email, string password, string question, string answer, int userLevel, string cultureName)
        {
            return
                DependencyInjectorModule.NewBusinessLogicInstance<IUserBusinessLogic>()
                    .CreateUser(email, password, question, answer, userLevel, cultureName);
        }

        public UserModel EditUser(long id, string email, string password, string question, string answer, int userLevel, string cultureName)
        {
            return
                DependencyInjectorModule.NewBusinessLogicInstance<IUserBusinessLogic>()
                    .EditUser(id, email, password, question, answer, userLevel, cultureName);
        }

        public UserModel ValidateUser(string email, string password)
        {
            return
                DependencyInjectorModule.NewBusinessLogicInstance<IUserBusinessLogic>().ValidateUser(email, password);
        }

        public bool DeActivate(long id)
        {
            return
                DependencyInjectorModule.NewBusinessLogicInstance<IUserBusinessLogic>().DeActivate(id);
        }

        public Guid? CreateGame(UserGameModel model)
        {
            return
                DependencyInjectorModule.NewBusinessLogicInstance<IUserGameBusinessLogic>().CreateGame(model);
        }

        public bool UpdateGame(UserGameModel model)
        {
            return
                DependencyInjectorModule.NewBusinessLogicInstance<IUserGameBusinessLogic>().UpdateGame(model);
        }

        public UserGameModel FindGame(Guid id)
        {
            return
                DependencyInjectorModule.NewBusinessLogicInstance<IUserGameBusinessLogic>().FindGame(id);
        }

        public TestResultModel MakeMove(Guid gameId, long medicalTestId)
        {
            return
                DependencyInjectorModule.NewBusinessLogicInstance<IUserGameBusinessLogic>().MakeMove(gameId, medicalTestId);
        }

        public List<MedicalTestModel> SearchMedicalTests(long? id, string name, string culture)
        {
            return DependencyInjectorModule.NewBusinessLogicInstance<IMedicalTestBusinessLogic>().SearchMedicalTests(id, name, culture);
        }
        public Guid RateGame(CaseRatingModel model)
        {
            return
                DependencyInjectorModule.NewBusinessLogicInstance<IMedicalCaseRatingBusinessLogic>().CreateMedicalCaseRating(
                    model.Creator.Id, model.Game.Id, model.Game.MedicalCase.Id, model.Rating
                    );
        }
        public long CreateMedicalTest(Guid? medCaseId, MedicalTestModel model, TestResultModel result, DiseaseModel disease)
        {
            return DependencyInjectorModule.NewBusinessLogicInstance<IMedicalTestBusinessLogic>()
                .CreateMedicalTest(medCaseId, model, result, disease);
        }

        public List<UserGameModel> SearchUserGames(UserGameSearch query, int page, int numGames)
        {
            return
                DependencyInjectorModule.NewBusinessLogicInstance<IUserGameBusinessLogic>().SearchUserGames(query, page, numGames);
        }

        public Guid CreateUserRating(long creatorId, long userToRankId, decimal userRatingValue)
        {
            return
                DependencyInjectorModule.NewBusinessLogicInstance<IUserRatingBusinessLogic>()
                    .CreateUserRating(creatorId, userToRankId, userRatingValue);
        }

        public UserRatingModel EditUserRating(Guid ratingId, long creatorId, long userToRankId, decimal userRatingValue)
        {
            return
                DependencyInjectorModule.NewBusinessLogicInstance<IUserRatingBusinessLogic>()
                    .EditUserRating(ratingId,creatorId, userToRankId, userRatingValue);
             
        }

        public List<UserRatingModel> FindUserRatingByCreatorId(long creatorId)
        {
            return
                DependencyInjectorModule.NewBusinessLogicInstance<IUserRatingBusinessLogic>()
                    .FindByCreatorId(creatorId);
       
        }

        public UserRatingModel FindUserRatingByUserRatingId(Guid ratingId)
        {
            return
                DependencyInjectorModule.NewBusinessLogicInstance<IUserRatingBusinessLogic>()
                    .FindByUserRatingId(ratingId);
       
        }

        public List<UserRatingModel> FindUserRatingByUserRankedId(long userRankedId)
        {
            return
              DependencyInjectorModule.NewBusinessLogicInstance<IUserRatingBusinessLogic>()
                  .FindByUserRankedId(userRankedId);
       
        }
        public List<MediaModel> SearchMedia(MediaSearchQuery query)
        {
            return DependencyInjectorModule.NewBusinessLogicInstance<IMediaBusinessLogic>().SearchMedia(query);
        }

        public bool CreateMediaForUser(long userId, string fileName, FileType ft, string filePath)
        {
            return DependencyInjectorModule.NewBusinessLogicInstance<IMediaBusinessLogic>()
                .CreateMediaForUser(userId, fileName, ft, filePath);
        }

        public bool CreateMediaForBodyPart(long bodyPartId, string fileName, FileType ft, string filePath)
        {
            return DependencyInjectorModule.NewBusinessLogicInstance<IMediaBusinessLogic>()
                .CreateMediaForBodyPart(bodyPartId, fileName, ft, filePath);
        }

        public bool EditMediaForUser(long id, long userId, string fileName, FileType ft, string filePath)
        {
            return DependencyInjectorModule.NewBusinessLogicInstance<IMediaBusinessLogic>()
                .EditMediaForUser(id, userId, fileName, ft, filePath);
        }

        public bool EditMediaForBodyPart(Guid id, long bodyPartId, string fileName, FileType ft, string filePath)
        {
            return DependencyInjectorModule.NewBusinessLogicInstance<IMediaBusinessLogic>()
                .EditMediaForBodyPart(id, bodyPartId, fileName, ft, filePath);
        }

        public Guid RateUser(long userIdToRate, long raterId, decimal rating)
        {
            return DependencyInjectorModule.NewBusinessLogicInstance<IUserRatingBusinessLogic>().CreateUserRating(raterId, userIdToRate, rating);
        }
    }
}
