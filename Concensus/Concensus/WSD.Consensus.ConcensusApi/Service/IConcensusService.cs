﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using WSD.Consensus.Domain.Models;

namespace WSD.Consensus.WebServices.ConsensusService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IConsensusService
    {
        [OperationContract]
        bool ChangePassword(string email, string oldPassword, string newPassword);
        [OperationContract]
        List<UserModel> SearchUsers(long? id, string email); 
        [OperationContract]
        bool GenerateUsersPassword(string answer, string email);
        [OperationContract]
        List<SymptomModel> SearchSymptoms(MetaDataSearchQuery query);
        [OperationContract]
        List<BodyPartModel> SearchBodyParts(MetaDataSearchQuery query);
        [OperationContract]
        List<DiseaseModel> SearchDiseases(MetaDataSearchQuery query);
        [OperationContract]
        long CreateBodyPart(string scientificName, string commonName, string cultureName);
        [OperationContract]
        BodyPartModel EditBodyPart(long id, string scientificName, string commonName, string cultureName);
        [OperationContract]
        long CreateDisease(string scientificName, string commonName, string cultureName);
        [OperationContract]
        Guid? CreateMedicalCase(MedicalCaseModel medicalCaseout);
        [OperationContract]
        MedicalCaseModel FindMedicalCase(Guid id);
        [OperationContract] 
        bool EditMedicalCase(MedicalCaseModel medicalCase);
        [OperationContract] 
        List<DiseaseDiagnosticResult> DiagnoseDisease(List<SymptomModel> symptoms, List<BodyPartModel> bodyParts, List<DemographicInformationModel> demographicInformation);
        [OperationContract]
        List<MedicalCaseModel> SearchMedicalCases(MedicalCaseSearch query, int numRecords, int page);
        [OperationContract] 
        Guid CreateMedicalCaseRating(long creatorId, Guid userGameId, Guid medicalCaseId, decimal medicalCaseRatingValue);
        [OperationContract]
        CaseRatingModel EditMedicalCaseRating(Guid ratingId, long creatorId, Guid medicalCaseId, decimal medicalCaseRatingValue);
        [OperationContract] 
        CaseRatingModel FindMedicalCaseByRatingId(Guid ratingId);
        [OperationContract]
        long CreateSymptom(string scientificName, string commonName, string cultureName);
        [OperationContract]
        SymptomModel EditSymptom(long id, string scientificName, string commonName, string cultureName);
        [OperationContract] 
        long CreateUser(string email, string password, string question, string answer, int userLevel, string cultureName);
        [OperationContract] 
        UserModel EditUser(long id, string email, string password, string question, string answer, int userLevel, string cultureName);
        [OperationContract]
        UserModel ValidateUser(string email, string password);
        [OperationContract]
        bool DeActivate(long id);
        [OperationContract]
        Guid? CreateGame(UserGameModel model);
        [OperationContract] 
        bool UpdateGame(UserGameModel model);
        [OperationContract]
        UserGameModel FindGame(Guid id);
        [OperationContract]
        TestResultModel MakeMove(Guid gameId, long medicalTestId);
        [OperationContract]
        List<MedicalTestModel> SearchMedicalTests(long? id, string name, string culture);
        [OperationContract]
        long CreateMedicalTest(Guid? medCaseId, MedicalTestModel model, TestResultModel result, DiseaseModel disease);
        [OperationContract]
        Guid RateUser(long userIdToRate, long raterId, decimal rating);

        [OperationContract]
        Guid RateGame(CaseRatingModel model);
        [OperationContract]
        List<UserGameModel> SearchUserGames(UserGameSearch query, int page, int numGames);
        Guid CreateUserRating(long creatorId, long userToRankId, decimal userRatingValue);
        [OperationContract]
        UserRatingModel EditUserRating(Guid ratingId, long creatorId, long userToRankId, decimal userRatingValue);
        [OperationContract]
        List<UserRatingModel> FindUserRatingByCreatorId(long creatorId);
        [OperationContract]
        UserRatingModel FindUserRatingByUserRatingId(Guid ratingId);
        [OperationContract]
        List<UserRatingModel> FindUserRatingByUserRankedId(long userRankedId);
        [OperationContract]
        List<MediaModel> SearchMedia(MediaSearchQuery query);
        [OperationContract]
        bool CreateMediaForUser(long userId, string fileName, FileType ft, string filePath);
        [OperationContract]
        bool CreateMediaForBodyPart(long bodyPartId, string fileName, FileType ft, string filePath);
        [OperationContract]
        bool EditMediaForUser(long id, long userId, string fileName, FileType ft, string filePath);
        [OperationContract]
        bool EditMediaForBodyPart(Guid id, long bodyPartId, string fileName, FileType ft, string filePath);          
          
    }

    
}
