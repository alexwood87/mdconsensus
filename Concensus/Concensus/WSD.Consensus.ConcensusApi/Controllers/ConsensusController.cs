﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using WSD.Consensus.Configuration;
using WSD.Consensus.ConsensusApi.Helpers.Authentication;
using WSD.Consensus.Configuration.Helpers;
using WSD.Consensus.Core.Contracts;
using WSD.Consensus.Domain.Models;
using WSD.Consensus.Presentation.Web.Authentication.Headers;

namespace WSD.Consensus.ConsensusApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorization(false, typeof(ConcensusApiAuthorizedValidator))]
    public class ConsensusController : ControllerBase
    {
        private readonly IConsensusCache _consensusCache;
        private readonly IMediaBusinessLogic _mediaBusinessLogic;
        private readonly IMedicalCaseBusinessLogic _medicalCaseBusinessLogic;
        private readonly IMedicalCaseRatingBusinessLogic _medicalCaseRatingBusinessLogic;
        private readonly IUserBusinessLogic _userBusinessLogic;
        private readonly IUserGameBusinessLogic _userGameBusinessLogic;
        private readonly ISymptomBusinessLogic _symptomBusinessLogic;
        private readonly IUserRatingBusinessLogic _userRatingBusinessLogic;
        private readonly IMedicalTestBusinessLogic _medicalTestBusinessLogic;
        private readonly IBodyPartBusinessLogic _bodyPartBusinessLogic;
        private readonly IDiseaseBusinessLogic _diseaseBusinessLogic;
        public ConsensusController(IConsensusCache consensusCache, IUserBusinessLogic userBusinessLogic,
            IUserGameBusinessLogic userGameBusinessLogic, IUserRatingBusinessLogic userRatingBusinessLogic,
            IMedicalCaseBusinessLogic medicalCaseBusinessLogic, IMedicalCaseRatingBusinessLogic medicalCaseRatingBusinessLogic,
            IMedicalTestBusinessLogic medicalTestBusinessLogic, ISymptomBusinessLogic symptomBusinessLogic,
            IMediaBusinessLogic mediaBusinessLogic, IBodyPartBusinessLogic bodyPartBusinessLogic, IDiseaseBusinessLogic diseaseBusinessLogic)
        {
            _consensusCache = consensusCache;
            _bodyPartBusinessLogic = bodyPartBusinessLogic;
            _diseaseBusinessLogic = diseaseBusinessLogic;
            _mediaBusinessLogic = mediaBusinessLogic;
            _medicalCaseBusinessLogic = medicalCaseBusinessLogic;
            _medicalCaseRatingBusinessLogic = medicalCaseRatingBusinessLogic;
            _medicalTestBusinessLogic = medicalTestBusinessLogic;
            _symptomBusinessLogic = symptomBusinessLogic;
            _userBusinessLogic = userBusinessLogic;
            _userGameBusinessLogic = userGameBusinessLogic;
            _userRatingBusinessLogic = userRatingBusinessLogic;
        }

        public IActionResult ChangePassword(string email, string oldPassword, string newPassword)
        {
            return
                MapToActionResult(() =>
                 _userBusinessLogic
                .ChangePassword(email, oldPassword, newPassword));
        }

        public delegate object Run();

        private static IActionResult MapToActionResult(Run run)
        {
            try
            {
                return
                    new JsonResult(new
                    {
                        Data = run.Invoke(),
                        Success = true
                    });
            }
            catch(Exception ex)
            {
                return new JsonResult(new
                {
                    Success = false,
                    Error = ex.Message
                });
            }
        }

        public IActionResult SearchUsers(long? id, string email)
        {
            return
                    MapToActionResult(() =>
                 _userBusinessLogic.SearchUsers(id, email));
        }

        public IActionResult GenerateUsersPassword(string answer, string email)
        {
            return
                    MapToActionResult(() =>
                            _userBusinessLogic
                                .GenerateUserPassword(answer, email));
        }

        public IActionResult SearchSymptoms(MetaDataSearchQuery query)
        {
            return MapToActionResult(() =>
                _consensusCache.SearchSymptomsByQuery(query));
        }

        public IActionResult SearchBodyParts(MetaDataSearchQuery query)
        {
            return MapToActionResult(() =>
                    _consensusCache.SearchBodyPartsByQuery(query));
        }

        public IActionResult SearchDiseases(MetaDataSearchQuery query)
        {
            return
                    MapToActionResult(() =>
                        _consensusCache.SearchDiseasesByQuery(query));
        }

        public IActionResult CreateBodyPart(string scientificName, string commonName, string cultureName)
        {
            return MapToActionResult(() =>
                _bodyPartBusinessLogic
                    .CreateBodyPart(scientificName, commonName, cultureName));
        }

        public IActionResult EditBodyPart(long id, string scientificName, string commonName, string cultureName)
        {
            return MapToActionResult(() =>
                _bodyPartBusinessLogic
                    .EditBodyPart(id, scientificName, commonName, cultureName));
        }

        public IActionResult CreateDisease(string scientificName, string commonName, string cultureName)
        {
            return MapToActionResult(() =>
                    _diseaseBusinessLogic
                    .CreateDisease(scientificName, commonName, cultureName));
        }

        public IActionResult CreateMedicalCase(MedicalCaseModel medicalCaseout)
        {
            return MapToActionResult(() =>
                _medicalCaseBusinessLogic
                    .CreateMedicalCase(medicalCaseout));
        }

        public IActionResult FindMedicalCase(Guid id)
        {
            return MapToActionResult(() =>
                _medicalCaseBusinessLogic.FindMedicalCase(id));
        }

        public IActionResult EditMedicalCase(MedicalCaseModel medicalCase)
        {
            return MapToActionResult(() =>
                _medicalCaseBusinessLogic.EditMedicalCase(medicalCase));
        }

        public IActionResult DiagnoseDisease(List<SymptomModel> symptoms, List<BodyPartModel> bodyParts, List<DemographicInformationModel> demographicInformation)
        {
            return MapToActionResult(() =>
                _medicalCaseBusinessLogic
                    .DiagnoseDisease(symptoms, bodyParts, demographicInformation));
        }

        public IActionResult SearchMedicalCases(MedicalCaseSearch query, int numRecords, int page)
        {
            return MapToActionResult(() =>
                    _medicalCaseBusinessLogic
                    .Search(query, numRecords, page));
        }

        public IActionResult CreateMedicalCaseRating(long creatorId, Guid userGameId, Guid medicalCaseId,
            decimal medicalCaseRatingValue)
        {
            return MapToActionResult(() =>
                _medicalCaseRatingBusinessLogic
                    .CreateMedicalCaseRating(creatorId, userGameId, medicalCaseId, medicalCaseRatingValue));
        }

        public IActionResult EditMedicalCaseRating(Guid ratingId, long creatorId, Guid medicalCaseId, decimal medicalCaseRatingValue)
        {
            return MapToActionResult(() =>
                _medicalCaseRatingBusinessLogic
                    .EditMedicalCaseRating(ratingId, creatorId, medicalCaseId, medicalCaseRatingValue));

        }

        public IActionResult FindMedicalCaseRatingsByCreatorId(long creatorId)
        {
            return MapToActionResult(() =>
                _medicalCaseRatingBusinessLogic.FindByCreatorId(creatorId));

        }

        public IActionResult FindMedicalCaseByRatingId(Guid ratingId)
        {
            return MapToActionResult(() =>
                _medicalCaseRatingBusinessLogic.FindByRatingId(ratingId));

        }

        public IActionResult FindMedicalCaseRatingsByMedicalCaseId(Guid medicalCaseId)
        {
            return MapToActionResult(() =>
                _medicalCaseRatingBusinessLogic
                .FindByMedicalCaseId(medicalCaseId));


        }

        public IActionResult CreateSymptom(string scientificName, string commonName, string cultureName)
        {
            return MapToActionResult(() =>
                _symptomBusinessLogic
                    .CreateSymptom(scientificName, commonName, cultureName));
        }

        public IActionResult EditSymptom(long id, string scientificName, string commonName, string cultureName)
        {
            return MapToActionResult(() =>
                _symptomBusinessLogic
                    .EditSymptom(id, scientificName, commonName, cultureName));
        }

        public IActionResult CreateUser(string email, string password, string question, string answer, int userLevel, string cultureName)
        {
            return MapToActionResult(() =>
                _userBusinessLogic
                    .CreateUser(email, password, question, answer, userLevel, cultureName));
        }

        public IActionResult EditUser(long id, string email, string password, string question, string answer, int userLevel, string cultureName)
        {
            return MapToActionResult(() =>
                _userBusinessLogic
                    .EditUser(id, email, password, question, answer, userLevel, cultureName));
        }

        public IActionResult ValidateUser(string email, string password)
        {
            return MapToActionResult(() =>
                _userBusinessLogic.ValidateUser(email, password));
        }

        public IActionResult DeActivate(long id)
        {
            return MapToActionResult(() =>
                _userBusinessLogic.DeActivate(id));
        }

        public IActionResult CreateGame(UserGameModel model)
        {
            return MapToActionResult(() =>
                _userGameBusinessLogic.CreateGame(model));
        }

        public IActionResult UpdateGame(UserGameModel model)
        {
            return MapToActionResult(() =>
                _userGameBusinessLogic.UpdateGame(model));
        }

        public IActionResult FindGame(Guid id)
        {
            return MapToActionResult(() =>
                _userGameBusinessLogic.FindGame(id));
        }

        public IActionResult MakeMove(Guid gameId, long medicalTestId)
        {
            return MapToActionResult(() =>
                _userGameBusinessLogic.MakeMove(gameId, medicalTestId));
        }

        public IActionResult SearchMedicalTests(long? id, string name, string culture)
        {
            return
                    MapToActionResult(() =>
                        _medicalTestBusinessLogic
                        .SearchMedicalTests(id, name, culture));
        }
        public IActionResult RateGame(CaseRatingModel model)
        {
            return
                    MapToActionResult(() =>
                        _medicalCaseRatingBusinessLogic.CreateMedicalCaseRating(
                            model.Creator.Id, model.Game.Id, model.Game.MedicalCase.Id, model.Rating
                    ));
        }
        public IActionResult CreateMedicalTest(Guid? medCaseId, MedicalTestModel model, TestResultModel result, DiseaseModel disease)
        {
            return
                    MapToActionResult(() =>
                        _medicalTestBusinessLogic
                        .CreateMedicalTest(medCaseId, model, result, disease));
        }

        public IActionResult SearchUserGames(UserGameSearch query, int page, int numGames)
        {
            return MapToActionResult(() =>
                    _userGameBusinessLogic
                    .SearchUserGames(query, page, numGames));
        }

        public IActionResult CreateUserRating(long creatorId, long userToRankId, decimal userRatingValue)
        {
            return MapToActionResult(() =>
                _userRatingBusinessLogic
                        .CreateUserRating(creatorId, userToRankId, userRatingValue));
        }

        public IActionResult EditUserRating(Guid ratingId, long creatorId, long userToRankId, decimal userRatingValue)
        {
            return MapToActionResult(() =>
                _userRatingBusinessLogic
                    .EditUserRating(ratingId, creatorId, userToRankId, userRatingValue));

        }

        public IActionResult FindUserRatingByCreatorId(long creatorId)
        {
            return MapToActionResult(() =>
                _userRatingBusinessLogic
                    .FindByCreatorId(creatorId));

        }

        public IActionResult FindUserRatingByUserRatingId(Guid ratingId)
        {
            return MapToActionResult(() =>
                _userRatingBusinessLogic
                    .FindByUserRatingId(ratingId));

        }

        public IActionResult FindUserRatingByUserRankedId(long userRankedId)
        {
            return MapToActionResult(() =>
                _userRatingBusinessLogic
                    .FindByUserRankedId(userRankedId));

        }
        public IActionResult SearchMedia(MediaSearchQuery query)
        {
            return
                 MapToActionResult(() =>
                 _mediaBusinessLogic.SearchMedia(query));
        }

        public IActionResult CreateMediaForUser(long userId, string fileName, FileType ft, string filePath)
        {
            return
                    MapToActionResult(() =>
                        _mediaBusinessLogic
                        .CreateMediaForUser(userId, fileName, ft, filePath));
        }

        public IActionResult CreateMediaForBodyPart(long bodyPartId, string fileName, FileType ft, string filePath)
        {
            return
                    MapToActionResult(() =>
                        _mediaBusinessLogic
                        .CreateMediaForBodyPart(bodyPartId, fileName, ft, filePath));
        }

        public IActionResult EditMediaForUser(long id, long userId, string fileName, FileType ft, string filePath)
        {
            return
                    MapToActionResult(() =>
                        _mediaBusinessLogic
                            .EditMediaForUser(id, userId, fileName, ft, filePath));
        }

        public IActionResult EditMediaForBodyPart(Guid id, long bodyPartId, string fileName, FileType ft, string filePath)
        {
            return
                  MapToActionResult(() =>
                        _mediaBusinessLogic
                        .EditMediaForBodyPart(id, bodyPartId, fileName, ft, filePath));
        }

        public IActionResult RateUser(long userIdToRate, long raterId, decimal rating)
        {
            return
                 MapToActionResult(() =>
                     _userRatingBusinessLogic.CreateUserRating(raterId, userIdToRate, rating));
        }
    }
}
