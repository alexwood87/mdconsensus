﻿using System;
namespace WSD.Consensus.Common.Logging
{
    public interface ILogger
    {
        void LogInfo(string message);
        void LogError(string message, Exception exception);
        void LogDebug(string message);
    }
}
