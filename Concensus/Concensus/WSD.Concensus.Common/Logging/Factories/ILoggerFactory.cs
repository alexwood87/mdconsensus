﻿using System;
using System.Runtime.CompilerServices;
using WSD.Consensus.Common.Models;

namespace WSD.Consensus.Common.Logging.Factories
{
    public interface ILoggerFactory
    {
        void Info(string message, LoggerTypeModel type = LoggerTypeModel.EmailFileTrace, [CallerMemberName] string caller = "", [CallerLineNumber] int line = 0,
            [CallerFilePath] string file = "");
        void Debug(string message, LoggerTypeModel type = LoggerTypeModel.EmailFileTrace, [CallerMemberName] string caller = "", [CallerLineNumber] int line = 0,
            [CallerFilePath] string file = "");
        void Error(string message,  Exception exception, LoggerTypeModel type = LoggerTypeModel.EmailFileTrace, [CallerMemberName] string caller = "", [CallerLineNumber] int line = 0,
            [CallerFilePath] string file = "");
    }
}
