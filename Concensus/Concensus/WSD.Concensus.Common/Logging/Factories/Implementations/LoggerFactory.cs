﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using WSD.Consensus.Common.Logging.Implementations;
using WSD.Consensus.Common.Models;

namespace WSD.Consensus.Common.Logging.Factories.Implementations
{
    public class LoggerFactory : ILoggerFactory
    {
        private ILogger _fileLogger, _emailLogger, _traceLogger;

        public void Init(string filePath, string server, int port, string fromEmail, string username, string password)
        {
            _fileLogger = new FileLogger(filePath);
            _emailLogger = new EmailLogger(server, port, username, password, fromEmail);
            _traceLogger = new TraceLogger();

        }

        private LoggerFactory()
        {
        }

        private IEnumerable<ILogger> GetLoggers(LoggerTypeModel loggerType)
        {
            List<ILogger> loggers = new List<ILogger>();
            switch(loggerType)
            {
                case LoggerTypeModel.Email:
                    loggers.Add(_emailLogger);
                    break;
                case LoggerTypeModel.EmailFile:
                    loggers.Add(_emailLogger);
                    loggers.Add(_fileLogger);
                    break;
                case LoggerTypeModel.EmailFileTrace:
                    loggers.Add(_emailLogger);
                    loggers.Add(_fileLogger);
                    loggers.Add(_traceLogger);
                    break;
                case LoggerTypeModel.EmailTrace:
                    loggers.Add(_emailLogger);
                    loggers.Add(_traceLogger);
                    break;
                case LoggerTypeModel.File:
                    loggers.Add(_fileLogger);
                    break;
                case LoggerTypeModel.FileTrace:
                    loggers.Add(_fileLogger);
                    loggers.Add(_traceLogger);
                    break;
                default:
                    loggers.Add(_traceLogger);
                    break;
            }
            return loggers;
        }

        public static ILoggerFactory Instance { get; } = new LoggerFactory();

        public void Debug(string message, LoggerTypeModel type = LoggerTypeModel.EmailFileTrace, 
            [CallerMemberName] string caller = "", [CallerLineNumber] int line = 0, [CallerFilePath] string file = "")
        {
            Task.Factory.StartNew(() =>
            {
                var loggers = GetLoggers(type);
                foreach(var log in loggers)
                {
                    log.LogDebug(message + " Caller - " + caller + " Line Number " + line + " File Path " + file);
                }
            });
        }

        public void Error(string message,  Exception exception, LoggerTypeModel type = LoggerTypeModel.EmailFileTrace, [CallerMemberName] string caller = "", [CallerLineNumber] int line = 0, [CallerFilePath] string file = "")
        {
            Task.Factory.StartNew(() =>
            {
                var loggers = GetLoggers(type);
                foreach (var log in loggers)
                {
                    log.LogError(message + " Caller - " + caller + " Line Number " + line + " File Path " + file, exception);
                }
            });
        }

        public void Info(string message, LoggerTypeModel type = LoggerTypeModel.EmailFileTrace, [CallerMemberName] string caller = "", [CallerLineNumber] int line = 0, [CallerFilePath] string file = "")
        {
            Task.Factory.StartNew(() =>
            {
                var loggers = GetLoggers(type);
                foreach (var log in loggers)
                {
                    log.LogInfo(message + " Caller - " + caller + " Line Number " + line + " File Path " + file);
                }
            });
        }
    }
}
