﻿using System;
using log4net;
using log4net.Appender;
using log4net.Layout;
using log4net.Repository.Hierarchy;

namespace WSD.Consensus.Common.Logging.Implementations
{
    public class FileLogger : ILogger
    {
        private readonly ILog _log;
        public FileLogger(string path)
        {
            Hierarchy hierarchy = (Hierarchy)LogManager.GetRepository("FileLogger");

            PatternLayout patternLayout = new PatternLayout
            {
                ConversionPattern = "%date [%thread] %-5level %logger - %message%newline"
            };
            patternLayout.ActivateOptions();

            RollingFileAppender roller = new RollingFileAppender
            {
                AppendToFile = false,
                File = path,
                Layout = patternLayout,
                MaxSizeRollBackups = 5,
                MaximumFileSize = "1GB",
                RollingStyle = RollingFileAppender.RollingMode.Size,
                StaticLogFileName = true
            };
            roller.ActivateOptions();
            hierarchy.Root.AddAppender(roller);
            _log =  LogManager.GetLogger("FileLogger", "FileLogger");
        }

        public void LogDebug(string message)
        {
            _log.Debug(message);
        }

        public void LogError(string message, Exception exception)
        {
            _log.Error(message, exception);
        }

        public void LogInfo(string message)
        {
            _log.Info(message);
        }
    }
}
