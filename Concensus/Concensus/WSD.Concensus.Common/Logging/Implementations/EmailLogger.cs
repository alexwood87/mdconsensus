﻿using System;
using System.Collections.Generic;
using WSD.Consensus.Common.Mail;
using WSD.Consensus.Common.Mail.Implementations;

namespace WSD.Consensus.Common.Logging.Implementations
{
    public class EmailLogger : ILogger
    {
        private readonly string _fromEmail;
        private readonly ISmtpMailer _mailer;
        public EmailLogger(string server, int port, string username, string password, string fromEmail)
        {
            _fromEmail = fromEmail;
            _mailer = new SmtpMailer(server, port, username, password);
        }

        public void LogDebug(string message)
        {
            _mailer.SendMailAsync(new List<string> { _fromEmail },
                _fromEmail, "Debug Message " + DateTime.UtcNow.ToLongTimeString(), message, null);
        }

        public void LogError(string message, Exception exception)
        {
            _mailer.SendMailAsync(new List<string> { _fromEmail },
                _fromEmail, "Error Message " + DateTime.UtcNow.ToLongTimeString(), message + " " + exception.Message
                , null);
        }

        public void LogInfo(string message)
        {
            _mailer.SendMailAsync(new List<string> { _fromEmail },
                _fromEmail, "Info Message " + DateTime.UtcNow.ToLongTimeString(), message, null);
        }
    }
}
