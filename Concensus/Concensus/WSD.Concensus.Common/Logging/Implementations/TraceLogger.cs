﻿using System;
using log4net;
using log4net.Appender;
using log4net.Layout;
using log4net.Repository.Hierarchy;

namespace WSD.Consensus.Common.Logging.Implementations
{
    public class TraceLogger : ILogger
    {
        private readonly ILog _log;
        public TraceLogger()
        {
            Hierarchy hierarchy = (Hierarchy)LogManager.GetRepository("TraceLogger");

            PatternLayout patternLayout = new PatternLayout
            {
                ConversionPattern = "%date [%thread] %-5level %logger - %message%newline"
            };
            patternLayout.ActivateOptions();

            TraceAppender roller = new TraceAppender
            {
                Category = patternLayout,
                ImmediateFlush = true,
                Layout = new PatternLayout(patternLayout.ConversionPattern),
                Name = "TraceAppender"
            };
            roller.ActivateOptions();
            hierarchy.Root.AddAppender(roller);
            _log = LogManager.GetLogger("TraceLogger", "TraceLogger");
        }

        public void LogDebug(string message)
        {
            _log.Debug(message);
        }

        public void LogError(string message, Exception exception)
        {
            _log.Error(message, exception);
        }

        public void LogInfo(string message)
        {
            _log.Info(message);
        }
    }
}
