﻿using System;
namespace WSD.Consensus.Common.Models
{
    public enum HttpVerbModel
    {
        GET,
        OPTIONS,
        HEAD,
        PATCH,
        PUT,
        POST
    }
}
