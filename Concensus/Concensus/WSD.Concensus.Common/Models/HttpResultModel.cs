﻿
namespace WSD.Consensus.Common.Models
{
    public class HttpResultModel
    {
        public string Data { get; set; }
        public int StatusCode { get; set; }
        public bool Success => StatusCode == 200; 
    }
}
