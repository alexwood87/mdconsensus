﻿
namespace WSD.Consensus.Common.Models
{
    public class AttachmentModel
    {
        public AttachmentModel(string filename, byte[] content)
        {
            FileName = filename;
            Content = content;
        }


        public byte[] Content
        {
            private set;
            get;
        }

        public string FileName
        {
            private set;
            get;
        }
    }
}
