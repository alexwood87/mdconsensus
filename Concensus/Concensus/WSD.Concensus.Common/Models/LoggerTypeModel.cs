﻿namespace WSD.Consensus.Common.Models
{
    public enum LoggerTypeModel
    {
       Email,
       File,
       Trace,
       EmailFile,
       EmailFileTrace,
       FileTrace,
       EmailTrace
    }
}
