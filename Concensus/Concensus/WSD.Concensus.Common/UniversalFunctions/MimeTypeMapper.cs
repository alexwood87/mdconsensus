﻿using System;
using System.IO;

namespace WSD.Consensus.Common.UniversalFunctions
{
    public static class MimeTypeParser
    {
        public static string ParseMimeType(string fileName)
        {
            var ext = Path.GetExtension(fileName);
            if (ext == null)
            {
                return "application/file";
            }
            switch (ext.ToLower())
            {
                case "pdf":
                    return "application/pdf";
                case "doc":
                case "docx":
                    return "application/vnd.openxmlformats";
                case "zip":
                    return "application/zip";
                case "html":
                    return "text/html";
                case "xml":
                    return "text/xml";
                case "json":
                    return "application/json";
                case "csv":
                    return "text/csv";
                case "xls":
                case "xslx":
                    return "application/vnd.ms-excel";
                case "ofx":
                    return "application/ofx";
            }
            throw new NotImplementedException($"Mime Type {ext} Is Not Recognized.");
        }
    }

}
