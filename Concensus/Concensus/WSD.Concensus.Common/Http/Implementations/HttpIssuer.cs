﻿using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using WSD.Consensus.Common.Models;

namespace WSD.Consensus.Common.Http.Implementations
{
    public class HttpIssuer : IHttpIssuer
    {
        public HttpResultModel IssueHttpRequest(string url, HttpVerbModel method, Dictionary<string,string> parameters,
            Dictionary<string, string> headers, string username, string password)
        {
            HttpWebRequest web = (HttpWebRequest)WebRequest.Create(url);
            web.Method = method.ToString();
            if (parameters != null)
            {
                switch (method)
                {
                    case HttpVerbModel.GET:
                    case HttpVerbModel.HEAD:
                    case HttpVerbModel.OPTIONS:
                        var urlArgs = new StringBuilder();
                        urlArgs.Append("?");
                        foreach (var p in parameters)
                        {
                            urlArgs.Append(p.Key + "=" + p.Value + "&");
                        }
                        web = (HttpWebRequest)WebRequest.Create(url + urlArgs.ToString());
                        web.Method = method.ToString();
                        SetHeadersAndUsernamePassword(ref web, headers, username, password);
                        break;
                    default:
                        var urlArgs2 = new StringBuilder();
                        foreach (var p in parameters)
                        {
                            urlArgs2.Append(p.Key + "=" + p.Value + "&");
                        }
                        SetHeadersAndUsernamePassword(ref web, headers, username, password);
                        var stream = web.GetRequestStream();
                        var strWriter = new StreamWriter(stream);
                        strWriter.WriteLine(urlArgs2.ToString());
                        strWriter.Close();
                        
                        break;
                }

            }
            var response = (HttpWebResponse) web.GetResponse();
            if(response.StatusCode != HttpStatusCode.OK)
            {
                return new HttpResultModel
                {
                     StatusCode = (int) response.StatusCode
                };
            }
            var reader = new StreamReader(response.GetResponseStream());
            var data = reader.ReadToEnd();
            reader.Close();
            return
                new HttpResultModel
                {
                    Data = data,
                    StatusCode = 200
                };
        }

        private void SetHeadersAndUsernamePassword(ref HttpWebRequest web, Dictionary<string, string> headers,
            string username, string password)
        {
            if (headers != null)
            {
                foreach (var h in headers)
                {
                    switch (h.Key.ToLower())
                    {
                        case "content-type":
                            web.ContentType = h.Value;
                            break;
                        case "accept":
                            web.Accept = h.Value;
                            break;
                        default:
                            web.Headers.Add(h.Key, h.Value);
                            break;
                    }
                }
            }
            if(!string.IsNullOrWhiteSpace(username))
            {
                web.Credentials = new NetworkCredential(username, password);
            }
            else
            {
                web.UseDefaultCredentials = true;
            }
        }
    }
}
