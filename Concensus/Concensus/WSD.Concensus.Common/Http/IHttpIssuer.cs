﻿using System.Collections.Generic;
using WSD.Consensus.Common.Models;
namespace WSD.Consensus.Common.Http
{
    public interface IHttpIssuer
    {
        HttpResultModel IssueHttpRequest(string url, HttpVerbModel method, Dictionary<string, string> parameters,
            Dictionary<string, string> headers, string username, string password);
    }
}
