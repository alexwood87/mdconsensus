﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using WSD.Consensus.Common.Models;
using WSD.Consensus.Common.UniversalFunctions;

namespace WSD.Consensus.Common.Mail.Implementations
{
    public class SmtpMailer : ISmtpMailer
    {
        private readonly SmtpClient _client;
        public SmtpMailer(string smtpServer, int port, string usernmame, string password)
        {
            _client = new SmtpClient(smtpServer, port)
            {
                Credentials = new NetworkCredential(usernmame, password)
            };
        }

        public void SendMailAsync(IEnumerable<string> toEmails, string fromEmail, string subject,
           string message, Dictionary<string, IEnumerable<AttachmentModel>> attachments)
        {
            Task.Factory.StartNew(() => SendMail(toEmails, fromEmail, subject, message, attachments));
        }

        public void SendMail(IEnumerable<string> toEmails, string fromEmail, string subject,
            string message, Dictionary<string, IEnumerable<AttachmentModel>> attachments)
        {
            if (toEmails == null || !toEmails.Any())
            {
                return;
            }

            foreach (var to in toEmails)
            {
                MailMessage mailMessage = new MailMessage(fromEmail, to, subject, message);
                if (attachments != null && attachments.ContainsKey(to))
                {
                    var attachmentList = attachments[to];
                    foreach (var attachment in attachmentList)
                    {
                        MemoryStream stream = new MemoryStream();
                        BinaryWriter writer = new BinaryWriter(stream);
                        writer.Write(attachment.Content);
                        writer.Close();
                        mailMessage.Attachments.Add(new Attachment(stream, attachment.FileName, MimeTypeParser.ParseMimeType(attachment.FileName)));
                    }
                }
                _client.SendAsync(mailMessage, to);
            }
        }

    }
}
