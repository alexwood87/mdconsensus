﻿using System.Collections.Generic;
using WSD.Consensus.Common.Models;

namespace WSD.Consensus.Common.Mail
{
    public interface ISmtpMailer
    {
        void SendMail(IEnumerable<string> toEmails, string fromEmail, string subject,
           string message, Dictionary<string, IEnumerable<AttachmentModel>> attachments);
        void SendMailAsync(IEnumerable<string> toEmails, string fromEmail, string subject,
           string message, Dictionary<string, IEnumerable<AttachmentModel>> attachments);
    }
}
