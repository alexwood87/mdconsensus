﻿using System;
using System.Runtime.CompilerServices;
using WSD.Consensus.Common.Logging.Factories.Implementations;
using WSD.Consensus.Common.Models;

namespace WSD.Consensus.Common.Extensions
{
    public static class LoggingExtensions
    {
        public static void LogException(this Exception ex, LoggerTypeModel type = LoggerTypeModel.EmailFileTrace, 
            [CallerMemberName] string caller = "", [CallerLineNumber] int line = 0,
            [CallerFilePath] string file = "")
        {
            LoggerFactory.Instance.Error(ex.Message, ex.InnerException ?? ex, type, caller, line, file);
        }
    }
}
