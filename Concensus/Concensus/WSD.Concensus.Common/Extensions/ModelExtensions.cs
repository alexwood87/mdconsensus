﻿using Newtonsoft.Json;

namespace WSD.Consensus.Common.Extensions
{
    public static class ModelExtensions
    {
        public static string ToJSON(this object obj)
        {
            return
                JsonConvert.SerializeObject(obj);

        }
    }
}
