﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using WSD.Consensus.Common.Extensions;
using WSD.Consensus.Core.Contracts;
using System.IO;
using WSD.Consensus.Configuration.Helpers;
using WSD.Consensus.Domain.Models;
using Autofac;
using Microsoft.Extensions.Configuration;
namespace WSD.Consensus.Tests
{
    [TestFixture]
    public class DiagnosisTests
    {
        private IContainer _container;
        [OneTimeSetUp]
        public void TestSetUp()
        {
            var builder = new ContainerBuilder();
            var config = new ConfigurationBuilder();
            config.AddJsonFile("appsettings.json");
            builder.RegisterInstance<IConfiguration>(config.Build());
            builder.RegisterModule(new DependencyInjectorModule());
            _container = builder.Build();
        }
        [TestCase]
        public void DiagnosisTest()
        {
            var resultsWriter = new StreamWriter(File.Create(".\\Results_" + DateTime.Now.ToString("yyyyMMddhhmmssfff") + ".txt"));
            var logic = _container.Resolve<IMedicalCaseBusinessLogic>();
            var symsLogic = _container.Resolve<ISymptomBusinessLogic>();
            var bpsLogic = _container.Resolve<IBodyPartBusinessLogic>();
            var medCases = logic.Search(new MedicalCaseSearch
            {
                Symptoms = new List<SymptomModel>() {
                new SymptomModel
                {
                    Id = 1
                }
            }
            }, 5, 1);
            foreach (var medCase in medCases)
            {
                for (double a = 0.0; a < 10.0; a += 0.1)
                {
                    for (double b = 0.0; b < 10.0; b += 0.1)
                    {
                        for (double c = 0.0; c < 10.0; c += 0.1)
                        {
                            var res = logic.DiagnoseDisease(medCase.SymptomToMedicalCaseModels.Select(x => x.SymptomModel).ToList(), medCase.BodyPartToMedicalCaseModels.Select(x => x.BodyPartModel).ToList(), medCase.Patient.DemographicInformation, c, b, a);
                            try
                            {
                                Assert.NotNull(res);
                                Assert.Greater(res.Count, 0);
                                var found = false;
                                foreach (var r in res)
                                {
                                    Assert.NotNull(r);
                                    Assert.True(r.Message.CallWasSuccess);
                                    Assert.Greater(r.Likelihood, -0.1);
                                    Assert.NotNull(r.Disease);
                                    if (r.Disease.Id == medCase.Disease.Id)
                                    {
                                        found = true;
                                    }
                                }
                                Assert.IsTrue(found);
                            }
                            catch (AssertionException ex)
                            {
                                resultsWriter.WriteLine(ex);
                            }
                            resultsWriter.WriteLine(res.ToJSON());
                        }
                    }
                }
                resultsWriter.Close();
            }
        }
    }
}
