#define _CRT_SECURE_NO_DEPRECATE
#include <stdint.h>
#include <stdlib.h>

#include <iostream>
#include <tchar.h>
#include <Urlmon.h>
#pragma comment (lib, "Urlmon.lib")

#include <windows.h>
#include <wininet.h>
#include <iostream>
#include <tchar.h>
#include <stdio.h>

#pragma comment(lib,"wininet.lib")
#define ERROR_OPEN_FILE       10
#define ERROR_MEMORY          11
#define ERROR_SIZE            12
#define ERROR_INTERNET_OPEN   13
#define ERROR_INTERNET_CONN   14
#define ERROR_INTERNET_REQ    15
#define ERROR_INTERNET_SEND   16

using namespace std;
#include <string>
#include <sstream>
#include <map>
#include <list>
#using <System.dll>

using namespace System;
using namespace System::Net;
using namespace System::Text;
using namespace System::IO;
using namespace std;
static char encoding_table[] = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
'w', 'x', 'y', 'z', '0', '1', '2', '3',
'4', '5', '6', '7', '8', '9', '+', '/' };

char *base64_encode(const unsigned char *data,
	size_t input_length, size_t *output_length) {

	*output_length = 4 * ((input_length + 2) / 3);

	char *encoded_data = (char*)malloc(*output_length);
	if (encoded_data == NULL) return NULL;

	for (int i = 0, j = 0; i < input_length;) {

		uint32_t octet_a = i < input_length ? (unsigned char)data[i++] : 0;
		uint32_t octet_b = i < input_length ? (unsigned char)data[i++] : 0;
		uint32_t octet_c = i < input_length ? (unsigned char)data[i++] : 0;

		uint32_t triple = (octet_a << 0x10) + (octet_b << 0x08) + octet_c;

		encoded_data[j++] = encoding_table[(triple >> 3 * 6) & 0x3F];
		encoded_data[j++] = encoding_table[(triple >> 2 * 6) & 0x3F];
		encoded_data[j++] = encoding_table[(triple >> 1 * 6) & 0x3F];
		encoded_data[j++] = encoding_table[(triple >> 0 * 6) & 0x3F];
	}
}
String^ IssueHttpRequest(char* url, String^ method, list<char*>* paramKeys, list<string>* paramValues)
{	
	HttpWebRequest^ web = dynamic_cast<HttpWebRequest^>(WebRequest::Create(gcnew String(url)));
	web->Method = method;
	if (paramKeys != NULL && paramValues != NULL)
	{
		if (!method->Equals("get"))
		{
			sprintf(url, "%s", "?");
			
			for (int p = 0; p < paramKeys->size(); p++)
			{
				sprintf(url, "%s", (const char*)&paramKeys[p]);
				sprintf(url, "%s", (const char*)"=");
			    sprintf(url,"%s",(const char*)&paramValues[p]);
				sprintf(url,"%s",(const char*)"&");
			}
			web = dynamic_cast<HttpWebRequest^>(WebRequest::Create(gcnew String(url)));
			web->Method = method;
			delete web;
		}
		else
		{
			char* urlArgs2 = "";
			for (int p = 0; p < paramKeys->size(); p++)
			{
				sprintf(urlArgs2, "%s", (const char*)&paramKeys[p]);
				sprintf(urlArgs2, "%s", (const char*)"=");
				sprintf(urlArgs2, "%s", (const char*)&paramValues[p]);
				sprintf(urlArgs2, "%s", (const char*)"&");
			}
			Stream^ stream = web->GetRequestStream();
			StreamWriter^ strWriter = gcnew System::IO::StreamWriter(stream);
			strWriter->WriteLine(urlArgs2);
			strWriter->Close();
			delete stream;
			delete strWriter;
		}

	}
	HttpWebResponse^ resp = dynamic_cast<HttpWebResponse^>(web->GetResponse());
	Stream^ str2 = resp->GetResponseStream();
	StreamReader^ reader = gcnew StreamReader(str2);
	String^ str = reader->ReadToEnd();
	delete str2;
	delete reader;
	delete resp;
	return str;
}

String^ UploadWithImage(char url[], char iaddr[], char* fileName, list<char*> paramKeys, list<char*> paramValues, char* filePath, char contentType[])
{
	// Local variables
	char *filename = fileName;   //Filename to be loaded
	char *filepath = filePath;   //Filename to be loaded
	static char *type = contentType;
	static TCHAR hdrs[] = "Content-Type: multipart/form-data; boundary=---------------------------7d82751e2bc0858";
	static char boundary[] = "-----------------------------7d82751e2bc0858";            //Header boundary
	static char nameForm[] = "uploadedfile";     //Input form name

	char * buffer;                   //Buffer containing file + headers
	char * content;                  //Buffer containing file
	FILE * pFile;                    //File pointer
	long lSize;                      //File size
	size_t result;
	char *pos; // used in the loop

	// Open file
	pFile = fopen(filepath, "rb");
	if (pFile == NULL)
	{
		printf("ERROR_OPEN_FILE");
		getchar();
		return gcnew String("ERROR_OPEN_FILE");
	}
	printf("OPEN_FILE\n");

	// obtain file size:
	fseek(pFile, 0, SEEK_END);
	lSize = ftell(pFile);
	rewind(pFile);

	// allocate memory to contain the whole file:
	content = (char*)malloc(sizeof(char)*lSize);
	if (content == NULL)
	{
		printf("ERROR_MEMORY");
		getchar();
		return "ERROR_OPEN_FILE";
	}

	printf("MEMORY_ALLOCATED\t \"%d\" \n", lSize);
	// copy the file into the buffer:
	result = fread(content, 1, lSize, pFile);

	rewind(pFile);

	if (result != lSize)
	{
		printf("ERROR_SIZE");
		getchar();
		return "ERROR_OPEN_FILE";
	}
	printf("SIZE_OK\n");

	// terminate
	fclose(pFile);
	printf("FILE_CLOSE\n");
	//allocate memory to contain the whole file + HEADER
	buffer = (char*)malloc(sizeof(char)*lSize + 2048);

	//print header
	sprintf(buffer, "%s\r\nContent-Disposition: form-data; name=\"%s\"; filename=\"%s\"\r\n", boundary, nameForm, filename);
	sprintf(buffer, "%sContent-Type: %s\r\n", buffer, type);
	//sprintf(buffer, "%sContent-Length: %d\r\n", buffer, lSize);
	sprintf(buffer, "%s\r\n%s\r\n", buffer, content);

	/**
	sprintf(buffer, "%s\r\n", buffer);
	memcpy(buffer + strlen(buffer),content,lSize);
	sprintf(buffer, "%s\r\n", buffer);
	*/
	sprintf(buffer, "%s%s--\r\n", buffer, boundary);

	//Open internet connection
	HINTERNET hSession = InternetOpen("WINDOWS", INTERNET_OPEN_TYPE_PRECONFIG, NULL, NULL, 0);
	if (hSession == NULL)
	{
		printf("ERROR_INTERNET_OPEN");
		getchar();
		return "ERROR_OPEN_FILE";
	}
	printf("INTERNET_OPENED\n");

	HINTERNET hConnect = InternetConnect(hSession, iaddr, INTERNET_DEFAULT_HTTP_PORT, NULL, NULL, INTERNET_SERVICE_HTTP, 0, 1);
	if (hConnect == NULL)
	{
		printf("ERROR_INTERNET_CONN");
		getchar();
		return "ERROR_INTERNET_CONN";
	}
	printf("INTERNET_CONNECTED\n");

	HINTERNET hRequest = HttpOpenRequest(hConnect, (const char*)"POST", _T(url), NULL, NULL, NULL, INTERNET_FLAG_RELOAD, 1);
	if (hRequest == NULL)
	{
		printf("ERROR_INTERNET_REQ");
		getchar();

	}
	printf("INTERNET_REQ_OPEN\n");

	BOOL sent = HttpSendRequest(hRequest, hdrs, strlen(hdrs), buffer, strlen(buffer));

	if (!sent)
	{
		printf("ERROR_INTERNET_SEND");
		getchar();
		return "ERROR_INTERNET_CONN";
	}
	printf("INTERNET_SEND_OK\n");
	printf("\r\n%s\r\n", buffer);

	//close any valid internet-handles
	InternetCloseHandle(hSession);
	InternetCloseHandle(hConnect);
	InternetCloseHandle(hRequest);
	return
		gcnew String(buffer);
}