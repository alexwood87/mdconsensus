#ifndef JAVASCRIPTHELPER
#define JAVASCRIPTHELPER 
#using <System.dll>
using namespace System;
template<class TE> class JavaScriptHelper
{
public:
	TE ConvertJson(String^ jsonStr);
};
#endif