// Alexander Wood
// dxgraphics.cpp - Direct3D framework source code file

#include <d3d9.h>
#include <d3dx9.h>
#include "dxgraphics.h"

//variable declarations
LPDIRECT3D9 d3d = NULL;
LPDIRECT3DDEVICE9 d3ddev = NULL;
LPDIRECT3DSURFACE9 backbuffer = NULL;
//font
LPDIRECT3DTEXTURE9 _font;
LPD3DXSPRITE _spriteHandler;

D3DXVECTOR3 cameraSource;
D3DXVECTOR3 cameraTarget;
LPD3DXSPRITE _sprite;

bool LoadFont()
{
    HRESULT result;

    //create the sprite handler
    result = D3DXCreateSprite(d3ddev, &_spriteHandler);
    if (result != D3D_OK)
        return false;

    //load the font
    _font = LoadTexture("C:\\3DModel\\SpriteFont.bmp", D3DCOLOR_XRGB(0,0,0));
    if (_font == NULL)
        return false;

    return true;
}

int Init_Direct3D(HWND hwnd, int width, int height, int fullscreen)
{
    //initialize Direct3D
    d3d = Direct3DCreate9(D3D_SDK_VERSION);
    if (d3d == NULL)
    {
        MessageBox(hwnd, "Error initializing Direct3D", "Error", MB_OK);
        return 0;
    }

    //set Direct3D presentation parameters
    D3DDISPLAYMODE dm;
    d3d->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &dm);
    D3DPRESENT_PARAMETERS d3dpp; 
    ZeroMemory(&d3dpp, sizeof(d3dpp));

    d3dpp.Windowed = (!fullscreen);
    d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
    d3dpp.EnableAutoDepthStencil = TRUE;
    d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
    d3dpp.PresentationInterval   = D3DPRESENT_INTERVAL_IMMEDIATE;
    d3dpp.BackBufferFormat = dm.Format;
    d3dpp.BackBufferCount = 1;
    d3dpp.BackBufferWidth = width;
    d3dpp.BackBufferHeight = height;
    d3dpp.hDeviceWindow = hwnd;

    //create Direct3D device
    d3d->CreateDevice(
        D3DADAPTER_DEFAULT, 
        D3DDEVTYPE_HAL, 
        hwnd,
        D3DCREATE_SOFTWARE_VERTEXPROCESSING,
        &d3dpp, 
        &d3ddev);

    if (d3ddev == NULL)
    {
        MessageBox(hwnd, "Error creating Direct3D device", "Error", MB_OK);
        return 0;
    }

    //clear the backbuffer to black
    d3ddev->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_XRGB(0,0,0), 1.0f, 0);

    //create pointer to the back buffer
    d3ddev->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &backbuffer);

	if(!LoadFont())
	{
		return 0;
	}

    return 1;
}

LPDIRECT3DSURFACE9 LoadSurface(char *filename, D3DCOLOR transcolor)
{
    LPDIRECT3DSURFACE9 image = NULL;
    D3DXIMAGE_INFO info;
    HRESULT result;
    
    //get width and height from bitmap file
    result = D3DXGetImageInfoFromFile(filename, &info);
    if (result != D3D_OK)
        return NULL;

    //create surface
    result = d3ddev->CreateOffscreenPlainSurface(
        info.Width,         //width of the surface
        info.Height,        //height of the surface
        D3DFMT_X8R8G8B8,    //surface format
        D3DPOOL_DEFAULT,    //memory pool to use
        &image,             //pointer to the surface
        NULL);              //reserved (always NULL)

    if (result != D3D_OK)
        return NULL;

    //load surface from file into newly created surface
    result = D3DXLoadSurfaceFromFile(
        image,              //destination surface
        NULL,               //destination palette
        NULL,               //destination rectangle
        filename,           //source filename
        NULL,               //source rectangle
        D3DX_DEFAULT,       //controls how image is filtered
        transcolor,         //for transparency (0 for none)
        NULL);              //source image info (usually NULL)

    //make sure file was loaded okay
    if (result != D3D_OK)
        return NULL;

    return image;
}


LPDIRECT3DTEXTURE9 LoadTexture(char *filename, D3DCOLOR transcolor)
{
    //the texture pointer
    LPDIRECT3DTEXTURE9 texture = NULL;

    //the struct for reading bitmap file info
    D3DXIMAGE_INFO info;

    //standard Windows return value
    HRESULT result;
    
    //get width and height from bitmap file
    result = D3DXGetImageInfoFromFile(filename, &info);
    if (result != D3D_OK)
        return NULL;

    //create the new texture by loading a bitmap image file
	D3DXCreateTextureFromFileEx( 
        d3ddev,              //Direct3D device object
        filename,            //bitmap filename
        info.Width,          //bitmap image width
        info.Height,         //bitmap image height
        1,                   //mip-map levels (1 for no chain)
        D3DPOOL_DEFAULT,     //the type of surface (standard)
        D3DFMT_UNKNOWN,      //surface format (default)
        D3DPOOL_DEFAULT,     //memory class for the texture
        D3DX_DEFAULT,        //image filter
        D3DX_DEFAULT,        //mip filter
        transcolor,          //color key for transparency
        &info,               //bitmap file info (from loaded file)
        NULL,                //color palette
        &texture );          //destination texture

    //make sure the bitmap textre was loaded correctly
    if (result != D3D_OK)
        return NULL;

    return texture;
}

void SetPosition(QUAD *quad, int ivert, float x, float y, float z)
{
    quad->vertices[ivert].x = x;
    quad->vertices[ivert].y = y;
    quad->vertices[ivert].z = z;
}

void SetVertex(QUAD *quad, int ivert, float x, float y, float z, float tu, float tv)
{
    SetPosition(quad, ivert, x, y, z);
    quad->vertices[ivert].tu = tu;
    quad->vertices[ivert].tv = tv;
}

VERTEX CreateVertex(float x, float y, float z, float tu, float tv)
{
    VERTEX vertex;
    vertex.x = x;
    vertex.y = y;
    vertex.z = z;
    vertex.tu = tu;
    vertex.tv = tv;
    return vertex;
}

QUAD *CreateQuad(char *textureFilename)
{
    QUAD *quad = (QUAD*)malloc(sizeof(QUAD));

    //load the texture
    D3DXCreateTextureFromFile(d3ddev, textureFilename, &quad->texture);

    //create the vertex buffer for this quad
	d3ddev->CreateVertexBuffer( 
        4*sizeof(VERTEX), 
        0, 
        D3DFVF_MYVERTEX, D3DPOOL_DEFAULT, 
        &quad->buffer, 
        NULL);

    //create the four corners of this dual triangle strip
    //each vertex is X,Y,Z and the texture coordinates U,V
    quad->vertices[0] = CreateVertex(-1.0f, 1.0f, 0.0f, 0.0f, 0.0f);
    quad->vertices[1] = CreateVertex( 1.0f, 1.0f, 0.0f, 1.0f, 0.0f);
    quad->vertices[2] = CreateVertex(-1.0f,-1.0f, 0.0f, 0.0f, 1.0f);
    quad->vertices[3] = CreateVertex( 1.0f,-1.0f, 0.0f, 1.0f, 1.0f);

    return quad;
}

void DeleteQuad(QUAD *quad)
{
    if (quad == NULL) 
        return;

    //free the vertex buffer
    if (quad->buffer != NULL)
        quad->buffer->Release();

    //free the texture
    if (quad->texture != NULL)
        quad->texture->Release();

    //free the quad
    free(quad);
}

void DrawQuad(QUAD *quad)
{
    //fill vertex buffer with this quad's vertices
    void *temp = NULL;
    quad->buffer->Lock(0, sizeof(quad->vertices), (void**)&temp, 0);
    memcpy(temp, quad->vertices, sizeof(quad->vertices));
    quad->buffer->Unlock();

    //draw the textured dual triangle strip
    d3ddev->SetTexture(0, quad->texture);
    d3ddev->SetStreamSource(0, quad->buffer, 0, sizeof(VERTEX));
	d3ddev->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);
}

void SetIdentity()
{
    //set default position, scale, and rotation
    D3DXMATRIX matWorld;
    D3DXMatrixTranslation(&matWorld, 0.0f, 0.0f, 0.0f);
    d3ddev->SetTransform(D3DTS_WORLD, &matWorld);
}

void ClearScene(D3DXCOLOR color)
{
    d3ddev->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, color, 1.0f, 0 );
}

void SetCamera(float x, float y, float z, float lookx, float looky, float lookz)
{
    D3DXMATRIX matView;
    D3DXVECTOR3 updir(0.0f,1.0f,0.0f);

    //move the camera
    cameraSource.x = x;
    cameraSource.y = y;
    cameraSource.z = z;
    
    //point the camera
    cameraTarget.x = lookx;
    cameraTarget.y = looky;
    cameraTarget.z = lookz;

    //set up the camera view matrix
    D3DXMatrixLookAtLH(&matView, &cameraSource, &cameraTarget, &updir);
    d3ddev->SetTransform(D3DTS_VIEW, &matView);
}

void DrawChar(int x, int y, char c, LPDIRECT3DTEXTURE9 lpfont, 
              int cols, int width, int height)
{
    _spriteHandler->Begin(D3DXSPRITE_ALPHABLEND);

    //create vector to update sprite position
	D3DXVECTOR3 position((float)x, (float)y, 0.0f);

    //ASCII code of ocrfont.bmp starts with 32 (space)
    int index = c - 32;

    //configure the rect
    RECT srcRect;
    srcRect.left = (index % cols) * width;
    srcRect.top = (index / cols) * height;
    srcRect.right = srcRect.left + width;
	srcRect.bottom = srcRect.top + height;

    //draw the sprite
    _spriteHandler->Draw(
        lpfont, 
        &srcRect,
        NULL,
        &position,
        D3DCOLOR_XRGB(255,255,255));

    _spriteHandler->End();
}

void DrawTextString(int x, int y, const char *text, 
              int cols, int width, int height)
{
	LPDIRECT3DTEXTURE9 lpfont = _font;
    const int xMaxWidth = 35;
	int xStart = 0;
	int yDrop = 0;
	for (unsigned int n=0; n<strlen(text); n++)
    {
        if(n % xMaxWidth ==0)
		{
			xStart = 0;
			yDrop += height;
		}
		else
		{
			xStart = ((n % xMaxWidth) * 8) ; 
		}
		DrawChar(x + xStart, y + yDrop, text[n], lpfont, cols, width, height);
    }
    
}


void SetPerspective(float fieldOfView, float aspectRatio, float nearRange, float farRange)
{
    //set the perspective so things in the distance will look smaller
    D3DXMATRIX matProj;
    D3DXMatrixPerspectiveFovLH(&matProj, fieldOfView, aspectRatio, nearRange, farRange);
    d3ddev->SetTransform(D3DTS_PROJECTION, &matProj);
}

MODEL *LoadModel(char *filename)
{
    MODEL *model = (MODEL*)malloc(sizeof(MODEL));
    LPD3DXBUFFER matbuffer;
    HRESULT result;

    //load mesh from the specified file
    result = D3DXLoadMeshFromX(
        filename,               //filename
        D3DXMESH_SYSTEMMEM,     //mesh options
        d3ddev,                 //Direct3D device
        NULL,                   //adjacency buffer
        &matbuffer,             //material buffer
        NULL,                   //special effects
        &model->material_count, //number of materials
        &model->mesh);          //resulting mesh

    if (result != D3D_OK)
    {
        MessageBox(NULL, "Error loading model file", "Error", MB_OK);
        return NULL;
    }

    //extract material properties and texture names from material buffer
    LPD3DXMATERIAL d3dxMaterials = (LPD3DXMATERIAL)matbuffer->GetBufferPointer();
    model->materials = new D3DMATERIAL9[model->material_count];
    model->textures  = new LPDIRECT3DTEXTURE9[model->material_count];

    //create the materials and textures
    for(DWORD i=0; i<model->material_count; i++)
    {
        //grab the material
        model->materials[i] = d3dxMaterials[i].MatD3D;

        //set ambient color for material 
        model->materials[i].Ambient = model->materials[i].Diffuse;

        model->textures[i] = NULL;
        if( d3dxMaterials[i].pTextureFilename != NULL && 
            lstrlen(d3dxMaterials[i].pTextureFilename) > 0 )
        {
			char* s = "C:\\3DModel\\";
			char* is = d3dxMaterials[i].pTextureFilename;
            std::string str = std::string(s);
			str.append(is);
			//load texture file specified in .x file
            result = D3DXCreateTextureFromFile(d3ddev, 
                //d3dxMaterials[i].pTextureFilename, 
                str.c_str(),
				&model->textures[i]);

            if (result != D3D_OK)
            {
                MessageBox(NULL, "Could not find texture file", "Error", MB_OK);
                return NULL;
            }
        }
    }

    //done using material buffer
    matbuffer->Release();

    return model;
}

VOID DeleteModel(MODEL *model)
{
    //remove materials from memory
    if( model->materials != NULL ) 
        delete[] model->materials;

    //remove textures from memory
    if (model->textures != NULL)
    {
        for( DWORD i = 0; i < model->material_count; i++)
        {
            if (model->textures[i] != NULL)
                model->textures[i]->Release();
        }
        delete[] model->textures;
    }
    
    //remove mesh from memory
    if (model->mesh != NULL)
        model->mesh->Release();

    //remove model struct from memory
    if (model != NULL)
        free(model);
    
}

void DrawModel(MODEL *model)
{
    //draw each mesh subset
    for( DWORD i=0; i<model->material_count; i++ )
    {
        // Set the material and texture for this subset
        d3ddev->SetMaterial( &model->materials[i] );
        d3ddev->SetTexture( 0, model->textures[i] );
    
        // Draw the mesh subset
        model->mesh->DrawSubset( i );
    }
}

