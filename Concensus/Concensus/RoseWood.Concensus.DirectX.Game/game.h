// Alexander Wood
// Ai Gui Main
// Main Game header file


#ifndef _GAME_H
#define _GAME_H

//windows/directx headers
#include <d3d9.h>
#include <d3dx9.h>
#include <DxErr.h>
#include <dsound.h>
#include <dinput.h>
#include <windows.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

//framework-specific headers
#include "dxgraphics.h"
#include "dxaudio.h"
#include "dxinput.h"

//application title
#define APPTITLE "AiGui Demo"

//screen setup
#define FULLSCREEN 0       //0 = windowed, 1 = fullscreen
#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600

//function prototypes
int Game_Init(HWND);
void Game_Run(HWND);
void Game_End(HWND);


#endif

