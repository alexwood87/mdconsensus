// Alexander Wood
// dxgraphics.h - Direct3D framework header file


#ifndef _DXGRAPHICS_H
#define _DXGRAPHICS_H

#include <d3d9.h>
#include <d3dx9.h>
#include <string>
//vertex and quad definitions
#define D3DFVF_MYVERTEX (D3DFVF_XYZ | D3DFVF_TEX1)
struct VERTEX
{
    float x, y, z;
    float tu, tv;
};
struct QUAD
{
    VERTEX vertices[4];
    LPDIRECT3DVERTEXBUFFER9 buffer;
    LPDIRECT3DTEXTURE9 texture;
};

//sprite structure
struct SPRITE {
    int x,y;
    int width,height;
    int movex,movey;
    int curframe,lastframe;
    int animdelay,animcount;
    int scalex, scaley;
    int rotation, rotaterate;
};


//define the MODEL struct
struct MODEL
{
    LPD3DXMESH mesh;
    D3DMATERIAL9* materials;
    LPDIRECT3DTEXTURE9* textures;
    DWORD material_count;
};



//function prototypes
void DrawTextString(int x, int y,const char *text, 
              int cols, int width, int height);
//void DrawTextString(HWND hwnd, LPDIRECT3DSURFACE9 fontSurface, int x, int y,int heightOffset, int widthOffset, DWORD color, const char *str );
int Init_Direct3D(HWND, int, int, int);
LPDIRECT3DSURFACE9 LoadSurface(char*, D3DCOLOR);
LPDIRECT3DTEXTURE9 LoadTexture(char*, D3DCOLOR);
void SetPosition(QUAD*,int,float,float,float);
void SetVertex(QUAD*,int,float,float,float,float,float);
VERTEX CreateVertex(float,float,float,float,float);
QUAD* CreateQuad(char*);
void DeleteQuad(QUAD*);
void DrawQuad(QUAD*);
void SetIdentity();
void SetCamera(float,float,float,float,float,float);
void SetPerspective(float,float,float,float);
void ClearScene(D3DXCOLOR);
MODEL *LoadModel(char *);
VOID DeleteModel(MODEL *);
void DrawModel(MODEL *);


//variable declarations
extern LPDIRECT3D9 d3d; 
extern LPDIRECT3DDEVICE9 d3ddev; 
extern LPDIRECT3DSURFACE9 backbuffer;
extern D3DXVECTOR3 cameraSource;
extern D3DXVECTOR3 cameraTarget;

#endif

