// Alexander Wood
// AI Gui Main
// Main Game source code file


#include "game.h"
#include<list>

using namespace std;
#define BLACK D3DCOLOR_ARGB(0,0,0,0)
MODEL* _AiGirl;
std::string* _questionString;
std::string* _answerString;
int _timeCycles;
int _hospitalLevel = 1;
list<list<int>*>* _hospitalMap = new list<list<int>*>();
const int RestartTimerTime = 60;
float angle = 0.0f;
//ConcensusProxy* proxy = NULL;
//initializes the game
int Game_Init(HWND hwnd)
{
	_timeCycles = 0;
	_questionString = new std::string("How Can I Help You?");
	_answerString = new std::string();
	_hospitalMap->push_back(new list<int>());

	//initialize keyboard
    if (!Init_Keyboard(hwnd))
    {
        MessageBox(hwnd, "Error initializing the keyboard", "Error", MB_OK);
        return 0;
    }

    //initialize the game, may use 2D by default here
    //for 3D, must set camera, perspective, z-buffer, and lighting
    //refer to chapters 11, 13, and 14
    //load 2D bitmaps or 3D models here
	//Render Map/Hospital Interior

	//Render Patients
	
	//Render User
	_AiGirl = LoadModel("C:\\3DModel\\Beautiful Girl.x");
	if(!_AiGirl)
	{
		return 0;		
	}
	
	SetIdentity();
	SetCamera(0.0f, 10.0f, 2.0f, 0.0f, 0.0f, 0.0f);
	SetPerspective(D3DX_PI/8.0f, 800.0f/600.0f, 0.1f, 100.0f);
	
	d3ddev->SetRenderState(D3DRS_ZENABLE, true);
	d3ddev->SetRenderState(D3DRS_AMBIENT, D3DCOLOR_XRGB(255, 255, 255));
	D3DXMATRIXA16 matWorld;
	D3DXMatrixRotationX(&matWorld, -1.0f);
	d3ddev->SetTransform(D3DTS_WORLD, &matWorld);
	
	//return okay
    return 1;
}

short PollEscape()
{
	return GetAsyncKeyState(VK_ESCAPE);
}
short KeyState(int ch)
{
	return GetKeyState(ch);
}

void UpdateGui(HWND hwnd)
{
	if(++_timeCycles < RestartTimerTime)
		return;
	_timeCycles = 0;
	Poll_Keyboard(); 
	
	if (PollEscape())
    {
		PostMessage(hwnd, WM_DESTROY, 0, 0);
		return;
	}
	else {
		if(GetAsyncKeyState(VK_SHIFT))
		{
			if(Key_Down(DIK_A))
			{
				_answerString->append("A");		
			}
			else if(Key_Down(DIK_B))
			{
				_answerString->append("B");	
			}
			else if(Key_Down(DIK_C))
			{
				_answerString->append("C");	
			}
			else if(Key_Down(DIK_D))
			{
				_answerString->append("D");	
			}else if(Key_Down(DIK_E))
			{
				_answerString->append("E");	
			}else if(Key_Down(DIK_F))
			{
				_answerString->append("F");	
			}else if(Key_Down(DIK_G))
			{
				_answerString->append("G");	
			}else if(Key_Down(DIK_H))
			{
				_answerString->append("H");	
			}else if(Key_Down(DIK_I))
			{
				_answerString->append("I");	
			}else if(Key_Down(DIK_J))
			{
				_answerString->append("J");	
			}else if(Key_Down(DIK_K))
			{
				_answerString->append("K");	
			}else if(Key_Down(DIK_L))
			{
				_answerString->append("L");	
			}else if(Key_Down(DIK_M))
			{
				_answerString->append("M");	
			}else if(Key_Down(DIK_N))
			{
				_answerString->append("N");	
			}else if(Key_Down(DIK_O))
			{
				_answerString->append("O");	
			}else if(Key_Down(DIK_P))
			{
				_answerString->append("P");	
			}else if(Key_Down(DIK_Q))
			{
				_answerString->append("Q");	
			}else if(Key_Down(DIK_R))
			{
				_answerString->append("R");	
			}else if(Key_Down(DIK_S))
			{
				_answerString->append("S");	
			}else if(Key_Down(DIK_T))
			{
				_answerString->append("T");	
			}else if(Key_Down(DIK_U))
			{
				_answerString->append("U");	
			}
			else if(Key_Down(DIK_W))
			{
				_answerString->append("W");	
			}else if(Key_Down(DIK_X))
			{
				_answerString->append("X");	
			}else if(Key_Down(DIK_Y))
			{
				_answerString->append("Y");	
			}else if(Key_Down(DIK_Z))
			{
				_answerString->append("Z");	
			}
		}
		else{
			if(Key_Down(DIK_A))
			{
				_answerString->append("a");		
			}
			else if(Key_Down(DIK_B))
			{
				_answerString->append("b");	
			}
			else if(Key_Down(DIK_C))
			{
				_answerString->append("c");	
			}
			else if(Key_Down(DIK_D))
			{
				_answerString->append("d");	
			}else if(Key_Down(DIK_E))
			{
				_answerString->append("e");	
			}else if(Key_Down(DIK_F))
			{
				_answerString->append("f");	
			}else if(Key_Down(DIK_G))
			{
				_answerString->append("g");	
			}else if(Key_Down(DIK_H))
			{
				_answerString->append("h");	
			}else if(Key_Down(DIK_I))
			{
				_answerString->append("i");	
			}else if(Key_Down(DIK_J))
			{
				_answerString->append("j");	
			}else if(Key_Down(DIK_K))
			{
				_answerString->append("k");	
			}else if(Key_Down(DIK_L))
			{
				_answerString->append("l");	
			}else if(Key_Down(DIK_M))
			{
				_answerString->append("m");	
			}else if(Key_Down(DIK_N))
			{
				_answerString->append("n");	
			}else if(Key_Down(DIK_O))
			{
				_answerString->append("o");	
			}else if(Key_Down(DIK_P))
			{
				_answerString->append("p");	
			}else if(Key_Down(DIK_Q))
			{
				_answerString->append("q");	
			}else if(Key_Down(DIK_R))
			{
				_answerString->append("r");	
			}else if(Key_Down(DIK_S))
			{
				_answerString->append("s");	
			}else if(Key_Down(DIK_T))
			{
				_answerString->append("t");	
			}else if(Key_Down(DIK_U))
			{
				_answerString->append("u");	
			}
			else if(Key_Down(DIK_W))
			{
				_answerString->append("w");	
			}else if(Key_Down(DIK_X))
			{
				_answerString->append("x");	
			}else if(Key_Down(DIK_Y))
			{
				_answerString->append("y");	
			}else if(Key_Down(DIK_Z))
			{
				_answerString->append("z");	
			}else if(Key_Down(DIK_0))
			{
				_answerString->append("0");	
			}else if(Key_Down(DIK_1))
			{
				_answerString->append("1");	
			}else if(Key_Down(DIK_2))
			{
				_answerString->append("2");	
			}else if(Key_Down(DIK_3))
			{
				_answerString->append("3");	
			}else if(Key_Down(DIK_4))
			{
				_answerString->append("4");	
			}else if(Key_Down(DIK_5))
			{
				_answerString->append("5");	
			}else if(Key_Down(DIK_6))
			{
				_answerString->append("6");	
			}
			else if(Key_Down(DIK_7))
			{
				_answerString->append("7");
			}else if(Key_Down(DIK_8))
			{
				_answerString->append("8");
			}else if(Key_Down(DIK_9))
			{
				_answerString->append("9");
			}
			else if(Key_Down(DIK_SPACE))
			{
				_answerString->append(" ");
			}
			else if(Key_Down(DIK_RETURN))
			{
				// send message to AI WebService
				if (_hospitalMap[0].size() == 0)
				{
					
				}
			}
			else if(Key_Down(DIK_LEFTARROW))
			{
				angle += -0.05;
				D3DXMATRIX matWorld;
				D3DXMatrixRotationX(&matWorld, -1.0f);
				d3ddev->SetTransform(D3DTS_WORLD, &matWorld);
				D3DXMATRIX matWorld2;
				D3DXMATRIX matWorld3;
				D3DXMatrixRotationX(&matWorld2, angle);
				matWorld3 = matWorld * matWorld2;
				d3ddev->SetTransform(D3DTS_WORLD, &matWorld3);
				
			}
			else if(Key_Down(DIK_RIGHTARROW))
			{
				angle += 0.05;
				D3DXMATRIX matWorld;
				D3DXMatrixRotationX(&matWorld, -1.0f);
				D3DXMATRIX matWorld2;
				D3DXMATRIX matWorld3;
				D3DXMatrixRotationX(&matWorld2, angle);
				matWorld3 = matWorld * matWorld2;
				d3ddev->SetTransform(D3DTS_WORLD, &matWorld3);
				
			}
			else if(Key_Down(DIK_BACK))
			{
				if(_answerString->size() > 0)
				{
					//const char* tmp = _answerString->c_str();
					int size = _answerString->size();
			
					if(size-1 == 0)
						_answerString = new std::string();
					else
					{												
						_answerString->pop_back();
					}
				}
			}
		}
		
	}
    //Poll_Mouse();
    //use Mouse_X(), Mouse_Y(), and Mouse_Button() 
}

//the main game loop
void Game_Run(HWND hwnd)
{
   
	//clear the screen here if needed

	ClearScene(BLACK);

    if (d3ddev->BeginScene())
    {
        //update the screen here
        //this is where you draw your background or sprites
        //or call the functions to render your 3D models
		if(_AiGirl)
			DrawModel(_AiGirl);   
		if(_questionString != NULL)
		{
			DrawTextString(500, 0, _questionString->c_str(),20,8,12);
		}
		if(_answerString != NULL)
		{
			DrawTextString(500,200,_answerString->c_str(),20,8,12);							
		}
        d3ddev->EndScene();
    }

    d3ddev->Present(NULL, NULL, NULL, NULL);

	UpdateGui(hwnd);
}


void Game_End(HWND hwnd)
{
    //delete all objects from memory
    //including surfaces, sprites, sounds, models, 
    //and anything else you used that allocated memory
    //DirectX objects are removed automatically in dxgraphics.cpp
	DeleteModel(_AiGirl);
	if(_questionString)
		delete _questionString;
	if(_answerString)
		delete _answerString;
	if (_hospitalMap)
	{
		for (int i = 0; i < _hospitalMap->size(); i++)
		{
			delete &_hospitalMap[i];
		}
		delete _hospitalMap;
	}
}

