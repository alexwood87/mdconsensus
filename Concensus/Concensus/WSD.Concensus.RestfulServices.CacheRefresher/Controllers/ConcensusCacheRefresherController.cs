﻿using System;
using Microsoft.AspNetCore.Mvc;
using WSD.Consensus.Configuration;
using WSD.Consensus.Common.Extensions;
namespace WSD.Consensus.RestfulServices.CacheRefresher.Controllers
{
    public class ConcensusCacheRefresherController : Controller
    {
        private readonly IConsensusCache _consensusCache;
        public ConcensusCacheRefresherController(IConsensusCache consensusCache)
        {
            _consensusCache = consensusCache;
        }
        public string RefreshServers(string cacheKey, string key)
        {
            try
            {
                _consensusCache.Delete(cacheKey, long.Parse(key));
                return
                    new { CallWasSuccess = true, FriendlyMessage = "Update Successful for " + 
                            (cacheKey ?? "") + (key ?? "") }.ToJSON();
            }
            catch (Exception ex)
            {
                return
                    new { CallWasSuccess = false, DeveloperMessage = ex + (cacheKey ?? "") + (key ?? ""), FriendlyMessage = "Error Loading Updated Item" }.ToJSON();
            }
        }
    }
}
