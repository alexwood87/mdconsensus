﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using WSD.Consensus.Configuration;
using WSD.Consensus.EducationApi.Helpers.Headers;
using WSD.Consensus.Configuration.Helpers;
using WSD.Consensus.Core.Contracts;
using WSD.Consensus.Domain.Models;
using WSD.Consensus.Presentation.Web.Authentication.Headers;

namespace WSD.Consensus.EducationApi.Controllers
{
    [Authorization(false, typeof(EducationApiAuthorizedValidator))]
    public class EducationApiController : Controller
    {

        private readonly IConsensusCache _consensusCache;
        private readonly IMediaBusinessLogic _mediaBusinessLogic;
        private readonly IMedicalCaseBusinessLogic _medicalCaseBusinessLogic;
        public EducationApiController(IConsensusCache consensusCache, IMedicalCaseBusinessLogic medicalCaseBusinessLogic,
            IMediaBusinessLogic mediaBusinessLogic)
        {
            _consensusCache = consensusCache;
            _mediaBusinessLogic = mediaBusinessLogic;
            _medicalCaseBusinessLogic = medicalCaseBusinessLogic;
        }

        public delegate object Run();

        private IActionResult MapResult(Run run)
        {
            try
            {
                return
                    Json(new
                    {
                        Success = true,
                        Data = run.Invoke()
                    });
            }
            catch(Exception ex)
            {
                return
                    Json(new {
                        Success = false,
                        Error = ex.Message            
                    });
            }
        }

        public IActionResult DiagnoseDisease(List<SymptomModel> symptoms, List<BodyPartModel> bodyParts, List<DemographicInformationModel> demographicInformation)
        {
            return
                MapResult(() => 
                 _medicalCaseBusinessLogic.DiagnoseDisease(symptoms, bodyParts, demographicInformation)
                );
        }

        public IActionResult SearchSymptoms(MetaDataSearchQuery query)
        {
            return MapResult(() =>
                _consensusCache.SearchSymptomsByQuery(query));
        }

        public IActionResult SearchBodyParts(MetaDataSearchQuery query)
        {
            return MapResult(() =>
                        _consensusCache.SearchBodyPartsByQuery(query));
        }

        public IActionResult SearchDiseases(MetaDataSearchQuery query)
        {
            return MapResult(() =>
                _consensusCache.SearchDiseasesByQuery(query));
        }

        public IActionResult SearchMedia(MediaSearchQuery query)
        {
            return
                MapResult(() =>
                _mediaBusinessLogic.SearchMedia(query));
        }

        public IActionResult CreateMediaForUser(long userId, string fileName, FileType ft, string filePath)
        {
            return
                MapResult(() =>
                 _mediaBusinessLogic
                .CreateMediaForUser(userId, fileName, ft, filePath));
        }

        public IActionResult CreateMediaForBodyPart(long bodyPartId, string fileName, FileType ft, string filePath)
        {
            return
                MapResult(() =>
                 _mediaBusinessLogic
                .CreateMediaForBodyPart(bodyPartId, fileName, ft, filePath));
        }

        public IActionResult EditMediaForUser(long id, long userId, string fileName, FileType ft, string filePath)
        {
            return
                MapResult(() =>
                    _mediaBusinessLogic
                    .EditMediaForUser(id, userId, fileName, ft, filePath));
        }

        public IActionResult EditMediaForBodyPart(Guid id, long bodyPartId, string fileName, FileType ft, string filePath)
        {
            return
                MapResult(() =>
                    _mediaBusinessLogic
                    .EditMediaForBodyPart(id, bodyPartId, fileName, ft, filePath));
        }
    }
}
