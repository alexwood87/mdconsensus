﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using WSD.Consensus.Domain.Models;

namespace WSD.Consensus.WebServices.EduService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IEduService
    {
        [OperationContract]
        List<DiseaseDiagnosticResult> DiagnoseDisease(List<SymptomModel> symptoms, List<BodyPartModel> bodyParts, List<DemographicInformationModel> demographicInformation);
        [OperationContract]
        List<SymptomModel> SearchSymptoms(MetaDataSearchQuery query);
        [OperationContract]
        List<BodyPartModel> SearchBodyParts(MetaDataSearchQuery query);
        [OperationContract]
        List<DiseaseModel> SearchDiseases(MetaDataSearchQuery query);
        [OperationContract]
        List<MediaModel> SearchMedia(MediaSearchQuery query);
        [OperationContract]
        bool CreateMediaForUser(long userId, string fileName, FileType ft, string filePath);
        [OperationContract]
        bool CreateMediaForBodyPart(long bodyPartId, string fileName, FileType ft, string filePath);
        [OperationContract]
        bool EditMediaForUser(long id, long userId, string fileName, FileType ft, string filePath);
        [OperationContract]
        bool EditMediaForBodyPart(Guid id, long bodyPartId, string fileName, FileType ft, string filePath);           
    }


}
