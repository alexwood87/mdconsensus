﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.ServiceModel;
using WSD.Consensus.Configuration;
using WSD.Consensus.EducationApi.Helpers.Headers;
using WSD.Consensus.Configuration.Helpers;
using WSD.Consensus.Core.Contracts;
using WSD.Consensus.Domain.Models;

namespace WSD.Consensus.WebServices.EduService
{
    public class EduService : IEduService
    {

        public EduService()
        {
            ValidateClient();
        }

        private void ValidateClient()
        {
            var validator = new EducationApiAuthorizedValidator();
            if (ConfigurationManager.AppSettings["DevMode"] == "Production")
            {
                try
                {
                    var authHeader = OperationContext.Current.IncomingMessageHeaders.GetHeader<string>("Authentication",
                        "AuthenticationHeader");
                    if (authHeader == null)
                    {
                        throw new UnauthorizedAccessException("Username Or Password Not Found");
                    }
                    var parts = authHeader.Split(':');
                    if (parts.Length <= 1)
                    {
                        throw new UnauthorizedAccessException("Username Or Password Not Found");
                    }
                    validator.Validate(parts[0], parts[1]);
                }
                catch (Exception ex)
                {
                    throw new UnauthorizedAccessException("Username Or Password Not Found", ex);
                }

            }
        }

        public List<DiseaseDiagnosticResult> DiagnoseDisease(List<SymptomModel> symptoms, List<BodyPartModel> bodyParts, List<DemographicInformationModel> demographicInformation)
        {
            return DependencyInjectorModule.NewBusinessLogicInstance<IMedicalCaseBusinessLogic>().DiagnoseDisease(symptoms,bodyParts,demographicInformation);
        }

        public List<SymptomModel> SearchSymptoms(MetaDataSearchQuery query)
        {
            return
                ConsensusCache.SearchSymptomsByQuery(query);
        }

        public List<BodyPartModel> SearchBodyParts(MetaDataSearchQuery query)
        {
            return
                ConsensusCache.SearchBodyPartsByQuery(query);
        }

        public List<DiseaseModel> SearchDiseases(MetaDataSearchQuery query)
        {
            return
                ConsensusCache.SearchDiseasesByQuery(query);
        }

        public List<MediaModel> SearchMedia(MediaSearchQuery query)
        {
            return DependencyInjectorModule.NewBusinessLogicInstance<IMediaBusinessLogic>().SearchMedia(query);
        }

        public bool CreateMediaForUser(long userId, string fileName, FileType ft, string filePath)
        {
            return DependencyInjectorModule.NewBusinessLogicInstance<IMediaBusinessLogic>()
                .CreateMediaForUser(userId, fileName, ft, filePath);
        }

        public bool CreateMediaForBodyPart(long bodyPartId, string fileName, FileType ft, string filePath)
        {
            return DependencyInjectorModule.NewBusinessLogicInstance<IMediaBusinessLogic>()
                .CreateMediaForBodyPart(bodyPartId, fileName, ft, filePath);
        }

        public bool EditMediaForUser(long id, long userId, string fileName, FileType ft, string filePath)
        {
            return DependencyInjectorModule.NewBusinessLogicInstance<IMediaBusinessLogic>()
                .EditMediaForUser(id, userId, fileName, ft, filePath);
        }

        public bool EditMediaForBodyPart(Guid id, long bodyPartId, string fileName, FileType ft, string filePath)
        {
            return DependencyInjectorModule.NewBusinessLogicInstance<IMediaBusinessLogic>()
                .EditMediaForBodyPart(id, bodyPartId, fileName, ft, filePath);
        }
    }
}
