﻿using System.Collections.Generic;
using WSD.Consensus.Presentation.Web.Authentication;

namespace WSD.Consensus.EducationApi.Helpers.Headers
{
    public class EducationApiAuthorizedValidator : ConcensusAuthorizedValidator
    {
        public EducationApiAuthorizedValidator() : base(new List<KeyValuePair<string, string>> { 
            new KeyValuePair<string, string>("EduApiUser", "dsjfgl;ghlhshe48grydft89e"),
            new KeyValuePair<string, string>("EduMobileUser", "fa,sgfgw7eaiufgysgfwi2e8y")
        })
        {

        }
       
    }
}
