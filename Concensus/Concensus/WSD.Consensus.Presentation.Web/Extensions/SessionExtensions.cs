﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace WSD.Consensus.Presentation.Extensions
{
    public static class SessionExtensions
    {
        public static void SetModel<E>(this ISession session, string name, E user)
        {
            session.SetString(name, JsonConvert.SerializeObject(user));
        }
        public static E GetModel<E>(this ISession session, string name)
        {
            var str = session.GetString(name);

            return !string.IsNullOrWhiteSpace(str) ?
                JsonConvert.DeserializeObject<E>(str) : default;
        }
    }
}
