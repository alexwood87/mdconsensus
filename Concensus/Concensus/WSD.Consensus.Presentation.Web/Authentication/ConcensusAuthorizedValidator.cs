﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WSD.Consensus.Presentation.Web.Authentication
{
    public class ConcensusAuthorizedValidator
    {
        protected readonly List<KeyValuePair<string, string>> _acceptableLogins;
        protected ConcensusAuthorizedValidator(List<KeyValuePair<string, string>> validUsernamePasswords)
        {
            _acceptableLogins = validUsernamePasswords;
        }
    
        public virtual void Validate(string userName, string password)
        {
            if (userName == null || password == null)
            {
                throw new ArgumentNullException();
            }

            if (_acceptableLogins.All(s => s.Key.ToLowerInvariant() 
                != userName.ToLowerInvariant() || s.Value != password))
            {
                throw new UnauthorizedAccessException("Incorrect Username or Password");
            }
        }
    }
}
