﻿
namespace WSD.Consensus.Presentation.Web.Authentication
{
    public interface IAuthenticationValidator
    {
        bool Validate(string username, string password);
    }
}
