﻿using System;
using Microsoft.AspNetCore.Mvc;
namespace WSD.Consensus.Presentation.Web.Authentication.Headers
{
    public class AuthorizationAttribute : TypeFilterAttribute
    {
            public AuthorizationAttribute(bool doesIPAddressRestrictionValidation,
                Type authenticationValidator) : base(typeof(AuthenticationFilter))
            {
                Arguments = new object[] { doesIPAddressRestrictionValidation, Activator.CreateInstance(authenticationValidator) };
            }
    }
}
