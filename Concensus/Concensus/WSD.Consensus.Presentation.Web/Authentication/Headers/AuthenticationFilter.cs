﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace WSD.Consensus.Presentation.Web.Authentication.Headers
{

    public class AuthenticationFilter : IAuthorizationFilter
    {
        private static readonly HashSet<string> AllowedIps = new HashSet<string>();
      
        static AuthenticationFilter()
        {
            AllowedIps.Add("::1");
            AllowedIps.Add("127.0.0.1"); 
            AllowedIps.Add("localhost");
            if (ConfigurationManager.AppSettings["AllowedIPAddresses"] != null)
            {
                ConfigurationManager.AppSettings["AllowedIPAddresses"].Split('-').ToList()
                    .ForEach((ip) =>
                    {
                        if (!AllowedIps.Contains(ip))
                        {
                            AllowedIps.Add(ip);
                        }
                    });
            }
        }

        private readonly bool _requiresIPAuthentication;
        private readonly IAuthenticationValidator _validator;
        public AuthenticationFilter(bool requiresIPAuthentication, IAuthenticationValidator validator)
        {
            _requiresIPAuthentication = requiresIPAuthentication;
            _validator = validator;
        }
            
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            try
            {
                if (ConfigurationManager.AppSettings["DevMode"] == "Production")
                {
                    if (context.HttpContext.Request.Headers.TryGetValue("UsernamePassword", out var col))
                    {
                        var parts = col[0].Split(':');
                        _validator.Validate(parts[0], parts[1]);
                    }
                    if (_requiresIPAuthentication &&
                        !AllowedIps.Contains(context.HttpContext.Connection.RemoteIpAddress.ToString()))
                    {

                        context.Result = new UnauthorizedResult();
                    }
                    else if (context.HttpContext.Request.Path.Value.Contains("Content"))
                    {
                        context.Result = new ForbidResult();
                    }
                }
            }
            catch
            {
                context.Result = new UnauthorizedResult();
            }
        }
    }
}
