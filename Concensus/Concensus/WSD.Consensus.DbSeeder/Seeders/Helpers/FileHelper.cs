﻿using System.Collections.Generic;
using System.Linq;
using System.IO;
namespace WSD.Consensus.DbSeeder.Seeders.Helpers
{
    public static class FileHelper
    {
        public static string[][] FileToParsableComponents(string filename)
        {
            var fil = File.ReadAllText(filename);
            var lines = fil.Replace("\r","").Split('\n');
            var list = lines.Where(x => !string.IsNullOrEmpty(x)).Select(x => x.Split('-'));
            var list2 = new List<string[]>();
            foreach (var i in list)
            {
                var s = new string[]
                {
                    i[0].Trim(),
                    i.Length>1 ? i[1].Trim() : string.Empty
                };
                list2.Add(s);
            }
            return
                list2.ToArray();
        }
    }
}
