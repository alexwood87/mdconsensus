﻿using Autofac;

namespace WSD.Consensus.DbSeeder.Seeders.Helpers
{
    public static class AutofacProvider
    {
        public static IContainer Container { get; set; }
        public static E Resolve<E>() => Container.Resolve<E>();

    }
}
