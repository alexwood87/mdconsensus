﻿using System;
using System.Threading.Tasks;
using WSD.Consensus.Core.Contracts;
using WSD.Consensus.Domain.Models;
using WSD.Consensus.DbSeeder.Seeders.Helpers;

namespace WSD.Consensus.DbSeeder.Seeders
{
    public class DiseasesSeeder : ISeeder
    {
        public async Task SeedDatabaseAsync()
        {
            var comps = FileHelper.FileToParsableComponents("./SeedFiles/ConcensusDiseases.txt");
            var logic = AutofacProvider.Resolve<IDiseaseBusinessLogic>();
            await Task.Factory.StartNew(() =>
            {
                foreach (var line in comps)
                {
                    var part = new DiseaseModel
                    {
                        CultureName = "US"
                    };
                    if (line.Length > 1)
                    {
                        part.CommonName = line[1] ?? "";
                        part.ScientificName = line[0];
                    }
                    else
                    {
                        part.ScientificName = line[0];
                        part.CommonName = "";
                    }
                    if (!string.IsNullOrEmpty(part.ScientificName))
                        if (0 == logic.CreateDisease(part.ScientificName, part.CommonName, part.CultureName))
                        {
                            Console.WriteLine(part.ScientificName + " failed");
                        }
                }
            });
        }
    }
}
