﻿using WSD.Consensus.Configuration.Helpers;
using WSD.Consensus.Domain.Models;
using WSD.Consensus.Core.Contracts;
using System;
using System.Threading.Tasks;
using WSD.Consensus.DbSeeder.Seeders.Helpers;

namespace WSD.Consensus.DbSeeder.Seeders
{
    public class BodyPartsSeeder : ISeeder
    {
        public async Task SeedDatabaseAsync()
        {
            var comps = FileHelper.FileToParsableComponents("./SeedFiles/ConcensusBodyParts.txt");
            var logic = AutofacProvider.Resolve<IBodyPartBusinessLogic>();
            await Task.Factory.StartNew(() =>
            {
                foreach (var line in comps)
                {
                    var part = new BodyPartModel
                    {
                        CultureName = "US"
                    };
                    if (line.Length > 1)
                    {
                        part.CommonName = line[0];
                        part.ScientificName = line[1];
                    }
                    else
                    {
                        part.ScientificName = line[0];
                        part.CommonName = "";
                    }
                    if (!string.IsNullOrEmpty(part.ScientificName))
                        if (logic.CreateBodyPart(part.ScientificName, part.CommonName, part.CultureName) == 0)
                        {
                            Console.WriteLine(part.ScientificName + " failed");
                        }
                }
            });
        }
    }
}
