﻿using System;
using WSD.Consensus.Domain.Models;
using WSD.Consensus.Core.Contracts;
using WSD.Consensus.DbSeeder.Seeders.Helpers;
using System.Threading.Tasks;

namespace WSD.Consensus.DbSeeder.Seeders
{
    public class SymptomsSeeder : ISeeder
    {
        public async Task SeedDatabaseAsync()
        {
            var logic = AutofacProvider.Resolve<ISymptomBusinessLogic>();
            var comps = FileHelper.FileToParsableComponents("./SeedFiles/ConcensusSymptoms.txt");
            await Task.Factory.StartNew(() =>
            {
                foreach (var line in comps)
                {
                    var part = new SymptomModel
                    {
                        CultureName = "US"
                    };
                    if (line.Length > 1)
                    {
                        part.CommonName = line[1] ?? "";
                        part.ScientificName = line[0];
                    }
                    else
                    {
                        part.ScientificName = line[0];
                        part.CommonName = "";
                    }
                    if (!string.IsNullOrEmpty(part.ScientificName))
                        if (0 == logic.CreateSymptom(part.ScientificName, part.CommonName, part.CultureName))
                        {
                            Console.WriteLine(part.ScientificName + " failed");
                        }

                }
            });
        }
    }
}
