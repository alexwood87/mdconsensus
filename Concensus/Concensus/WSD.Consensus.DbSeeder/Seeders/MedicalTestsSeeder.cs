﻿using WSD.Consensus.Core.Contracts;
using WSD.Consensus.Domain.Models;
using System;
using System.Threading.Tasks;
using WSD.Consensus.DbSeeder.Seeders.Helpers;

namespace WSD.Consensus.DbSeeder.Seeders
{
    public class MedicalTestsSeeder : ISeeder
    {
        public async Task SeedDatabaseAsync()
        {
            var logic = AutofacProvider.Resolve<IMedicalTestBusinessLogic>();
            var comps = FileHelper.FileToParsableComponents("./SeedFiles/ConcensusMedicalTests.txt");
            await Task.Factory.StartNew(() =>
            {
                foreach (var line in comps)
                {
                    var part = new MedicalTestModel
                    {
                        Name = line[0]
                    };

                    if (0 == logic.CreateMedicalTest(null, part, null, null))
                    {
                        Console.WriteLine(part.Name + " failed");
                    }
                }
            });
        }
    }
}
