﻿
using System.Threading.Tasks;

namespace WSD.Consensus.DbSeeder.Seeders
{
    public interface ISeeder
    {
        Task SeedDatabaseAsync();
    }
}
