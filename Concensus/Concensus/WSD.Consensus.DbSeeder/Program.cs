﻿using WSD.Consensus.Configuration.Helpers;
using System;
using WSD.Consensus.DbSeeder.Seeders;
using System.Threading.Tasks;
using WSD.Consensus.DbSeeder.Seeders.Helpers;
using Microsoft.Extensions.Configuration;
using Autofac;

namespace WSD.Consensus.DbSeeder
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var builder = new ContainerBuilder();
            var config = new ConfigurationBuilder();
            config.AddJsonFile("appsettings.json");
            builder.RegisterInstance<IConfiguration>(config.Build());
            builder.RegisterModule(new DependencyInjectorModule());
            AutofacProvider.Container = builder.Build();
    
            ISeeder[] seeders = {
                new SymptomsSeeder(),
                new DiseasesSeeder(),
                new BodyPartsSeeder(),
                new MedicalTestsSeeder()
            };
            foreach(var s in seeders)
            {
                await s.SeedDatabaseAsync();
            }
            Console.Read();
        }
    }
}
