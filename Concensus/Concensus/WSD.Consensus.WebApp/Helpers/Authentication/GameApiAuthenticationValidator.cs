﻿using System.Collections.Generic;
using WSD.Consensus.Presentation.Web.Authentication;

namespace WSD.Consensus.WebApp.Helpers.Authentication
{
    public class GameApiAuthenticationValidator : ConcensusAuthorizedValidator
    {
        public GameApiAuthenticationValidator() : base(new List<KeyValuePair<string, string>> { 
            new KeyValuePair<string, string>("unitygameapi","skdfguskgfdkxcfusgkufgr"),
            new KeyValuePair<string, string>("unitysimulatorapi", "saiudugfusedgfusdgugf")
        })
        {
        }
    }
}
