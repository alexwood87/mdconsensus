﻿using System.Configuration;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using WSD.Consensus.Configuration.Helpers;
using Autofac.Extensions.DependencyInjection;
using Autofac;

namespace WSD.Consensus.WebApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });


            services.AddMvc(o => o.EnableEndpointRouting = false).AddNewtonsoftJson();
            if(ConfigurationManager.ConnectionStrings["SQLSessionServer"] == null)
            {
                services.AddDistributedMemoryCache();
            }
            else
            {
                services.AddDistributedSqlServerCache((options) => {
                    options.ConnectionString = ConfigurationManager.ConnectionStrings["SQLSessionServer"].ConnectionString;
                    options.TableName = ConfigurationManager.AppSettings["SQLSessionServerTableName"];
                });
            }
            services.AddSession();
            services.AddAutofac((builder) => builder.RegisterModule(new DependencyInjectorModule()));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.EnvironmentName == "DEVELOPMENT")
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
            app.UseSession();

        }
    }
}
