﻿var vertexShaderText =
[
'precision mediump float;',
'',
'attribute vec3 aVertexPosition;',
'attribute vec3 aVertexNormal;',
'attribute vec3 aTextureCoord;', ,
'varying vec3 fragTextCoord;',
'void main()',
'{',
'  fragTextCoord = aVertexPosition;',
'  gl_Position = vec4(aVertexPosition, 1.0);',
'}'
].join('\n');

var fragmentShaderText =
[
'precision mediump float;',
'',
'varying vec3 fragTextCoord;',
'uniform sampler2D sampler;',
'',
'void main()',
'{',
'  gl_FragColor = texture2D(sampler, vec2(fragTextCoord[0],fragTextCoord[1]));',
'}'
].join('\n');
function readTextFile(file) {
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", file, true);
    rawFile.onreadystatechange = function () {
        if (rawFile.readyState === 4) {
            var allText = rawFile.responseText;
            //document.getElementById("textSection").innerHTML = allText;
            LoadGameModel(allText);
        }
    }
   
    rawFile.send();
}

var filesToDraw = [];
function LoadGameModel(objString) {
    filesToDraw.push(objString);
    var mesh = new OBJ.Mesh(objString);
    // compile the shaders and create a shader program
    shaderProgram = gl.createProgram();
    // compilation stuff here
    gl.clearColor(0.75, 0.85, 0.8, 1.0);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    gl.enable(gl.DEPTH_TEST);
    gl.enable(gl.CULL_FACE);
    gl.frontFace(gl.CCW);
    gl.cullFace(gl.BACK);

    //
    // Create shaders
    // 
    vertexShader = gl.createShader(gl.VERTEX_SHADER);
    fragmentShader = gl.createShader(gl.FRAGMENT_SHADER);

    gl.shaderSource(vertexShader, vertexShaderText);
    gl.shaderSource(fragmentShader, fragmentShaderText);

    gl.compileShader(vertexShader);
    if (!gl.getShaderParameter(vertexShader, gl.COMPILE_STATUS)) {
        console.error('ERROR compiling vertex shader!', gl.getShaderInfoLog(vertexShader));
        return;
    }

    gl.compileShader(fragmentShader);
    if (!gl.getShaderParameter(fragmentShader, gl.COMPILE_STATUS)) {
        console.error('ERROR compiling fragment shader!', gl.getShaderInfoLog(fragmentShader));
        return;
    }

    shaderProgram = gl.createProgram();
    gl.attachShader(shaderProgram, vertexShader);
    gl.attachShader(shaderProgram, fragmentShader);
    gl.linkProgram(shaderProgram);
    if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
        console.error('ERROR linking program!', gl.getProgramInfoLog(shaderProgram));
        return;
    }
    gl.useProgram(shaderProgram);
    // make sure you have vertex, vertex normal, and texture coordinate
    // attributes located in your shaders and attach them to the shader program
    if (!shaderProgram) {
        alert('no shader');
        return;
    }
    gl.validateProgram(shaderProgram);
  
    if (!gl.getProgramParameter(shaderProgram, gl.VALIDATE_STATUS)) {
        alert(gl.getProgramInfoLog(shaderProgram));
        return;
    }
    shaderProgram.vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
    if (!shaderProgram.vertexPositionAttribute) {
        alert('no vertex');
        return;
    }
    gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);

    shaderProgram.vertexNormalAttribute = gl.getAttribLocation(shaderProgram, "aVertexNormal");
    gl.enableVertexAttribArray(shaderProgram.vertexNormalAttribute);

    shaderProgram.textureCoordAttribute = gl.getAttribLocation(shaderProgram, "aTextureCoord");
    gl.enableVertexAttribArray(shaderProgram.textureCoordAttribute);

    // create and initialize the vertex, vertex normal, and texture coordinate buffers
    // and save on to the mesh object
    OBJ.initMeshBuffers(gl, mesh);

    // now to render the mesh
    gl.bindBuffer(gl.ARRAY_BUFFER, mesh.vertexBuffer);
    gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, mesh.vertexBuffer.itemSize, gl.FLOAT, false, 0, 0);

    // it's possible that the mesh doesn't contain
    // any texture coordinates (e.g. suzanne.obj in the development branch).
    // in this case, the texture vertexAttribArray will need to be disabled
    // before the call to drawElements
    if (!mesh.textures.length) {
        gl.disableVertexAttribArray(shaderProgram.textureCoordAttribute);
    }
    else {
        // if the texture vertexAttribArray has been previously
        // disabled, then it needs to be re-enabled
        gl.enableVertexAttribArray(shaderProgram.textureCoordAttribute);
        gl.bindBuffer(gl.ARRAY_BUFFER, mesh.textureBuffer);
        gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, mesh.textureBuffer.itemSize, gl.FLOAT, false, 0, 0);
    }

    gl.bindBuffer(gl.ARRAY_BUFFER, mesh.normalBuffer);
    gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute, mesh.normalBuffer.itemSize, gl.FLOAT, false, 0, 0);

    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, mesh.indexBuffer);
   
    gl.drawElements(gl.TRIANGLES, mesh.indexBuffer.numItems, gl.UNSIGNED_SHORT, 0);
}

var gl;
var canvas;
var bp = ['/gamemodels/Patient_{0}_{1}.obj', 'Nurse_{0}_{1}.obj'];
var map = '/gamemodels/HospitalMap.obj';
var outsideMap = "/gamemodels/HospialMapOutside.obj"; 
function StartHeartBeat() {
    $.ajax({
        url : '/Game/HeartBeat'
    });
    setTimeout(StartHeartBeat, 10000);
}
function GameInit() {
    canvas = document.getElementById('thegamecanvas');
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    gl = canvas.getContext('webgl');
    if (!gl) {
        gl = canvas.getContext('experimental-webgl');
        if (!gl) {
            alert('Your browser does not support Web GL please upgrade to Firefox, Chrome, Safari 6+ or Microsoft Edge');
            return;
        }
    }
    StartHeartBeat();
    $('#txtMedicalTest').autocomplete({
        source: '/Game/MedicalCaseAutoComplete'
    });
    $('#btnMakeMove').click(function () {
        $.ajax({
            url: '/Game/MakeMove',
            data: {
                caseId: currentCaseId,
                medicalTestId : $('#txtMedicalTest').val().split(' ')[0] 
            },
            success: function(data) {
                if (data && data.Success) {
                    if (data.HasWon) {
                        alert('You Won');
                        patientPos[0] = 0;
                        patientPos[1] = 1;
                        patientPos[2] = 0;
                        LoadInsideMap();
                    } else {
                        if (data.Diseases) {
                            var sb = '';
                            for (var j in data.Diseases) {
                                sb += ' ' + data.Diseases[j].Name;
                            }
                            alert('You discovered the paitent has Diseases - ' + sb);
                        } else {
                            alert("Your patient's test was negative");
                        }
                    }
                }
            }
        });
    });
    readTextFile('/gamemodels/HospitalMapOutside.obj');
    window.onclick = function() {
        LoadInsideMap();
    };
   
}

var vertexShader;
var fragmentShader;
var shaderProgram;
function HandleCollisions() {
    for (var i = 0; i < patientPos.length; i += 3) {
        if (patientPos[i] == playerPos[0]) {
            if (patientPos[i + 1] == playerPos[1]) {
                if (patientPos[i + 2] == playerPos[2]) {
                    var patientId = i / 3;
                    try {
                        currentCaseId = $('#hfCase').val().split(';')[patientId];
                        $.ajax({
                            url: '/Game/StartGame?caseId=' + currentCaseId,
                            success: function(data) {
                                LoadControls();
                            },
                            error: function(a, b, c) {
                                alert(c);
                            }
                        });
                    } catch (e) {
                        alert(e);
                    }
                }
            }
        }
    }
}

var currentCaseId = 0;
function LoadControls() {
    playerPos[1]--;
    
}
function Update() {
    HandleCollisions();
    for (var i in filesToDraw) {
        LoadGameModel(filesToDraw[i]);
    }
}
var currentlyPressedKeys = {};
var mapSize = [50,50,50];
function handleKeyDown(event) {
    currentlyPressedKeys[event.keyCode] = true;
    var key = String.fromCharCode(event.keyCode);
    if (key == "D") {
        playerPos[0]++;
        if (playerPos[0] > mapSize[0]) {
            playerPos[0] = mapSize[0];
        }
    } else if(key == "A") {
        playerPos[0]--;
        if (playerPos[0] < 0) {
            playerPos[0] = 0;
        }
    } else if (key == "W") {
        playerPos[2]++;
        if (playerPos[2] > mapSize[2]) {
            playerPos[2] = mapSize[2];
        }
    } else if (key == "S") {
        playerPos[2]--;
        if (playerPos[2] < 0) {
            playerPos[2] = 0;
        }
    }
}

function handleKeyUp(event) {
    currentlyPressedKeys[event.keyCode] = false;
}
function LoadLoop() {
    window.onkeydown= handleKeyDown;
    window.onkeyup = handleKeyUp;
    playerPos[0] = 0;
    playerPos[1] = 1;
    playerPos[2] = 0;
    Update();
    requestAnimationFrame(Update);
}

var playerPos = [];
var patientPos = [];
function LoadInsideMap() {
    readTextFile('/gamemodels/HospitalMap.obj');
    $.ajax({
        url: '/Game/SearchCases',
        success: function(data) {

            if (data.Success) {
                
                for (var i in data.Cases) {
                    patientPos.push(0);
                    patientPos.push(1);
                    patientPos.push(2 * i);
                    var race = data.PatientsRaces[i].Value;
                    var gender = data.PatientsGenders[i].Value;
                    $('#hfCase').val($('#hfCase').val() + ';' + data[i].MedicalCaseId);
                    readTextFile(bp[0].replace('{0}', race.toString()).replace('{1}',gender.toString()));
                }
                for (var j = 0; j < 3; j++) {
                    readTextFile(bp[1].replace('{0}', j.toString()).replace('{1}','1'));
                }

            }
            
            LoadLoop();
        },
        error: function(a, b, c) {
            alert(c);
        }
    });
    
}
