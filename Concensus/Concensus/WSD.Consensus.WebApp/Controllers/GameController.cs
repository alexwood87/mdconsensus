﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using WSD.Consensus.Domain.Models;
using WSD.Consensus.Core.Contracts;
using WSD.Consensus.Presentation.Extensions;
using Microsoft.AspNetCore.Http;

namespace WSD.Consensus.WebApp.Controllers
{
    public class GameController : Controller
    {    
        private readonly IUserGameBusinessLogic _userGameBusinessLogic;      
        private readonly IMedicalCaseBusinessLogic _medicalCaseBusinessLogic;
        private readonly IMedicalTestBusinessLogic _medicalTestBusinessLogic;
        private readonly IMedicalCaseRatingBusinessLogic _medicalCaseRatingBusinessLogic;  

        public GameController(
          IUserGameBusinessLogic userGameBusinessLogic,    
          IMedicalCaseBusinessLogic medicalCaseBusinessLogic,
          IMedicalCaseRatingBusinessLogic medicalCaseRatingBusinessLogic,
          IMedicalTestBusinessLogic medicalTestBusinessLogic)
        {               
            _medicalTestBusinessLogic = medicalTestBusinessLogic;                      
            _userGameBusinessLogic = userGameBusinessLogic;         
            _medicalCaseBusinessLogic = medicalCaseBusinessLogic;
            _medicalCaseRatingBusinessLogic = medicalCaseRatingBusinessLogic;
        }
        // GET: Ga
        // GET: Game
        public ActionResult Index()
        {
            if (GetCurrentUser() == null)
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        public JsonResult MedicalTestsAutoComplete(string term)
        {
            try
            {
                return Json(_medicalTestBusinessLogic.SearchMedicalTests(null, term, 
                    GetCurrentUser().CultureName).Select(x => x.Id + ' ' + x.Name));
            }
            catch (Exception ex)
            {
                return
                    Json(new { Error = ex.Message, FriendlyMessage = "Error Loading Medical Cases", Success = false});
            }

        }

        private static readonly char[] chars = new[]
        {
            'a','b','c','d','e','f','g','h','i','j','k',
            'l','m','n','o','p','q','r','s','t', 'u','v','w',
            'x', 'y', 'z'
        };
        private static char GetRandomChar()
        {
            var x = Math.Abs(new Random().Next())%26;
            return chars[x];
        }

        public JsonResult SearchCases()
        {
            try
            {
                var cases = _medicalCaseBusinessLogic.Search(new MedicalCaseSearch
                {
                    Name = GetRandomChar().ToString()
                }, 20, 1);
                if (cases == null || cases.Count == 0)
                {
                    cases = new List<MedicalCaseModel>{ _medicalCaseBusinessLogic.FindMedicalCase(new Guid("26003AF1-565D-45E5-9508-906235ABC32F"))};
                }
                return Json(new {
                   Cases = cases,
                   PatientsRaces = cases.Where(c => c.Patient.DemographicInformation != null).Select(x => x.Patient.DemographicInformation.First(y => y.DemographicType == DemographicType.Race)).ToList(),
                    PatientsGenders = cases.Where(c => c.Patient.DemographicInformation != null).Select(x => x.Patient.DemographicInformation.First(y=>y.DemographicType == DemographicType.Gender)).ToList(),
                    Success = true
                    });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Error = ex.Message,
                    Success = false,
                    FriendlyMessage = "Failed To Get Medical Cases"
                });
            }
        }

        private UserModel GetCurrentUser()
        {
            return HttpContext.Session.GetModel<UserModel>("User");
        }

        public JsonResult MakeMove(string caseId, long medicalTestId, string medicalTestName)
        {
            try
            {
                var user = GetCurrentUser();
                var games = _userGameBusinessLogic.SearchUserGames(new UserGameSearch
                {
                    UserGameUserId = user.Id
                }, 1, int.MaxValue);
                var medCase = _medicalCaseBusinessLogic.Search(new MedicalCaseSearch
                {
                    MedicalCaseId = new Guid(caseId)
                }, 1, 1);
                if (games == null)
                {
                    if (_userGameBusinessLogic.CreateGame(new UserGameModel
                    {
                        MedicalCase =  medCase.First(),
                        IsFinished = false,
                        GameName = user.Email + ' ' + medCase.First().MedicalCaseName,
                        Player = user,
                        TestsDone =
                            _medicalTestBusinessLogic.SearchMedicalTests(null, medicalTestName, GetCurrentUser().CultureName),
                        Score = 0
                    }) == null)
                    {
                        throw new Exception("Cannot Create Game");
                    }

                    games = _userGameBusinessLogic.SearchUserGames(new UserGameSearch
                    {
                        UserGameUserId = user.Id
                    }, 1, int.MaxValue);
                }

                var game =
                    games.Where(x => !x.IsFinished).FirstOrDefault(x => x.MedicalCase.Id == new Guid(caseId));
                if (game == null)
                {
                    game = games.FirstOrDefault(x => x.MedicalCase.Id == new Guid(caseId));
                }
                if (game == null)
                {
                    game = new UserGameModel
                    {
                        MedicalCase = medCase.First(),
                        GameName = user.Email + ' ' + medCase.First().MedicalCaseName,
                        GameLevel = 1,
                        IsFinished = false,
                        Player = user,
                        Score = (decimal) 0.0

                    };
                    _userGameBusinessLogic.CreateGame(game);
                    game = _userGameBusinessLogic.SearchUserGames(new UserGameSearch
                    {
                        UserGameUserId = user.Id,
                        UseGameName = user.Email + ' ' + medCase.First().MedicalCaseName
                    }, 1, 1).First();
                }
             
                var testResult = _userGameBusinessLogic.MakeMove(game.Id, medicalTestId);
                game = _userGameBusinessLogic.FindGame(game.Id);
                return Json(
                    new
                    {
                        Success = true,
                        HasWon = game.IsFinished,
                        Diseases = testResult.DiseasesPresent,
                        Accurracy = testResult.Acurracy,
                        GameId =  game.Id
                    });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Error = ex.Message,
                    Success = false,
                    FriendlyMessage = "Unable to make move"
                });
            }
        }

        public JsonResult CreateRate(decimal rating, string caseId, string gameId)
        {
            var cas = _medicalCaseBusinessLogic.Search(new MedicalCaseSearch
            {
                MedicalCaseId = new Guid(caseId)
            }, 1, 1).First();
            var user = GetCurrentUser();
            var urating = new UserRatingModel
            {
                Creator = user,
                AppliedToUser = cas.Creator,
                Rating = rating,
                Weight = (int) user.UserLevel                
            };
            if (_medicalCaseRatingBusinessLogic.CreateMedicalCaseRating(urating.Creator.Id, new Guid(gameId), new Guid(caseId), rating) != Guid.Empty)
            {
                return Json(new
                {
                    Success = true
                });
            }
       
            return Json(new {Success=false, FriendlyMessage ="Unable to rate user"});

        }
        public ViewResult Rate(string caseId, string gameId)
        {
            var cas = _medicalCaseBusinessLogic.Search(new MedicalCaseSearch
            {
                MedicalCaseId = new Guid(caseId)
            }, 1, 1);

            return
                View(new UserRatingModel
                {
                    AppliedToUser = cas.First().Creator,
                    Creator = GetCurrentUser(),
                    Rating = (decimal)0.0,
                    Weight = 0
                });
        }
        public JsonResult HeartBeat()
        {
            HttpContext.Session.SetString("HeartBeatTime", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss fff"));
            return Json(new
            {
                Success = true
            });
        }

        public JsonResult StartGame(string caseId)
        {
            try
            {
                var cas = _medicalCaseBusinessLogic.Search(new MedicalCaseSearch
                {
                    MedicalCaseId = new Guid(caseId)
                }, 1, 1).Single();
                var str =
                    new StringBuilder();
                foreach (var x in cas.SymptomToMedicalCaseModels.Select(x => x.SymptomModel)
                    .Select(x => x.ScientificName)
                    .ToArray())
                {
                    str.Append(x);
                    str.AppendLine();
                }

                var bpStr =
                    new StringBuilder();
                foreach (var x in cas.BodyPartToMedicalCaseModels.Select(x => x.BodyPartModel)
                    .Select(x => x.ScientificName)
                    .ToArray())
                {
                    bpStr.Append(x);
                    bpStr.AppendLine();
                }

                return
                    Json(new
                    {
                        cas.Patient,
                        SymptomString = str.ToString(),
                        BodyPartString = bpStr.ToString(),
                        Success = true
                        
                    });

            }
            catch (Exception ex)
            {
                return
                    Json(new
                    {
                        Error = ex.Message,
                        Success = false,
                        FriendlyMessage = "Failed to load patient with case id " + caseId 
                    });
            }
        }

    }
}