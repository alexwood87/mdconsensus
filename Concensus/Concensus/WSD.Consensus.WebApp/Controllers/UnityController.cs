﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using WSD.Consensus.Presentation.Web.Authentication.Headers;
using WSD.Consensus.Core.Contracts;
using WSD.Consensus.Domain.Models;
using WSD.Consensus.Configuration;
using WSD.Consensus.WebApp.Helpers.Authentication;

namespace WSD.Consensus.WebApp.Controllers
{
    [Authorization(false, typeof(GameApiAuthenticationValidator))]
    public class UnityController : Controller
    {
        private readonly IUserBusinessLogic _userBusinessLogic;
        private readonly IUserGameBusinessLogic _userGameBusinessLogic;
        private readonly IUserRatingBusinessLogic _userRatingBusinessLogic;
        private readonly IMedicalCaseBusinessLogic _medicalCaseBusinessLogic;
        private readonly IMedicalTestBusinessLogic _medicalTestBusinessLogic;
        private readonly IMedicalCaseRatingBusinessLogic _medicalCaseRatingBusinessLogic;
        private readonly IConsensusCache _consensusCache;
        public UnityController(
            IConsensusCache consensusCache,            
            IUserBusinessLogic userBusinessLogic,
            IUserGameBusinessLogic userGameBusinessLogic,
            IUserRatingBusinessLogic userRatingBusinessLogic,
            IMedicalCaseBusinessLogic medicalCaseBusinessLogic,
            IMedicalCaseRatingBusinessLogic medicalCaseRatingBusinessLogic,
            IMedicalTestBusinessLogic medicalTestBusinessLogic)
        {
            _consensusCache = consensusCache;
            _userBusinessLogic = userBusinessLogic;
            _medicalTestBusinessLogic = medicalTestBusinessLogic;
            _userGameBusinessLogic = userGameBusinessLogic;
            _userRatingBusinessLogic = userRatingBusinessLogic;
            _medicalCaseBusinessLogic = medicalCaseBusinessLogic;
            _medicalCaseRatingBusinessLogic = medicalCaseRatingBusinessLogic;
        }
        public JsonResult ValidateUser(string email, string password)
        {
            try
            {
                var user = _userBusinessLogic.ValidateUser(email, password);
                return
                    Json(new { Success = true, Data = user});
            }
            catch(Exception ex)
            {
                return
                    Json(new {Success = false, Error = ex.Message});
            }
        }
        public JsonResult ChangePassword(string email, string oldPassword, string newPassword)
        {
            try
            {
                var user = _userBusinessLogic.ChangePassword(email, oldPassword, newPassword);
                return
                    Json(new { Success = true, Data = user });
            }
            catch (Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.Message });
            }
        }
        public JsonResult CreateUser(string email, string password, string question, string answer, 
            int userLevel, string culture)
        {
            try
            {
                var user = _userBusinessLogic.CreateUser(email, password, question, answer, userLevel, culture ?? "en-UK");
                return
                    Json(new { Success = true, Data = user });
            }
            catch (Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.Message });
            }
        }
        public JsonResult MakeMove(Guid gameId, long medicalTestId)
        {
            try
            {
                var data = _userGameBusinessLogic.MakeMove(gameId, medicalTestId);
                return
                    Json(new { Success = true, Data = data });
            }
            catch (Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.Message });
            }

        }
        public JsonResult SearchUsers(long? id, string email)
        {
            try
            {
                var data = _userBusinessLogic.SearchUsers(id, email);
                return
                    Json(new { Success = true, Data = data });
            }
            catch (Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.Message });
            }
        }
        public JsonResult SearchMedicalTests(long? id, string name, string culture)
        {
            try
            {
                var data = _medicalTestBusinessLogic.SearchMedicalTests(id, name, culture);
                return
                    Json(new { Success = true, Data = data });
            }
            catch (Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.ToString() });
            }
        }
        public JsonResult SearchSymptoms(string culture, string term)
        {
            try
            {
                return
                    Json(new
                    {
                        Data = _consensusCache.SearchSymptoms(
                        term, culture, true),
                        Success = true
                    });
                        /*new MetaDataSearchQuery {
                        CultureName = culture,
                        DoDbLookUpIfFails = true,
                        PageIndex = 0,
                        NumRowsReturned = int.MaxValue,
                        ScientificName = term
                    //}), Success = true});*/
            }
            catch(Exception ex)
            {
                return
                    Json(new { Error  = ex.Message, Success = false });
            }
        }
        public JsonResult SearchBodyParts(string culture, string term)
        {
            try
            {
                return Json(new
                {
                    Data = _consensusCache.SearchBodyPartsByQuery(new MetaDataSearchQuery
                    {
                        CultureName = culture,
                        DoDbLookUpIfFails = true,
                        PageIndex = 0,
                        NumRowsReturned = int.MaxValue,
                        ScientificName = term
                    }),
                    Success = true
                });
            }
            catch (Exception ex)
            {
                return
                    Json(new { Error = ex.Message, Success = false });
            }
        }
        public JsonResult UpdateGame(Guid gameId, decimal? rating, string gameName, long userId, long? medicalTestId)
        {
            try
            {
                var game = _userGameBusinessLogic.SearchUserGames(new UserGameSearch
                {
                    UserGameId = gameId
                }, 0, 1).Single();
                
                var tests = game.TestsDone?.ToList();
                if(tests == null)
                {
                    tests = new List<MedicalTestModel>();
                }
                var user = _userBusinessLogic.SearchUsers(userId, null).Single();
                if (medicalTestId != null)
                {
                    tests.Add(_medicalTestBusinessLogic.SearchMedicalTests(medicalTestId, null, user.CultureName).Single());
                    game.TestsDone = tests;
                }
               
                if (gameName != null)
                {
                    game.GameName = gameName;
                }
                if (rating != null)
                {
                    game.CaseRating = new CaseRatingModel
                    {
                        Creator = user,
                        Rating = rating.Value,
                        WeightModifier = (decimal)user.UserLevel,
                        Game = game,
                        MedicalCase = game.MedicalCase                        
                    };
                }
                var res = _userGameBusinessLogic.UpdateGame(game);
                return
                    Json(new { Success = true, Data = res });
            }
            catch (Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.Message });
            }
        }
        public JsonResult RateGame(Guid gameId, int rating, long userId)
        {
            try
            {
                var game = _userGameBusinessLogic.SearchUserGames(new UserGameSearch
                {
                    UserGameId = gameId
                }, 0, 1).Single();

                var tests = game.TestsDone?.ToList();
                if (tests == null)
                {
                    tests = new List<MedicalTestModel>();
                }
                var user = _userBusinessLogic.SearchUsers(userId, null).Single();
               
                if (rating > 0)
                {
                    game.CaseRating = new CaseRatingModel
                    {
                        Creator = new UserModel { Id = userId },
                        Rating = rating,
                        WeightModifier = (int)user.UserLevel,
                        Game = new UserGameModel { Id = game.Id, 
                        MedicalCase = new MedicalCaseModel { Id = game.MedicalCase.Id } },
                        MedicalCase = game.MedicalCase
                    };
                    
                }
                var res = _medicalCaseRatingBusinessLogic.CreateMedicalCaseRating(userId, game.Id,
                    game.MedicalCase.Id, game.CaseRating.Rating);
                return
                    Json(new { Success = true, Data = res });
            }
            catch (Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.Message + ex.InnerException??"" });
            }
        }



        public JsonResult RateUser(double rating, long raterId, long creatorId)
        {
            try
            {
                var data = 
                    _userRatingBusinessLogic.CreateUserRating(creatorId, raterId, (decimal)rating);
                return
                    Json(new { Data = data, Success = true });
            }
            catch (Exception ex)
            {
                return
                    Json(new {Success = false, Error = ex.Message + "-" + ex.InnerException??"" });
            }
        }
        public JsonResult SearchMedicalCases(Guid? medicalCaseId, long? creatorId, string gameName, string description
            , string diseaseName, string symptomsCommaList, string bodyPartCommaList, 
            int? gender, int? race, int? weight, int? age, string culture, long currentUserId)
        {
           
            try
            {
                var symptomList = Array.Empty<string>();
                var parts = Array.Empty<string>();
                if(!string.IsNullOrEmpty(symptomsCommaList))
                {
                    symptomList = symptomsCommaList.Split(',');
                }
                if(!string.IsNullOrEmpty(bodyPartCommaList))
                {
                    parts = bodyPartCommaList.Split(',');
                }
                List<MedicalCaseModel> games = null;
                if (symptomList.Length == 0 && parts.Length == 0)
                {
                   games = _medicalCaseBusinessLogic.Search(new MedicalCaseSearch
                   {
                       MedicalCaseId = medicalCaseId,
                       CreatorId = creatorId,
                       Name = gameName,
                       Description = description,
                       DiseaseName = diseaseName,
                       UserIdWhosePlayingToIgnore = currentUserId
                   }, 10, 0).ToList();
                }
                else
                {
                    games = new List<MedicalCaseModel>();
                    if (symptomList.Length > 0 && parts.Length == 0)
                    {
                        foreach (var sym in symptomList)
                        {
                            if (!string.IsNullOrEmpty(sym))
                            {
                                games.AddRange(_medicalCaseBusinessLogic.Search(new MedicalCaseSearch
                                {
                                    MedicalCaseId = medicalCaseId,
                                    CreatorId = creatorId,
                                    Name = gameName,
                                    Description = description,
                                    DiseaseName = diseaseName,
                                    Symptoms = _consensusCache.SearchSymptomsByQuery(new MetaDataSearchQuery
                                    {
                                        CultureName = culture,
                                        DoDbLookUpIfFails = true,
                                        PageIndex = 0,
                                        NumRowsReturned = 10,
                                        Id = long.Parse(sym)
                                    }),
                                    UserIdWhosePlayingToIgnore = currentUserId
                                }, 10, 0));
                            }
                        }
                    }
                    else if (symptomList.Length == 0 && parts.Length > 0)
                    {
                        foreach (var p in parts)
                        {
                            if (!string.IsNullOrEmpty(p))
                            {
                                games.AddRange(_medicalCaseBusinessLogic.Search(new MedicalCaseSearch
                                {
                                    MedicalCaseId = medicalCaseId,
                                    CreatorId = creatorId,
                                    Name = gameName,
                                    Description = description,
                                    DiseaseName = diseaseName,
                                    BodyParts = _consensusCache.SearchBodyPartsByQuery(new MetaDataSearchQuery
                                    {
                                        CultureName = culture,
                                        DoDbLookUpIfFails = true,
                                        PageIndex = 0,
                                        NumRowsReturned = 10,
                                        Id = long.Parse(p)
                                    }),
                                    UserIdWhosePlayingToIgnore = currentUserId
                                }, 10, 0));
                            }
                        }
                    }
                    else
                    {
                        foreach (var sym in symptomList)
                        {
                            if(string.IsNullOrEmpty(sym))
                            {
                                continue;
                            }
                            foreach (var p in parts)
                            {
                                if (!string.IsNullOrEmpty(p))
                                {
                                    games.AddRange(_medicalCaseBusinessLogic.Search(new MedicalCaseSearch
                                    {
                                        MedicalCaseId = medicalCaseId,
                                        CreatorId = creatorId,
                                        Name = gameName,
                                        Description = description,
                                        DiseaseName = diseaseName,
                                        BodyParts = _consensusCache.SearchBodyPartsByQuery(new MetaDataSearchQuery
                                        {
                                            CultureName = culture,
                                            DoDbLookUpIfFails = true,
                                            PageIndex = 0,
                                            NumRowsReturned = 10,
                                            Id = long.Parse(p)
                                        }),
                                        Symptoms = _consensusCache.SearchSymptomsByQuery(new MetaDataSearchQuery {
                                            CultureName = culture,
                                            DoDbLookUpIfFails = true,
                                            NumRowsReturned = 10,
                                            PageIndex = 0,
                                            Id = long.Parse(sym)
                                        }),
                                        UserIdWhosePlayingToIgnore = currentUserId

                                    }, 10, 0));
                                }
                            }
                        }
                    }

                }
                if (games == null)
                {
                    games = new List<MedicalCaseModel>();
                }
                if(age != null)
                {
                    games = games.Where(x => x.Patient.DemographicInformation.All(y => y.DemographicType == DemographicType.Age && y.Value == age.Value)).ToList();
                }
                if (culture != null)
                {
                    games = games.Where(x => x.CultureName == culture).ToList();
                }
                if (gender != null)
                {
                    games = games.Where(x => x.Patient.DemographicInformation.All(y => y.DemographicType == DemographicType.Gender && y.Value == gender)).ToList();
                }
                if (race != null)
                {
                    games = games.Where(x => x.Patient.DemographicInformation.All(y => y.DemographicType == DemographicType.Race && y.Value == race)).ToList();
                }
                if (weight != null)
                {
                    games = games.Where(x => x.Patient.DemographicInformation.All(y => y.DemographicType == DemographicType.Weight && y.Value == weight)).ToList();
                }
                return
                    Json(new { Success = true, Data = games.Take(5).ToList() });
            }
            catch (Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.ToString() });
            }
        }
        public JsonResult SearchUserGame(string gameName, long? userId, Guid? userGameId)
        {
            try
            {
                var games = _userGameBusinessLogic.SearchUserGames(new UserGameSearch {
                    UseGameName = gameName,
                    UserGameUserId = userId,
                    UserGameId = userGameId
                },0, 100);
                return
                    Json(new { Success = true, Data = games });
            }
            catch (Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.Message });
            }
        }
        public JsonResult CreateGame(string gameName, long userId, Guid medicalCaseId, int gameLevel)
        {
            try
            {
                var game = _userGameBusinessLogic.CreateGame(new UserGameModel
                {
                    GameName = gameName,
                    Player = _userBusinessLogic.SearchUsers(userId, null).Single(),
                    IsFinished = false,
                    Score = 0,
                    MedicalCase = _medicalCaseBusinessLogic.Search(new MedicalCaseSearch
                    {
                        MedicalCaseId = medicalCaseId
                    }, 1, 0).Single(),
                    GameLevel = gameLevel
                });
                return
                    Json(new { Success = true, Data = game });
            }
            catch (Exception ex)
            {
                return
                    Json(new { Success = false, Error = ex.Message });
            }
        }
    }
}