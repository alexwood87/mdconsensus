﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Microsoft.AspNetCore.Mvc;
using WSD.Consensus.Core.Contracts;
using WSD.Consensus.Domain.Models;
using Microsoft.AspNetCore.Http;
using WSD.Consensus.Presentation.Extensions;

namespace WSD.Consensus.WebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUserBusinessLogic _userBusinessLogic;

        public HomeController(IUserBusinessLogic userBusinessLogic)
        {
            _userBusinessLogic = userBusinessLogic;
        }
        public IActionResult Tos()
        {
            return View();
        }
        public IActionResult Privacy()
        {
            return View();
        }
        public IActionResult Downloads()
        {
            return
                View();
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        { 

            return View();
        }

        public IActionResult Contact()
        {
           
            return View();
        }

        public IActionResult Register()
        {
            return View();
        }
        private static List<string> GetCultures()
        {
            var count = new List<string>();
            foreach (CultureInfo ci in CultureInfo.GetCultures(CultureTypes.AllCultures))
            {
                RegionInfo ri;
                try
                {
                    ri = new RegionInfo(ci.Name);
                }
                catch
                {
                    // If a RegionInfo object could not be created we don't want to use the CultureInfo
                    //    for the country list.
                    continue;
                }

                // If the country is not alreayd in the countryList add it...
                if (!count.Contains(ri.TwoLetterISORegionName ))
                {
                        count.Add(ri.TwoLetterISORegionName);
                }
             
            }
            return count;
        }
        [HttpGet]
        public IActionResult ChangePassword()
        {
            return View();    
        }

        public IActionResult Error()
        {
            return View();
        }
        [HttpGet]
        public IActionResult GeneratePassword()
        {            
            return View();
        }
        [HttpGet]
        public IActionResult GetQuestion(string email)
        {

            return
                Json(new
                { Data =
                    _userBusinessLogic.SearchUsers(null, email).SingleOrDefault(x => x.Email != null && x.Email.ToLower() == email.ToLower())
                });
        }

        [HttpPost]
        public IActionResult GeneratePassword(string email, string answer)
        {
            try
            {
                if (_userBusinessLogic.GenerateUserPassword(answer, email))
                {
                    return
                        RedirectToAction("ChangePassword");
                }
                return View();
            }
            catch 
            {
                return View();
            }
        }
        [HttpPost]
        public IActionResult ChangePassword(UserModel model, string NewPassword)
        {
            try
            {
                _userBusinessLogic.ChangePassword(model.Email, model.Password, NewPassword);
                return RedirectToAction("Index");
            }
            catch 
            {
                return RedirectToAction("Error");
            }
        }
        public IActionResult SignOnRedirect(string email, string password)
        {
            try
            {
                var success = _userBusinessLogic.ValidateUser(email, password);
                if (success != null)
                {
                    HttpContext.Session.SetModel("User", success.Id.ToString());
                    return Redirect("~/GameCreation/Index");
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch 
            {
                return
                    RedirectToAction("Index");
            }
        }
        public IActionResult SignMeOn(string Username, string Password)
        {
            try
            {
                var success = _userBusinessLogic.ValidateUser(Username, Password);
                if (success != null)
                {
                    HttpContext.Session.SetModel("User", success);
                    return Json(new
                    {
                        Success = true
                    });
                }
                return Json(new { Success = false, FriendlyMessage = "Login Failed" });
            }
            catch 
            {
                return Json(new
                {
                    Success = false
                });
            }

        }

        [HttpGet]
        public IActionResult DashBoard()
        {
            if (HttpContext.Session.GetModel<UserModel>("User") == null)
            {
                return RedirectToAction("Index");
            }
            return View();
        }

        public IActionResult RegisterMe(string Username, string Password, string Question, string EducationalType,
            string Answer)
        {
            try
            {
                _userBusinessLogic.CreateUser(Username, Password, Question, Answer, int.Parse(EducationalType),
                    Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName);
                return
                    Json(new
                    {
                        Success = true
                    });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    FriendlyMessage = "We Failed To Register You",
                    Error = ex.Message
                });
            }

        }
    }
}