﻿function RemoveTypeCreation(atag) {
    $(atag).parent().remove();
}
function RemoveTypeCreationTest(atag) {
    var medTest = $('#medicaltests').val().split(';');
    var name = $(atag).prev().text().split(':')[0];
    var sb = '';
    for (var r in medTest) {
        if (r == name) {
            continue;;
        }
        sb += r+ ';';
    }
    $('#medicaltests').val(sb);
    $(atag).parent().remove();
}
$(document).ready(function () {
    $('.autocompletedisease').autocomplete({
        source: '/GameCreation/DiseaseAutoComplete'
    });
    $('.autocompletedisease').on('input', function () {
        $(this).autocomplete("search", $(this).val());
    });
    
    $('.autocompletebodyparts').autocomplete({
        source: '/GameCreation/BodyPartsAutoComplete',
        select: function (event, ui) {
            var bp = ui.item.value;
         
            $('#hfBodyParts').val($('#hfBodyParts').val() + ';' + bp);
            $('#lstBodyParts').append('<li>' + bp + ' <a onclick="RemoveTypeCreation(this);">Remove</a></li>');
        }
    });
  
    $('.autocompletebodyparts').on('input', function () {
        $(this).autocomplete("search", $(this).val());
    });
   
    $('.autocompleteSymptoms').autocomplete({
        source: '/GameCreation/SymptomAutoComplete',
        select: function (event, ui) {
            var bp = ui.item.value;
            $('#hfSymptoms').val($('#hfSymptoms').val() + ';' + bp);
            $('#lstSymptoms').append('<li>' + bp + ' <a onclick="RemoveTypeCreation(this);">Remove</a></li>');
        }
    });
    $('.autocompleteSymptoms').on('input', function () {
        $(this).autocomplete("search", $(this).val());
    });

    $('#txtMedicalTestName').autocomplete({
        source: '/GameCreation/MedicalTestAutoComplete',
        select: function (event, ui)
        {
            var bp = ui.item.value;
            $('#lstMedicalTests').append('<li>' + bp + '<a onclick="RemoveTypeCreationTest(this)>Remove</a></li>');
            $('#medicaltests').val($('#medicaltests').val() + ';' + bp.split(':')[0]);
        }        
    });

 
    $('#txtMedicalTestName').on('input', function () {
        $(this).autocomplete("search", $(this).val());
    });
    
    $('.fancyboxwithreload').fancybox({
        type: 'iframe',
        onClose: function() {
            window.location.reload();
        }
    });
   
    $('#medicalTestsPopup').fancybox({
        type: 'iframe'
    });
    $('.fancyboxWindow').fancybox({
        type: 'iframe'
    });

  
});