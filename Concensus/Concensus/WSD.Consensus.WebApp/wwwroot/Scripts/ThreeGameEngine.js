﻿var container;

var camera, scene, renderer;

var mouseX = 0, mouseY = 0;

var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;


function PlaySound(filename, shouldLoop) {
    var myAudio = new Audio(filename);
    if (shouldLoop) {
        if (typeof myAudio.loop == 'boolean') {
            myAudio.loop = true;
        } else {
            myAudio.addEventListener('ended', function() {
                this.currentTime = 0;
                this.play();
            }, false);
        }
    }
    myAudio.play();
}
function LoadDea(filename, x, y, z, rotationArray)
{
    if(!scene)
    {
        return;
    }
    var l = new THREE.ColladaLoader();
    l.load(filename, function (result) {
        result.scene.position.set(x, y, z);
        if (rotationArray && rotationArray.length > 2)
        {
            result.scene.rotation.set(rotationArray[0], rotationArray[1], rotationArray[2]);
        }
        scene.add(result.scene);
    });
}
var vertexShader;
var fragmentShader;
var shaderProgram;
function HandleCollisions() {
    for (var i = 0; i < patientPos.length; i += 3) {
        if (patientPos[i] == playerPos[0]) {
            if (patientPos[i + 1] == playerPos[1]) {
                if (patientPos[i + 2] == playerPos[2]) {
                    var patientId = i / 3;
                    try {
                        currentCaseId = $('#hfCase').val().split(';')[patientId+1];
                        
                        $.ajax({
                            url: '/Game/StartGame?caseId=' + currentCaseId,
                            success: function (data) {
                                if (data.Success) {
                                         
                                    DrawText('Patient Name:' + data.Patient.FirstName + ' ' + data.Patient.LastName , 4, [-50,16,0]);
                                    if (data.SymptomString) {
                                        DrawText('Symptoms: ' + data.SymptomString, 4, [-50, 12, 0]);
                                    }
                                    if (data.BodyPartString) {
                                        DrawText('Body Parts: ' + data.BodyPartString , 4, [-50, 8, 0]);
                                    }
                                    DrawText('Use the textbox to enter a medical test to try on the patient' , 4, [-50, 4, 0]);
                                   
                                    LoadControls();
                                } else {
                                    alert(data.FriendlyMessage);
                                }
                            },
                            error: function (a, b, c) {
                                alert(c);
                            }
                        });
                    } catch (e) {
                        alert(e);
                    }
                }
            }
        }
    }
}
function createRepeatingPlane(url, pos, rotation) {
    var geometry = new THREE.PlaneGeometry(2000, 2000, 5, 5);
    geometry.applyMatrix(new THREE.Matrix4().makeRotationX(-Math.PI / 2));
    var texture = THREE.ImageUtils.loadTexture(url);
    texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
    texture.repeat.set(64, 64);
    var material = new THREE.MeshBasicMaterial({ color: 0xffffff, map: texture });
    var me = new THREE.Mesh(geometry, material);
    if (pos && pos.length > 2) {
        me.position.y = pos[1];
        me.position.x = pos[0];
        me.position.z = pos[2];
        me.rotation.y = rotation[1];
        me.rotation.x = rotation[0];
        me.rotation.z = rotation[2];
    }
    return me;
}
var filesToDraw = [];
var currentCaseId = 0;
function LoadControls() {
    playerPos[1]--;

}
function Update() {
    HandleCollisions();
    for (var i in filesToDraw) {
        scene.add(filesToDraw[i]);
    }
}
var currentlyPressedKeys = {};
var mapSize = [50, 50, 50];
function handleKeyDown(event) {
    currentlyPressedKeys[event.keyCode] = true;
    var key = String.fromCharCode(event.keyCode);
    if (key == "D") {
        playerPos[0]++;
        if (playerPos[0] > mapSize[0]) {
            playerPos[0] = mapSize[0];
        }
    } else if (key == "A") {
        playerPos[0]--;
        if (playerPos[0] < -1*mapSize[0]) {
            playerPos[0] = -1*mapSize[0];
        }
    } else if (key == "W") {
        playerPos[2]++;
        if (playerPos[2] > mapSize[2]) {
            playerPos[2] = mapSize[2];
        }
    } else if (key == "S") {
        playerPos[2]--;
        if (playerPos[2] < -1*mapSize[2]) {
            playerPos[2] = -1*mapSize[2];
        }
    }
}

function handleKeyUp(event) {
    currentlyPressedKeys[event.keyCode] = false;
}
function LoadLoop() {
    //window.onkeydown = handleKeyDown;
    //window.onkeyup = handleKeyUp;
    playerPos[0] = 0;
    playerPos[1] = 1;
    playerPos[2] = 0;
    Update();
    requestAnimationFrame(Update);
}
function readTextFile(modelPath, modelTextures, xyz) {
    DrawModel(modelPath, modelTextures, xyz);
}
var playerPos = [];
var patientPos = [];
function LoadInsideMap() {
    lastText = [];
    scene = new THREE.Scene();
    camera.position.z = 50;
    camera.position.x = 50;
    var ambient = new THREE.AmbientLight(0xffffff);
    scene.add(ambient);
    for (var i = -10; i < 10; i++) {
        var directionalLight = new THREE.DirectionalLight(0xffffff);
        directionalLight.position.set(0, 1, i);
        scene.add(directionalLight);
    }
    // texture
    scene.add(createRepeatingPlane('/gamemodels/hospitalfloortexture.jpg', [0,0,0], [0,0,0]));
    scene.add(createRepeatingPlane('/gamemodels/hospitalceilingtexture.jpg', [0,50,0], [Math.PI,0,0]));
    scene.add(createRepeatingPlane('/gamemodels/hospitalwalls.jpg', [100, 0, 0], [0, 0, Math.PI/2.0]));
    scene.add(createRepeatingPlane('/gamemodels/hospitalwalls.jpg', [-100, 0, 0], [0, 0, -Math.PI / 2.0]));
    scene.add(createRepeatingPlane('/gamemodels/hospitalwalls.jpg', [0, 0, 100], [-Math.PI / 2.0, 0, 0]));
    scene.add(createRepeatingPlane('/gamemodels/hospitalwalls.jpg', [0, 0, -100], [Math.PI/2.0, 0, 0]));
    /*
        Play Hospital Sounds
    
    */
    try {
        PlaySound('/Sounds/BackgroundNoise.wav', true);
    } catch (ex) {
        
    }
    //readTextFile('/gamemodels/HospitalMap/HospitalFloor.obj', null, null);//'/gamemodels/HospitalMap/hospitalroom/_Color_004_2.jpg');
    $.ajax({
        url: '/Game/SearchCases',
        success: function (data) {

            if (data.Success) {
                var j = 0;
                for (var i in data.Cases) {
                    patientPos.push(0);
                    patientPos.push(1);
                    patientPos.push(2 * i);
                    var race = 1;
                    var gender = 1;
                    try {
                        race = data.PatientsRaces[i].Value;
                        gender = data.PatientsGenders[i].Value;
                    } catch (ex) {
                        
                    }

                    $('#hfCase').val($('#hfCase').val() + ';' + data.Cases[i].Id);//MedicalCaseId
                    LoadDea('/gamemodels/patientnurses.dae', 0, 0, 2 * i++, [30, 0, 0]);
                  
                }
           

            }
            $(window).unbind();
            LoadLoop();
            //renderer = new THREE.WebGLRenderer();
            renderer.setPixelRatio(window.devicePixelRatio);
            renderer.setSize(window.innerWidth, window.innerHeight);
            render();
            //container.appendChild(renderer.domElement);
        },
        error: function (a, b, c) {
            alert(c);
            //renderer = new THREE.WebGLRenderer();
            renderer.setPixelRatio(window.devicePixelRatio);
            renderer.setSize(window.innerWidth, window.innerHeight);
            render();
            //container.appendChild(renderer.domElement);
        }
    });
    
}

var onProgress = function (xhr) {
    if (xhr.lengthComputable) {
        var percentComplete = xhr.loaded / xhr.total * 100;
        console.log(Math.round(percentComplete, 2) + '% downloaded');
    }
};

var onError = function (xhr) {
};
function init() {
    
    container = document.getElementById('container'); 
    $('#container').click(function () {
        LoadInsideMap();
        document.getElementById('btnUp').onclick = function () {
            playerPos[2]--;
            if (playerPos[2] < -1 * mapSize[2]) {
                playerPos[2] = -1 * mapSize[2];
            }
          
        };
        document.getElementById('btnDown').onclick = function () {
            playerPos[2]++;
            if (playerPos[2] > mapSize[2]) {
                playerPos[2] = mapSize[2];
            }
        };
        document.getElementById('btnRight').onclick = function () {
            playerPos[0]++;
            if (playerPos[0] > mapSize[0]) {
                playerPos[0] = mapSize[0];
            }
        };
        document.getElementById('btnLeft').onclick = function () {
            playerPos[0]--;
            if (playerPos[0] < -1 * mapSize[0]) {
                playerPos[0] = -1 * mapSize[0];
            }
        };
        window.onclick = function () { return true;};
    });
    //document.body.appendChild( container );
    if (!container) {
        alert('container is null');
    }
    //camera = new THREE.PerspectiveCamera(90, 1, 1, 2000);//window.innerWidth / window.innerHeight, 1, 2000);
    camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);

    camera.position.z = 200;
    camera.position.x = -100;
    //camera.position.y = 100;
    // scene

    scene = new THREE.Scene();

    var ambient = new THREE.AmbientLight(0xffffff);
    scene.add(ambient);

    var directionalLight = new THREE.DirectionalLight(0xffffff);
    directionalLight.position.set(0, 0, 1);
    scene.add(directionalLight);
    DrawText('Welcome', 12, [-50,4,0]);
    // texture

    var manager = new THREE.LoadingManager();
    manager.onProgress = function (item, loaded, total) {

        console.log(item, loaded, total);

    };
    
   

    $('#txtMedicalTest').autocomplete({
        source: '/GameCreation/MedicalTestAutoComplete'
    });
    $('#btnMakeMove').click(function () {
        $.ajax({
            url: '/Game/MakeMove',
            data: {
                caseId: currentCaseId,
                medicalTestId: $('#txtMedicalTest').val().split(':')[0],
                medicalTestName :$('#txtMedicalTest').val().split(':')[2]
            },
            success: function (data) {
                if (data && data.Success) {
                    if (data.HasWon) {
                        alert('You Won');
                        patientPos[0] = 0;
                        patientPos[1] = 1;
                        patientPos[2] = 0;
                        LoadInsideMap();
                        $('div').fancybox({
                            href: '/Game/Rate?caseId=' + currentCaseId + '&gameId=' + data.GameId,
                            type: 'iframe'
                        }).click();
                    } else {
                        if (data.Diseases) {
                            var sb = '';
                            for (var j in data.Diseases) {
                                sb += ' ' + data.Diseases[j].Name;
                            }
                            alert('You discovered the paitent has Diseases - ' + sb);
                        } else {
                            alert("Your patient's test was negative");
                        }
                    }
                }
            },
            error : function(a, b, c) {
                alert(c);
            }
        });
    });
    
    var loader2 = new THREE.ImageLoader(manager);
    var hospitalTextures = [];
    
    
    var textures = [];
   
    LoadDea('/gamemodels/hospital.dae',-180,0,0,[30,0.0,0.0]);
        //
      
        StartHeartBeat();
        renderer = new THREE.WebGLRenderer();
        renderer.setPixelRatio(window.devicePixelRatio);
        renderer.setSize(window.innerWidth, window.innerHeight);

        container.appendChild(renderer.domElement);

        document.addEventListener('mousemove', onDocumentMouseMove, false);
        $(window).on("tap", function () {
            playerPos[2] += 10;
            if (playerPos[2] + camera.position.z > mapSize[2]) {
                playerPos[2] = -1 * mapSize[2];
            }
        });
        $(window).on("swipeleft", function () {
            playerPos[0] -= 10;
            if (playerPos[0] < -1 * mapSize[0]) {
                playerPos[0] = -1 * mapSize[0];
            }

        });
        $(window).on("swiperight", function () {

            playerPos[0] += 10;
            if (playerPos[0] > mapSize[0]) {
                playerPos[0] = mapSize[0];
            }

        });
        //

        window.addEventListener('resize', onWindowResize, false);

        
    
   
}
var lastText = [];
function DrawText(textStr, textSize, pos) {
    var viewport = {
        width  : $(window).width(),
        height : $(window).height()
    };
    if (viewport.width <= 620)
    {
        if (lastText.indexOf(textStr) === -1) {
            lastText.push(textStr);
            alert(textStr);
        }
        return;
    }

    var fLoader = new THREE.FontLoader();
    fLoader.load('/fonts/helvetiker_regular.typeface.js', function (theFont) {
        var textShapes = new THREE.TextGeometry(textStr, {
            size: textSize,
            height: 5,
            curveSegments: 6,
            font: theFont,
            weight: "regular"
        });
        ///var text = new THREE.ShapeGeometry(textShapes);
        var textMesh = new THREE.Mesh(textShapes, new THREE.MeshBasicMaterial({ color: 0x0000ff }));
        textMesh.position.x = pos[0];
        textMesh.position.y = pos[1];
        textMesh.position.z = pos[2];
        scene.add(textMesh);
    });
  
}
var bp = ['/gamemodels/Patient_{0}_{1}.obj', 'Nurse_{0}_{1}.obj'];
var map = '/gamemodels/HospitalMap/HospitalMap.obj';
var outsideMap = "/gamemodels/HospialMapOutside.obj";
function StartHeartBeat() {
    $.ajax({
        url: '/Game/HeartBeat'
    });
    setTimeout(StartHeartBeat, 10000);
}
function DrawModel(pathToModel, modelTextures, coordinates) {
    var manager = new THREE.LoadingManager();
    manager.onProgress = function (item, loaded, total) {

        console.log(item, loaded, total);
        
    };
    var textures = [];
    if (modelTextures && modelTextures.length) {
        var i = 0;
        for (var p in modelTextures) {
            textures[i] = new THREE.Texture();
            var loader2 = new THREE.ImageLoader(manager);

            loader2.load(modelTextures[i], function(image) {

                textures[i].image = image;
                textures[i].needsUpdate = true;

            });
            i++;
        }
    }

    var j = 0;
    var loader = new THREE.OBJLoader(manager);
    loader.load(pathToModel, function (object) {
        
        object.traverse(function (child) {

            if (child instanceof THREE.Mesh) {
                if (modelTextures) {
                    child.material.map = textures[j++];
                }
            }

        });
        filesToDraw.push(object);
        if (coordinates && coordinates.length && coordinates.length > 2) {
            object.position.x = coordinates[0];
            object.position.y = coordinates[1];
            object.position.z = coordinates[2];
        } else {
            object.position.y = 0;
        }
        
       
        scene.add(object);

    }, onProgress, onError);
}
function onWindowResize() {

    windowHalfX = window.innerWidth / 2;
    windowHalfY = window.innerHeight / 2;

    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(window.innerWidth, window.innerHeight);

}

function onDocumentMouseMove(event) {

    mouseX = (event.clientX - windowHalfX) / 2;
    mouseY = (event.clientY - windowHalfY) / 2;

}

//
var zCoordinate;
function animate() {

    requestAnimationFrame(animate);
    HandleCollisions();
    render();

}

function render() {
    if (playerPos && playerPos.length > 2) {
        var xpos = camera.position.x + playerPos[0] * .05 ;
        if (xpos < -100) {
            xpos = -100;
        }
        if (xpos > 100) {
            xpos = 100;
        }
        camera.position.x = xpos;
    } else {
        var xpos1 = camera.position.x;
        if (xpos1 < -100) {
            xpos1 = -100;
        }
        if (xpos1 > 100) {
            xpos1 = 100;
        }
        camera.position.x = xpos1;
    }
    var yC = camera.position.y;
    if (yC < 1) {
        yC = 1;
    }
    camera.position.y = yC;

    if (playerPos && playerPos.length > 2) {
        var zpos = camera.position.z + playerPos[2] * 0.05;;
        if (zpos < -100) {
            zpos = -100;
        }
        if (zpos > 100) {
            zpos = 100;
        }
        camera.position.z = zpos;
    }
    camera.lookAt(scene.position);

    renderer.render(scene, camera);

}