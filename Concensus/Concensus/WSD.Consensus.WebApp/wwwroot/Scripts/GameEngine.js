﻿var program, canvas, gl;

function InitGame() {
    // Get A WebGL context
    canvas = document.getElementById("thegamecanvas");
    gl = canvas.getContext('webgl');
    if (!gl) {
        gl = canvas.getContext("experimental-webgl");
    }
    if (!gl) {
        alert('Browser Not Supported Please Use Chrome Or Firefox');
        return;
    }
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    gl.viewport(0, 0, window.innerWidth, window.innerHeight);
    gl.clearColor(0, 0, 0, 1.0);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
    var vertexShader = gl.createShader(gl.VERTEX_SHADER);
    var fragmentShader = gl.createShader(gl.FRAGMENT_SHADER);
    var vShaderText = [
        'precision mediump float;',
        'attribute vec3 vertPosition;',
        'attribute vec3 vertColor;',
        'uniform mat4 mWorld;',
        'uniform mat4 mProj;',
        'uniform mat4 mView;',
        'varying vec3 fragColor;',
        'void main()',
        '{' +
        'gl_Position = mProj * mView * mWorld * vec4(vertPosition, 1.0);',
        'fragColor = vertColor;',
        '}'
    ].join('\n');
    var fragShaderText = [
         'precision mediump float;',
         'varying vec3 fragColor;',
         'void main()',
         '{',
         'gl_FragColor = vec4(fragColor,1.0);',
         '}'
    ].join('\n');
    gl.shaderSource(vertexShader, vShaderText);
    gl.shaderSource(fragmentShader, fragShaderText);
    gl.compileShader(vertexShader);
    gl.compileShader(fragmentShader);
    if (!gl.getShaderParameter(vertexShader, gl.COMPILE_STATUS) || !gl.getShaderParameter(fragmentShader, gl.COMPILE_STATUS)) {
        alert('Failed to initialize shader');
        return;
    }
    var program = gl.createProgram();
    gl.attachShader(program, vertexShader);
    gl.attachShader(program, fragmentShader);
    gl.linkProgram(program);
    if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
        alert('failed to init program');
        return;
    }
    var verts = [
        0.5, 0.0,  0.0, 1.0, 1.0,0.0,
        -0.5, -0.5, 0.0,0.0,1.0,1.0,
        -0.5, 0.5, 0.0, 1.0, 0.0, 1.0
    ];
    var buf = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, buf);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(verts), gl.STATIC_DRAW);
    var position = gl.getAttribLocation(program, 'vertPosition');
    var color = gl.getAttribLocation(program, 'vertColor');
    gl.vertexAttribPointer(position, //attribute of position
        3, //number of vecs per position
        gl.FLOAT,//datat type
        gl.FALSE,
        6 * Float32Array.BYTES_PER_ELEMENT,//number of bytes per vertex 
        0 //offset from start of array
    );
    gl.vertexAttribPointer(color, //attribute of position
       3, //number of vecs per position
       gl.FLOAT,//datat type
       gl.FALSE,
       6 * Float32Array.BYTES_PER_ELEMENT,//number of bytes per vertex 
       3*Float32Array.BYTES_PER_ELEMENT //offset from start of array
   );
    gl.enableVertexAttribArray(position);
    gl.enableVertexAttribArray(color);
    gl.useProgram(program);
    var matWorld = gl.getUniformLocation(program, 'mWorld');
    var matView = gl.getUniformLocation(program, 'mView');
    var matProj = gl.getUniformLocation(program, 'mProj');

    var projMatrix = new Float32Array(16);
    var viewMatrix = new Float32Array(16);
    var worldMatrix = new Float32Array(16);
    mat4.identity(worldMatrix);
    mat4.lookAt(viewMatrix,[0,0,-5],[0,0,0],[0,1,0]);
    mat4.perspective(projMatrix, glMatrix.toRadian(45), canvas.width/canvas.height,.1, 1000.0);


    gl.uniformMatrix4fv(matWorld, gl.FALSE, worldMatrix);
    gl.uniformMatrix4fv(matView, gl.FALSE, viewMatrix);
    gl.uniformMatrix4fv(matProj, gl.FALSE, projMatrix);
    var identityMatrix = new Float32Array(16);
    mat4.identity(identityMatrix);
    var angle = 0.0;
    var loop = function () {
        angle = performance.now() / 1000.0 / 6 * 2 *Math.PI;
        mat4.rotate(worldMatrix, identityMatrix, angle, [0, 1, 0]);
        gl.uniformMatrix4fv(matWorld, gl.FALSE, worldMatrix);
        gl.clearColor(0,0,0,1.0);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
        gl.drawArrays(gl.TRIANGLES, 0, 3);
        requestAnimationFrame(loop);
    }
    requestAnimationFrame(loop);
    // setup a GLSL program
    //program = createProgramFromScripts(gl, ["2d-vertex-shader", "2d-fragment-shader"]);
    //gl.useProgram(program);

    //// look up where the vertex data needs to go.
    //var positionLocation = gl.getAttribLocation(program, "a_position");

    //// Create a buffer and put a single clipspace rectangle in
    //// it (2 triangles)
    //var buffer = gl.createBuffer();
    //gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
    //gl.bufferData(
    //    gl.ARRAY_BUFFER,
    //    new Float32Array([
    //        -1.0, -1.0,
    //         1.0, -1.0,
    //        -1.0, 1.0,
    //        -1.0, 1.0,
    //         1.0, -1.0,
    //         1.0, 1.0]),
    //    gl.STATIC_DRAW);
    //gl.enableVertexAttribArray(positionLocation);
    //gl.vertexAttribPointer(positionLocation, 2, gl.FLOAT, false, 0, 0);

    //// draw
    //gl.drawArrays(gl.TRIANGLES, 0, 6);
}

function EndGame() {
    
}
function RenderImage(img) {
    // look up where the texture coordinates need to go.
    //var texCoordLocation = gl.getAttribLocation(program, "a_texCoord");
 
    //// provide texture coordinates for the rectangle.
    //var texCoordBuffer = gl.createBuffer();
    //gl.bindBuffer(gl.ARRAY_BUFFER, texCoordBuffer);
    //gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([
    //    0.0,  0.0,
    //    1.0,  0.0,
    //    0.0,  1.0,
    //    0.0,  1.0,
    //    1.0,  0.0,
    //    1.0,  1.0]), gl.STATIC_DRAW);
    //gl.enableVertexAttribArray(texCoordLocation);
    //gl.vertexAttribPointer(texCoordLocation, 2, gl.FLOAT, false, 0, 0);
 
    //// Create a texture.
    //var texture = gl.createTexture();
    //gl.bindTexture(gl.TEXTURE_2D, texture);
 
    //// Set the parameters so we can render any size image.
    //gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    //gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
    //gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
    //gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
 
    //// Upload the image into the texture.
    //gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
   
    
}
function UpdatePlayer() {
    
}
function RenderScene() {
    
}
