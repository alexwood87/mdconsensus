﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using WSD.Consensus.Domain.Entities;
using WSD.Consensus.Domain.Helpers;
using WSD.Consensus.Domain.Models;
using WSD.Consensus.Domain.Repositories;
namespace WSD.Consensus.Infrastructure.Repositories
{
    public class MedicalTestRepository : GenericRepository<MedicalTestEntity>, IMedicalTestRepository
    {
        public MedicalTestRepository(ConsensusContext context) : base(context)
        {
        }

        public List<MedicalTestModel> SearchMedicalTests(long? id, string name, string culture)
        {
            if(id != null)
            {
                return
                    new List<MedicalTestModel> {
                          EntityMapper.Map<MedicalTestEntity,MedicalTestModel>(
                              _context.MedicalTests.Single(x=> x.Id == id))
                        };
            }
            if (string.IsNullOrEmpty(name))
            {
                return null;
            }
            name = name.ToLower().Trim();
            return _context.MedicalTests.Where(x => x.Name.ToLower().Contains(name)).Take(50).ToList()
                .Select(EntityMapper.Map<MedicalTestEntity,MedicalTestModel>).ToList();
        }

        public MedicalTestModel CreateMedicalTest(Guid? medCaseId, MedicalTestModel model, TestResultModel result, 
            DiseaseModel disease)
        {
            using (var trans = new CommittableTransaction(new TimeSpan(0, 1, 0)))
            {
                try
                {

                    var medTest = _context.MedicalTests.SingleOrDefault(x => x.Name.ToLower() == model.Name.ToLower());
                    if (medTest != null)
                    {
                        trans.Commit();
                        return
                            EntityMapper.Map<MedicalTestEntity, MedicalTestModel>(medTest);
                    }    
                    medTest = new MedicalTestEntity { Name = model.Name, AffectiveModifier = model.AffectiveModifer };

                    medTest = Add(medTest);
                    _context.SaveChanges();
                    var testRes = new TestResultEntity();

                    DiseaseEntity d = new DiseaseEntity();
                    if (disease.Id > 0)
                    {
                        d = _context.Diseases.Single(x => x.Id == disease.Id);
                    }
                    else if
                       (!string.IsNullOrEmpty(disease.ScientificName) && !string.IsNullOrEmpty(disease.CultureName))
                        {
                           

                        if (
                            !_context.Diseases.All(
                                x => x.ScientificName != disease.ScientificName || x.CultureName != disease.CultureName))
                        {
                            d =
                                _context.Diseases.First(
                                    x =>
                                        x.CultureName == disease.CultureName &&
                                        x.ScientificName == disease.ScientificName);
                        }
                        else
                        {
                            d.CultureName = disease.CultureName;
                            d.ScientificName = disease.ScientificName;
                            d.CommonName = disease.CommonName;
                            d = _context.Diseases.Add(d).Entity;
                            _context.SaveChanges();
                        }
                        
                    }
                    if (!_context.TestResults.All(x => x.Id != result.Id))
                    {
                        testRes = _context.TestResults.Add(new TestResultEntity
                        {
                            Accuracy = result.Acurracy,
                            MedicalTestId = medTest.Id,

                        }).Entity;
                    }
                    else
                    {
                        testRes = _context.TestResults.Single(x => x.Id == result.Id);
                    }
                    medTest.TestResults.Add(new TestResultEntity
                    {
                        MedicalTestId = medTest.Id,
                        Accuracy = result.Acurracy,
                        Id = testRes.Id
                    });
                    _context.SaveChanges();
                    if (medCaseId != null)
                    {
                        medTest.MedicalCasesToMedicalTests.Add(new MedicalCasesToMedicalTestEntity
                        {
                            MedicalTestId = medTest.Id,
                            MedicalCaseId = medCaseId.Value
                        });
                    }

                    if (string.IsNullOrEmpty(disease.ScientificName) || string.IsNullOrEmpty(disease.CultureName))
                    {
                        return EntityMapper.Map<MedicalTestEntity, MedicalTestModel>(medTest);
                    }
                    medTest.DiseasesToMedicalTests.Add(new DiseasesToMedicalTestEntity
                    {
                        DiseaseId = d.Id
                    });
                    _context.SaveChanges();
                    trans.Commit();
                    return EntityMapper.Map<MedicalTestEntity, MedicalTestModel>(medTest); ;
                }
                catch (Exception ex)
                {
                    trans.Rollback(ex);
                    return null;
                }
            }
        }
    }
}
