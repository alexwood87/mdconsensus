﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using WSD.Consensus.Domain.Repositories;
using WSD.Consensus.Domain.Entities;
using WSD.Consensus.Domain.Models;
using WSD.Consensus.Domain.Helpers;

namespace WSD.Consensus.Infrastructure.Repositories
{
    public class MedicalCaseRepository : GenericRepository<MedicalCaseEntity>, IMedicalCaseRepository
    {
        public MedicalCaseRepository(ConsensusContext context) : base(context)
        {

        }
        public Guid? CreateMedicalCase(MedicalCaseModel medicalCase)
        {
            using (var trans = new CommittableTransaction(new TimeSpan(0, 1, 0)))
            {
                try
                {
                    var medCase = EntityMapper.MapMedicalCaseToEntity(medicalCase);
                    medCase.User = _context.Users.Single(x => x.Id == medicalCase.Creator.Id);
                    var mc = medCase.MedicalCasesToBodyParts;
                    medCase.MedicalCasesToBodyParts = null;
                    var symc = medCase.MedicalCasesToSymptoms;
                    medCase.MedicalCasesToSymptoms = null;
                    var medTests = medCase.MedicalCasesToMedicalTests;
                    medCase.MedicalCasesToMedicalTests = null;
                    medCase.DiseaseId = medicalCase.Disease.Id;
                    if (medCase.Patient.Id == Guid.Empty)
                    {
                        medCase.Patient.Id = Guid.NewGuid();
                        medCase.Patient = _context.Patients.Add(medCase.Patient).Entity;
                        _context.SaveChanges();
                    }
                    if (medCase.Id == Guid.Empty)
                    {
                        medCase.Id = Guid.NewGuid();
                    }

                    medCase = Add(medCase);
                    _context.SaveChanges();
                    foreach (var m in mc)
                    {
                        m.MedicalCaseId = medCase.Id;
                        m.Id = Guid.NewGuid();
                        m.MedicalCase = medCase;
                        m.BodyPart = _context.BodyParts.Single(x => x.Id == m.BodyPartId);
                    }
                    foreach (var m in symc)
                    {
                        m.MedicalCaseId = medCase.Id;
                        m.Id = Guid.NewGuid();
                        m.MedicalCase = medCase;
                        m.Symptom = _context.Symptoms.Single(x => x.Id == m.SymptomId);                       
                    }
                    foreach (var m in medTests)
                    {
                        m.MedicalCaseId = medCase.Id;
                        m.Id = Guid.NewGuid();
                        m.MedicalTest = _context.MedicalTests.Single(x => x.Id == m.MedicalTestId);
               
                    }
                    _context.MedicalCasesToBodyParts.AddRange(mc);
                    _context.MedicalCasesToMedicalTests.AddRange(medTests);
                    _context.MedicalCasesToSymptoms.AddRange(symc);
                    _context.SaveChanges();
                    trans.Commit();
                    return medCase.Id;
                }
                catch(Exception ex)
                {
                    trans.Rollback(ex);
                    return null;
                }
            }
        }

        public List<DiseaseDiagnosticResult> DiagnoseDisease(List<SymptomModel> symptoms, List<BodyPartModel> bodyParts, List<DemographicInformationModel> demographicInformation, double userWeight = 1, double caseWeight = 2, double demographicWeight = 1)
        {
            var possibleDiseases = new List<DiseaseDiagnosticResult>();
            var query = _context.MedicalCases.AsQueryable();
          
            foreach (var medCase in query)
            {
                var userRanking = medCase.User.UserLevel * (medCase.User.UserRankingTo
                    .Sum(x => x.UserRankingValue)/medCase.User.UserRankingsFrom.Count);
                var syms = medCase.MedicalCasesToSymptoms.AsQueryable();
                var bps = medCase.MedicalCasesToBodyParts.AsQueryable();
                var demos = medCase.Patient.DemographicInformations.AsQueryable();
                var symTotal = 0;
                var bpTotal = 0;
                decimal demoCount = 0;
                decimal demoWeight = 0;
                foreach (var sym in syms)
                {
                    var sym1 = sym;
                    if(symptoms.Any(x => x.Id == sym1.SymptomId))
                    {
                        symTotal++;
                    }
                }
                foreach(var bp in bps)
                {
                    var bp1 = bp;
                    if(bodyParts.Any(x => x.Id == bp1.BodyPartId))
                    {
                        bpTotal++;
                    }
                }
                foreach(var demo in demos)
                {
                    var demo1 = demo;
                    if(demographicInformation.Any(x => x.Id == demo1.Id))
                    {
                        demoWeight += demo1.Weight;
                        demoCount++;
                    }
                }
                decimal caseMainRankB = medCase.CaseRankings.Sum(x => x.Rating) / medCase.CaseRankings.Count();
                caseMainRankB *= bpTotal + symTotal;
                var total = Math.Pow((double)userRanking, userWeight) + Math.Pow((double)caseMainRankB, caseWeight) + Math.Pow((double)demoWeight, demographicWeight);
                if(possibleDiseases.Count > 5)
                {
                    int place = -1;
                    double preTotal = 0;
                    for(var pos =0; pos < possibleDiseases.Count; pos++)
                    {
                        
                        if(possibleDiseases[pos].Likelihood < total)
                        {
                            if(preTotal < total)
                            {
                                preTotal = possibleDiseases[pos].Likelihood;
                                place = pos;
                            }
                        }
                    }
                    if(place != -1)
                    {
                        possibleDiseases[place] = new DiseaseDiagnosticResult
                        {
                            Likelihood = total,
                            Disease = new DiseaseModel
                            {
                                Id = medCase.DiseaseId,
                                CommonName = medCase.Disease.CommonName,
                                ScientificName = medCase.Disease.ScientificName,
                                CultureName = medCase.CultureName
                            }
                        };
                    }
                }
                else
                {
                    possibleDiseases.Add(new DiseaseDiagnosticResult
                    {
                        Likelihood = total,
                        Disease = new DiseaseModel
                        {
                            Id = medCase.DiseaseId,
                            CommonName = medCase.Disease.CommonName,
                            ScientificName = medCase.Disease.ScientificName,
                            CultureName = medCase.CultureName
                        }
                    });
                }
            }
            var totalWeight = possibleDiseases.Sum(x => x.Likelihood);
            foreach (var possibleD in possibleDiseases)
            {
                possibleD.Likelihood = possibleD.Likelihood / totalWeight;
            }
            return possibleDiseases;
        }

        public bool EditMedicalCase(MedicalCaseModel medicalCase)
        {
            using (var trans = new CommittableTransaction(new TimeSpan(0, 1, 0)))
            {
                try
                {
                    var medCase = Find(medicalCase.Id);
                    medCase = EntityMapper.MapMedicalCaseToEntity(medicalCase);
                    _context.MedicalCases.Attach(medCase);
                    _context.SaveChanges();
                    trans.Commit();
                    return true;
                }
                catch (Exception ex)
                {
                    trans.Rollback(ex);
                    return false;
                }
            }
        }

        public MedicalCaseModel FindMedicalCase(Guid id)
        {
            return EntityMapper.MapMedicalCase(Find(id));
        }

        public List<MedicalCaseModel> Search(MedicalCaseSearch query, int numRecords, int page)
        {
            if(page == 0)
            {
                ++page;
            }
            var medCases = new List<MedicalCaseModel>();
            if(query.MedicalCaseId != null)
            {
                medCases.Add(FindMedicalCase(query.MedicalCaseId.Value));
            }
            if (query.CreatorId != null)
            {
                medCases.AddRange(_context.MedicalCases.Where(x => x.CreatorId == query.CreatorId)
                    .Select(Domain.Helpers.EntityMapper.MapMedicalCase).Skip((page-1)*numRecords).Take(numRecords));
            }

            if(!string.IsNullOrEmpty(query.Name))
            {
                query.Name = query.Name.ToLower().Trim();
                medCases.AddRange(_context.MedicalCases.Where(x => x.CaseName.ToLower().Contains(query.Name))
                    .Select(Domain.Helpers.EntityMapper.MapMedicalCase).Skip((page-1)*numRecords).Take(numRecords).ToList());
            }
            if(!string.IsNullOrEmpty(query.Description))
            {
                query.Description = query.Description.ToLower().Trim();
                medCases.AddRange(_context.MedicalCases.Where(x => x.Description.ToLower().Contains(query.Description))
                    .Select(Domain.Helpers.EntityMapper.MapMedicalCase).Skip((page - 1)*numRecords).Take(numRecords).ToList());                
            }
           
            var dicMedCaseIdToCount = new Dictionary<Guid, int>();
            var countTotal = 0;
            if (query.BodyParts != null)
            {
                countTotal += query.BodyParts.Count;
                foreach (var bp in query.BodyParts)
                {
                    foreach (var bp1 in _context.BodyParts.Where(x => x.Id == bp.Id))
                    {
                        var medCases2 = bp1.MedicalCasesToBodyParts.Select(x => EntityMapper.MapMedicalCase(x.MedicalCase));
                        foreach (var medC in medCases2)
                        {
                            if (!dicMedCaseIdToCount.ContainsKey(medC.Id))
                            {
                                dicMedCaseIdToCount.Add(medC.Id, 1);
                            }
                            else
                            {
                                dicMedCaseIdToCount[medC.Id]++;
                            }
                        }
                    }

                }
            }
            if (query.Symptoms != null)
            {
                countTotal += query.Symptoms.Count;
                foreach (var sym in query.Symptoms)
                {
                    foreach (var sym1 in _context.Symptoms.Where(x => x.Id == sym.Id))
                    {
                        var medCases2 = sym1.MedicalCasesToSymptoms.Select(x => 
                            EntityMapper.MapMedicalCase(x.MedicalCase));
                        foreach (var medC in medCases2)
                        {
                            if (!dicMedCaseIdToCount.ContainsKey(medC.Id))
                            {
                                dicMedCaseIdToCount.Add(medC.Id, 1);
                            }
                            else
                            {
                                dicMedCaseIdToCount[medC.Id]++;
                            }
                        }
                    }
                }
            }
            if (query.DemoGraphicInfo != null)
            {
                countTotal += query.DemoGraphicInfo.Count;
                foreach (var demo in query.DemoGraphicInfo)
                {
                    foreach (var bp1 in _context.DemographicInformations.Where(x => x.Id 
                        == demo.Id))
                    {
                        var medCases2 = bp1.Patient.MedicalCases.Select(x => Domain.Helpers.EntityMapper.MapMedicalCase(x));
                        foreach (var medC in medCases2)
                        {
                            if (!dicMedCaseIdToCount.ContainsKey(medC.Id))
                            {
                                dicMedCaseIdToCount.Add(medC.Id, 1);
                            }
                            else
                            {
                                dicMedCaseIdToCount[medC.Id]++;
                            }
                        }
                    }
                }
            }

            
            if (!string.IsNullOrEmpty(query.DiseaseName))
            {
                query.DiseaseName = query.DiseaseName.ToLower().Trim();
                var medCs = _context.MedicalCases.Where(x => x.Disease.ScientificName.ToLower().Contains(query.DiseaseName)).OrderByDescending(c => c.CaseName).Skip((page-1)*numRecords).Take(numRecords).ToList();
                return medCs.Select(x => Domain.Helpers.EntityMapper.MapMedicalCase(x)).ToList();

            }
      
            if (dicMedCaseIdToCount.Count > 0 && medCases.Count == 0)
            {
                var guids = dicMedCaseIdToCount.Where(x => x.Value == countTotal).OrderByDescending(c => c.Key).Skip(numRecords*(page-1)).Take(numRecords).ToList();
                foreach (var g in guids)
                {
                    medCases.Add(FindMedicalCase(g.Key));
                }
            }
            if (query.UserIdWhosePlayingToIgnore != null)
            {
                var user = _context.Users.Single(x => x.Id == query.UserIdWhosePlayingToIgnore.Value);
                medCases = medCases.Where(c => user.UserGames.All(x => x.MedicalCaseId != c.Id || !x.IsFinished)).ToList();               
            }
            return medCases;
        }
    }
}
