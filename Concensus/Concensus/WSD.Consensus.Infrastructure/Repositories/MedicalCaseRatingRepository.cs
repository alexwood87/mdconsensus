﻿using WSD.Consensus.Domain.Entities;
using System.Transactions;
using WSD.Consensus.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using WSD.Consensus.Domain.Models;
using WSD.Consensus.Domain.Helpers;

namespace WSD.Consensus.Infrastructure.Repositories
{
    public class MedicalCaseRatingRepository : GenericRepository<CaseRankingEntity>, IMedicalCaseRatingRepository
    {
        public MedicalCaseRatingRepository(ConsensusContext context):base(context)
        {

        }

        public CaseRatingModel CreateMedicalCaseRating(long creatorId, Guid userGameId, Guid medicalCaseId, decimal medicalCaseRatingValue)
        {
            var g = _context.UserGames.SingleOrDefault(x => x.Id == userGameId);
            if(g != null && g.MedicalCase.CreatorId == creatorId)
            {
                return
                    null;
            }
            using var trans = new CommittableTransaction(new TimeSpan(0, 1, 0));
            try
            {
                var user = _context.Users.Single(x => x.Id == creatorId);
                var weight = user.UserLevel * medicalCaseRatingValue;
                var ca = new CaseRankingEntity
                {
                    CreatorId = creatorId,
                    MedicalCaseId = medicalCaseId,
                    Rating = medicalCaseRatingValue,
                    UserGameId = userGameId,
                    WeightModifier = weight,
                    Id = Guid.NewGuid()
                };
                _context.CaseRankings.Add(ca);
                _context.SaveChanges();
                trans.Commit();
                return EntityMapper.MapCaseRatingToModel(ca);
            }
            catch (Exception ex)
            {
                trans.Rollback(ex);
                throw;
            }
        }

        public CaseRatingModel EditMedicalCaseRating(Guid ratingId, long creatorId, Guid medicalCaseId, decimal medicalCaseRatingValue)
        {
            using var trans = new CommittableTransaction(new TimeSpan(0, 1, 0));
            try
            {
                var user = _context.Users.Find(ratingId);
                var caseRating = Find(ratingId);
                caseRating.MedicalCaseId = medicalCaseId;
                caseRating.CreatorId = creatorId;
                caseRating.Rating = medicalCaseRatingValue;
                caseRating.WeightModifier = user.UserLevel * medicalCaseRatingValue;
                _context.SaveChanges();
                trans.Commit();
                return EntityMapper.Map<CaseRankingEntity, CaseRatingModel>(caseRating);
            }
            catch (Exception ex)
            {
                trans.Rollback(ex);
                return null;
            }
        }

        public List<CaseRatingModel> FindByCreatorId(long creatorId)
        {
            var user = _context.Users.SingleOrDefault(x => x.Id == creatorId);
            if (user == null)
                return null;
            return user.CaseRankings.Select(Domain.Helpers.EntityMapper.Map<CaseRankingEntity, CaseRatingModel>).ToList();
        }

        public List<CaseRatingModel> FindByMedicalCaseId(Guid medicalCaseId)
        {
            var medCase = _context.MedicalCases.SingleOrDefault(x => x.Id == medicalCaseId);
            if (medCase == null)
                return null;
            return
                medCase.CaseRankings.Select(EntityMapper.Map<CaseRankingEntity, CaseRatingModel>).ToList();

        }

        public CaseRatingModel FindByRatingId(Guid ratingId)
        {
            var cas = _context.CaseRankings.SingleOrDefault(x => x.Id == ratingId);
            if (cas == null)
                return null;
            return EntityMapper.Map<CaseRankingEntity, CaseRatingModel>(cas);
        }

        public List<CaseRatingModel> FindCaseRatingByMedicalCaseId(Guid medicalCaseId)
        {
            return
                _context.CaseRankings.Where(x => x.MedicalCaseId
                == medicalCaseId).Select(
                    EntityMapper.Map<CaseRankingEntity, CaseRatingModel>).ToList();
        }
       
    }
}
