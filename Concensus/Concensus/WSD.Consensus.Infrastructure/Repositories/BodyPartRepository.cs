﻿using WSD.Consensus.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using WSD.Consensus.Domain.Models;
using WSD.Consensus.Domain.Entities;
using System.Transactions;
using WSD.Consensus.Domain.Helpers;

namespace WSD.Consensus.Infrastructure.Repositories
{
    public class BodyPartRepository : GenericRepository<BodyPartEntity>, IBodyPartRepository
    {
        public BodyPartRepository(ConsensusContext context):base(context)
        {

        }

        public List<BodyPartModel> FindByScientificNameContains(string sciName, string cultureName)
        {
            if (string.IsNullOrEmpty(sciName))
            {
                return
                    _context.BodyParts.Where(
                        x => x.CultureName == cultureName && x.ScientificName != "" && x.ScientificName != null)
                    .Select(EntityMapper.Map<BodyPartEntity, BodyPartModel>).ToList();
            }
            return
                _context.BodyParts.Where(
                    x => x.CultureName == cultureName && x.ScientificName.ToLower().Contains(sciName.ToLower()))
                    .Select(EntityMapper.Map<BodyPartEntity, BodyPartModel>).ToList();
        }
        public BodyPartModel CreateBodyPart(string scientificName, string commonName, string cultureName)
        {
            using (var trans = new CommittableTransaction(new TimeSpan(0, 1, 0)))
            {
                try
                {
                    var bp = FindByScientificName(scientificName, cultureName);
                    if (bp != null)
                        return bp;
                    var b =_context.BodyParts.Add(new BodyPartEntity
                    {
                        CultureName = cultureName,
                        CommonName = commonName,
                        ScientificName = scientificName
                    });
                    _context.SaveChanges();
                    trans.Commit();
                    return EntityMapper.Map<BodyPartEntity, BodyPartModel>(b.Entity);
                }
                catch(Exception ex)
                {
                    trans.Rollback(ex);
                    return null;
                }
            }
        }

        public BodyPartModel EditBodyPart(long id, string scientificName, string commonName, string cultureName)
        {
            using (var trans = new CommittableTransaction(new TimeSpan(0, 1, 0)))
            {
                try
                {
                    var bodyPart = Find(id);
                    bodyPart.CommonName = commonName;
                    bodyPart.CultureName = cultureName;
                    bodyPart.ScientificName = scientificName;
                    _context.SaveChanges();
                    trans.Commit();
                    return EntityMapper.Map<BodyPartEntity, BodyPartModel>(bodyPart);
                }
                catch (Exception ex)
                {
                    trans.Rollback(ex);
                    return null;
                }
            }
        }

        public BodyPartModel FindById(long id)
        {
            return
                Domain.Helpers.EntityMapper.Map<BodyPartEntity, BodyPartModel>(Find(id));
        }

        public BodyPartModel FindByScientificName(string sciName, string cultureName)
        {
            var bp =
                _context.BodyParts.FirstOrDefault(x => x.ScientificName == sciName && x.CultureName == cultureName);
            if (bp == null)
                return null;
            return
                EntityMapper.Map<BodyPartEntity, BodyPartModel>(bp);
        }

        public List<BodyPartModel> LoadAll()
        {
            return
                _context.BodyParts.Select(EntityMapper.Map<BodyPartEntity, BodyPartModel>).ToList();
        }
    }
    
}
