﻿using WSD.Consensus.Domain.Entities;
using System.Transactions;
using WSD.Consensus.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using WSD.Consensus.Domain.Models;
using WSD.Consensus.Domain.Helpers;

namespace WSD.Consensus.Infrastructure.Repositories
{
    public class UserRatingRepository : GenericRepository<UserRankingEntity>, IUserRatingRepository
    {
        public UserRatingRepository(ConsensusContext context):base(context)
        {

        }

        public UserRatingModel CreateUserRating(long creatorId, long userToRankId, decimal userRatingValue)
        {
            if(creatorId == userToRankId)
            {
                return null;
            }
            //using (var trans = new CommittableTransaction(new TimeSpan(0, 1, 0)))
            //{
                try
                {
                    var ur = _context.UserRankings.Add(new UserRankingEntity
                    {
                        CreatorId = creatorId,
                        UserRankingValue = userRatingValue,
                        UserRankedId = userToRankId,
                        Id = Guid.NewGuid()
                    }).Entity;
                    _context.SaveChanges();
                //    trans.Commit();
                    return EntityMapper.MapUserRatingToModel(ur);
                }
                catch
                {
                    //trans.Rollback(ex);
                    throw;                    
                }
            //}
        }

        public UserRatingModel EditUserRating(long ratingId, long creatorId, long userToRankId, decimal userRatingValue)
        {
            using var trans = new CommittableTransaction(new TimeSpan(0, 1, 0));
            try
            {
                var userRating = Find(ratingId);
                userRating.UserRankedId = userToRankId;
                userRating.CreatorId = creatorId;
                userRating.UserRankingValue = userRatingValue;
                _context.SaveChanges();
                trans.Commit();
                return EntityMapper.Map<UserRankingEntity, UserRatingModel>(userRating);
            }
            catch (Exception ex)
            {
                trans.Rollback(ex);
                return null;
            }
        }

        public List<UserRatingModel> FindByCreatorId(long creatorId)
        {
            var ratings = _context.UserRankings.Where(x => x.CreatorId == creatorId).ToList();
            return
                ratings.Select(Domain.Helpers.EntityMapper.Map<UserRankingEntity, UserRatingModel>).ToList();
        }

        public List<UserRatingModel> FindByUserRankedId(long userRankedId)
        {
            return
                _context.UserRankings.Where(x => x.UserRankedId == userRankedId).ToList()
                    .Select(EntityMapper.Map<UserRankingEntity, UserRatingModel>).ToList();
        }

        public UserRatingModel FindByUserRatingId(Guid ratingId)
        {
            var rating =
                _context.UserRankings.SingleOrDefault(x => x.Id == ratingId);
            if (rating == null)
                return null;
            return Domain.Helpers.EntityMapper.Map<UserRankingEntity, UserRatingModel>(rating);
        }

        public List<UserRatingModel> FindUserRatingByCreatorId(long creatorId)
        {
            return
                _context.UserRankings.Where(x => x.CreatorId == creatorId)
                    .Select(EntityMapper.Map<UserRankingEntity, UserRatingModel>).ToList();
        }
        public List<UserRatingModel> FindUserRatingByUserRatedId(long userRatedId)
        {
            return
            _context.UserRankings.Where(x => x.UserRankedId == userRatedId)
                .Select(EntityMapper.Map<UserRankingEntity, UserRatingModel>).ToList();

        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
