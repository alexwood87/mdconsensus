﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using WSD.Consensus.Domain.Models;
using WSD.Consensus.Domain.Repositories;
using WSD.Consensus.Domain.Entities;
using System.Transactions;
namespace WSD.Consensus.Infrastructure.Repositories
{
    public class MediaRepository : GenericRepository<MediumEntity>, IMediaRepository
    {
        public MediaRepository(ConsensusContext context) :base(context)
        {
            
        }
        public List<MediaModel> SearchMedia(MediaSearchQuery query)
        {
            var query2 = _context.Media.AsQueryable();
            if (query.MediaId != null)
            {
                return 
                    new List<MediaModel>
                    {
                        Domain.Helpers.EntityMapper.Map<MediumEntity, MediaModel>(_context.Media.Single(x => x.Id 
                            == query.MediaId))
                    };
            }
            if (!string.IsNullOrEmpty(query.FileName))
            {
                query.FileName = query.FileName.ToLower().Trim();
                query2 =
                    _context.Media.Where(x => x.FileName.ToLower(CultureInfo.InvariantCulture).Contains(query.FileName)).AsQueryable();
            }
            if (query.FileType == null)
            {
                var ft = (int) query.FileType.GetValueOrDefault(0);
                return
                    query2.Where(x => x.FileType == ft).Select(Domain.Helpers.EntityMapper.Map<MediumEntity, 
                        MediaModel>).ToList();
            }
            //if (query.BodyPartId != null && query.MediaId != null)
            //{
            //    return
            //       _context.MediaToUsers.Where(x => x.UserId == query.UserId &&  x.BodyPartId == query.BodyPartId)
            //           .ToList()
            //           .Select(x => Domain.Helpers.EntityMapper.Map<Medium, MediaModel>(x.Medium))
            //           .ToList();    
            //}
            if (query.BodyPartId != null)
            {
                return
                    _context.MediaToBodyParts.Where(x => x.BodyPartId == query.BodyPartId)
                       .ToList().Select(x => Domain.Helpers.EntityMapper.Map<MediumEntity, MediaModel>(x.Medium))
                        .ToList();
            }
            if (query.UserId != null)
            {
                return
                    _context.MediaToUsers.Where(x => x.UserId == query.UserId)
                        .ToList()
                        .Select(x => Domain.Helpers.EntityMapper.Map<MediumEntity, MediaModel>(x.Medium))
                        .ToList();
            }
            return
                null;
        }

        public bool CreateMediaForUser(long userId, string fileName, int ft, string filePath)
        {
            using (var trans = new CommittableTransaction(new TimeSpan(0, 1, 0)))
            {
                try
                {
                    if (_context.MediaToUsers.Any(x => x.UserId == userId && x.Medium.FileName == fileName))
                        return false;
                    MediumEntity m = Add(new MediumEntity
                    {
                        FileName = fileName,
                        FilePath = filePath,
                        FileType = ft
                    });

                    _context.MediaToUsers.Add(new MediaToUserEntity
                    {
                        MediaId = m.Id,                   
                        UserId = userId
                    });
                    _context.SaveChanges();
                    trans.Commit();
                    return true;

                }
                catch (Exception ex)
                {
                    trans.Rollback(ex);
                    return false;
                }
            }
        }

        public bool CreateMediaForBodyPart(long bodyPartId, string fileName, int ft, string filePath)
        {

            using (var trans = new CommittableTransaction(new TimeSpan(0, 1, 0)))
            {
                try
                {
                    if (_context.MediaToBodyParts.Any(x => x.BodyPartId == bodyPartId && x.Medium.FileName == fileName))
                        return false;
                    MediumEntity m = Add(new MediumEntity
                    {
                        FileName = fileName,
                        FilePath = filePath,
                        FileType = ft
                    });

                    _context.MediaToBodyParts.Add(new MediaToBodyPartEntity
                    {
                        MediaId = m.Id,
                        BodyPartId = bodyPartId                        
                    });
                    _context.SaveChanges();
                    trans.Commit();
                    return true;

                }
                catch (Exception ex)
                {
                    trans.Rollback(ex);
                    return false;
                }
            }    
        }

        public bool EditMediaForUser(long id, long userId, string fileName, int ft, string filePath)
        {

            using (var trans = new CommittableTransaction(new TimeSpan(0, 1, 0)))
            {
                try
                {
                    if (!_context.MediaToUsers.Any(x => x.Id == id))
                        return false;
                    MediumEntity m = _context.MediaToUsers.Single(x => x.Id == id).Medium;

                    m.FileName = fileName;
                    m.FilePath = filePath;
                    m.FileType = ft;
                    
                    _context.SaveChanges();
                    trans.Commit();
                    return true;

                }
                catch (Exception ex)
                {
                    trans.Rollback(ex);
                    return false;
                }
            }
        }

        public bool EditMediaForBodyPart(Guid id, long bodyPartId, string fileName, int ft, string filePath)
        {   
            using (var trans = new CommittableTransaction(new TimeSpan(0, 1, 0)))
            {
                try
                {
                    if (!_context.MediaToBodyParts.Any(x => x.Id == id))
                        return false;
                    MediumEntity m = _context.MediaToBodyParts.Single(x => x.Id == id).Medium;

                    m.FileName = fileName;
                    m.FilePath = filePath;
                    m.FileType = ft;
                    
                    _context.SaveChanges();
                    trans.Commit();
                    return true;

                }
                catch (Exception ex)
                {
                    trans.Rollback(ex);
                    return false;
                }
            }
        }
    }
}
