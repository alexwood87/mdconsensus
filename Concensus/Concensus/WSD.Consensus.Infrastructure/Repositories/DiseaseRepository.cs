﻿using WSD.Consensus.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WSD.Consensus.Domain.Models;
using WSD.Consensus.Domain.Entities;
using System.Transactions;
using WSD.Consensus.Domain.Helpers;

namespace WSD.Consensus.Infrastructure.Repositories
{
    public class DiseaseRepository : GenericRepository<DiseaseEntity>, IDiseaseRepository
    {
        public DiseaseRepository(ConsensusContext context):base(context)
        {

        }

        public List<DiseaseModel> FindByScientificNameContains(string scientificName, string cultureName)
        {
            return
                _context.Diseases.Where(
                    x => x.CultureName == cultureName && x.ScientificName.ToLower().Contains(scientificName.ToLower()))
                    .Select(EntityMapper.Map<DiseaseEntity,DiseaseModel>)
                    .ToList();
        }

        public DiseaseModel CreateDisease(string scientificName, string commonName, string cultureName)
        {
            using (var trans = new CommittableTransaction(new TimeSpan(0, 1, 0)))
            {
                try
                {
                    var res = FindByScientificName(scientificName, cultureName);
                    if (res != null)
                        return res;
                    var d = _context.Diseases.Add(new DiseaseEntity
                    {
                        CultureName = cultureName,
                        CommonName = commonName,
                        ScientificName = scientificName
                    });
                    _context.SaveChanges();
                    trans.Commit();
                    return EntityMapper.Map<DiseaseEntity, DiseaseModel>(d.Entity);
                }
                catch(Exception ex)
                {
                    trans.Rollback(ex);
                    return null;
                }
            }
        }

        public DiseaseModel EditDisease(long id, string scientificName, string commonName, string cultureName)
        {
            using (var trans = new CommittableTransaction(new TimeSpan(0, 1, 0)))
            {
                try
                {
                    var disease = Find(id);
                    disease.CommonName = commonName;
                    disease.CultureName = cultureName;
                    disease.ScientificName = scientificName;
                    _context.SaveChanges();
                    trans.Commit();
                    return Domain.Helpers.EntityMapper.Map<DiseaseEntity,DiseaseModel>(disease);
                }
                catch (Exception ex)
                {
                    trans.Rollback(ex);
                    return null;
                }
            }
        }

        public DiseaseModel FindById(long id)
        {
            return Domain.Helpers.EntityMapper.Map<DiseaseEntity,DiseaseModel>(Find(id));
        }

        public DiseaseModel FindByScientificName(string scientificName, string cultureName)
        {
            var res = _context.Diseases.SingleOrDefault(x => x.CultureName == cultureName && 
                x.ScientificName == scientificName);
            if (res == null)
                return null;
            return
               EntityMapper.Map<DiseaseEntity,DiseaseModel>(res);
        }

        public List<DiseaseModel> LoadAll()
        {
            return
                _context.Diseases.Select(EntityMapper.Map<DiseaseEntity, DiseaseModel>).ToList();
                
        }
    }
    
}
