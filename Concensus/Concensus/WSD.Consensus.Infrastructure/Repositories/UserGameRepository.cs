﻿using System;
using System.Collections.Generic;
using System.Linq;
using WSD.Consensus.Domain.Models;
using WSD.Consensus.Domain.Repositories;
using WSD.Consensus.Domain.Entities;
using System.Transactions;
using WSD.Consensus.Domain.Helpers;

namespace WSD.Consensus.Infrastructure.Repositories
{
    public class UserGameRepository : GenericRepository<UserGameEntity>, IUserGameRepository
    {
        public UserGameRepository(ConsensusContext context) : base(context)
        {

        }
        public Guid? CreateGame(UserGameModel model)
        {
            UserGameEntity game = Domain.Helpers.EntityMapper.MapGameModelToEntity(model);
            game.User = _context.Users.Single(x => x.Id == model.Player.Id);
            game.MedicalCase = _context.MedicalCases.Single(x => x.Id == game.MedicalCaseId);
            game.MedicalCase.User = _context.Users.Single(c => c.Id == model.MedicalCase.Creator.Id);
            game.MedicalCase.Patient = _context.Patients.Single(x => x.Id == model.MedicalCase.Patient.Id);
            using (var trans = new CommittableTransaction(new TimeSpan(0, 1, 0)))
            {
                try
                {
                    game.Id = Guid.NewGuid();
                    Add(game);
                    _context.SaveChanges();
                    trans.Commit();
                    return game.Id;
                }
                catch(Exception ex)
                {
                    trans.Rollback(ex);
                    return null;
                }
            }
        }

        public List<UserGameModel> SearchUserGames(UserGameSearch query, int page, int numGames)
        {
            var games = new List<UserGameModel>();
            if (query.UserGameId != null)
            {
                return new List<UserGameModel>
                {
                    FindGame(query.UserGameId.Value)
                };
            }
            if(!string.IsNullOrEmpty(query.UseGameName))
            {
                query.UseGameName = query.UseGameName.ToLower().Trim();
                games.AddRange(_context.UserGames.Where(x => x.GameName.ToLower().Contains(query.UseGameName)).OrderBy(x => x.GameName).Skip((page - 1) * numGames).Take(numGames).ToList().Select(Domain.Helpers.EntityMapper.MapGameEntityToModel));
            }
            if(query.UserGameUserId != null)
            {
                List<UserGameModel> games2;
                if(games.Count == 0)
                {
                    games2 = _context.UserGames.Where(x => x.PlayerId == query.UserGameUserId).OrderBy(x=>x.GameName).Skip((page - 1) * numGames).Take(numGames).ToList().Select(Domain.Helpers.EntityMapper.MapGameEntityToModel).ToList();
                    games.AddRange(games2);
                }
                else
                {
                    games = games.Where(x => x.Player.Id == query.UserGameUserId.Value).ToList();
                }
                
            }
            return games;
        }

        public UserGameModel FindGame(Guid id)
        {
            return EntityMapper.MapGameEntityToModel(Find(id));
        }

        public TestResultModel MakeMove(Guid gameId, long medicalTestId)
        {
            var game = Find(gameId);
            var testId = 0;
            var testRes = 0;
            using (var trans = new CommittableTransaction(new TimeSpan(0, 1, 0)))
            {
                try
                {
                    var testResult = new TestResultEntity
                    {
                        Id = testRes,
                        MedicalTestId = medicalTestId
                    };
                    var rnd = new Random();
                    testResult.Accuracy = (decimal) Math.Abs(rnd.NextDouble())% (decimal)101.0;
                    testResult = _context.TestResults.Add(testResult).Entity;
                    _context.SaveChanges();
                    var d = _context.DiseasesToMedicalTests.FirstOrDefault(x => x.MedicalTestId == medicalTestId);
                    if (d != null)
                    {
                        testResult.DiseasesToTestResults.Add(new DiseasesToTestResultEntity
                        {
                            Id = Guid.NewGuid(),
                            DiseaseId = d.DiseaseId,
                            RandomAccuracyModifier = (decimal)Math.Abs(rnd.NextDouble()) % (decimal)101.0,
                            TestResultId = testResult.Id
                        });
                    }
                        _context.SaveChanges();
                    
                    var testId2 = testId == 0 ? testResult.Id : testId;
                    if(game.UserGamesToMedicalTests.All(x => x.TestResultId != testResult.Id))
                    {
                        game.UserGamesToMedicalTests.Add(new UserGamesToMedicalTestEntity
                        {
                            MedicalTestId = medicalTestId,
                            UserGameId = gameId,
                            TestResultId = testId2,
                            Id = Guid.NewGuid()
                        });
                    }
                    _context.SaveChanges();
                    var medCount = game.MedicalCase.MedicalCasesToMedicalTests.Count;
                    var count = 0;
                    foreach(var medTests in game.MedicalCase.MedicalCasesToMedicalTests)
                    {
                        if (game.UserGamesToMedicalTests.Any(x => x.MedicalTestId == medTests.MedicalTestId))
                        {
                            count++;                                
                        }
                    }
                    if(count == game.MedicalCase.MedicalCasesToMedicalTests.Count)
                    {
                        _context.DiseasesToTestResults.Add(new DiseasesToTestResultEntity
                        {
                            DiseaseId = game.MedicalCase.DiseaseId,
                            Id = Guid.NewGuid(),
                            RandomAccuracyModifier = Math.Abs(rnd.Next()) % 101,
                            TestResultId = testResult.Id
                        });
                        testResult.DiseasesToTestResults.Add(new DiseasesToTestResultEntity
                        {
                            DiseaseId = game.MedicalCase.DiseaseId,
                            Id = Guid.NewGuid(),
                            RandomAccuracyModifier = Math.Abs(rnd.Next()) % 101,
                            TestResultId = testResult.Id,
                            Disease = game.MedicalCase.Disease
                        });
                        game.Score = count;
                        game.IsFinished = true;
                        _context.SaveChanges();
                    }
                    if (game.MedicalCase.DiseaseId == (testResult.DiseasesToTestResults.Any()? 
                            testResult.DiseasesToTestResults.First().DiseaseId: -1))
                    {
                        game.Score = game.MedicalCase.MedicalCasesToMedicalTests.Count * 
                            (game.MedicalCase.User.UserLevel == 0 ? (decimal).5 : (int)game.MedicalCase.User.UserLevel);
                        game.IsFinished = true;
                        _context.SaveChanges();
                    }
                        
                    trans.Commit();
                    return EntityMapper.MapTestResultEntityToModel(testResult);
                }
                catch(Exception ex)
                {
                    trans.Rollback(ex);
                    return null;
                }
            }
        }

        public bool UpdateGame(UserGameModel model)
        {
            UserGameEntity game = EntityMapper.MapGameModelToEntity(model);
            using (var trans = new CommittableTransaction(new TimeSpan(0, 1, 0)))
            {
                try {
                    var g = Find(model.Id);
                    g.Score = game.Score;
                    g.GameLevel = game.GameLevel;
                    g.GameName = game.GameName;
                    g.IsFinished = game.IsFinished;
                    
                    _context.SaveChanges();
                    trans.Commit();
                    return true;
                }
                catch(Exception ex)
                {
                    trans.Rollback(ex);
                    return false;
                }
            }
        }
    }
}
