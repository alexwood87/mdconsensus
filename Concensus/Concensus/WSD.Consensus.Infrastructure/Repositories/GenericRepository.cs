﻿using System.Collections.Generic;
using System.Linq;
using WSD.Consensus.Domain.Entities;
using WSD.Consensus.Domain.Repositories;

namespace WSD.Consensus.Infrastructure.Repositories
{
    public class GenericRepository<TE> : IGenericRepository<TE> where TE : class
    {
        protected ConsensusContext _context;
        protected GenericRepository(ConsensusContext context)
        {
            _context = context;
        }
        public TE Add(TE entity)
        {
            return _context.Add(entity).Entity;
        }
        public TE Find(object id)
        {
            return _context.Set<TE>().Find(new object[] { id });
        }

        public TE Update(TE entity)
        {
            return  _context.Update(entity).Entity;
        }

        public TE Delete(TE entity)
        {
            return _context.Remove(entity).Entity;
            //return (TE)_context.Set<TE>().Remove(entity).Entity;
        }
    }
}
