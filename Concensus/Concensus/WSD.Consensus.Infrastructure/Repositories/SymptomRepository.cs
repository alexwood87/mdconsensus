﻿using WSD.Consensus.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using WSD.Consensus.Domain.Models;
using WSD.Consensus.Domain.Entities;
using System.Transactions;
using WSD.Consensus.Domain.Helpers;

namespace WSD.Consensus.Infrastructure.Repositories
{
    public class SymptomRepository : GenericRepository<SymptomEntity>, ISymptomRepository
    {
        public SymptomRepository(ConsensusContext context):base(context)
        {

        }
        public SymptomModel CreateSymptom(string scientificName, string commonName, string cultureName)
        {
            if(string.IsNullOrEmpty(scientificName) || string.IsNullOrEmpty(cultureName))
            {
                return null;
            }
            var sy = _context.Symptoms.SingleOrDefault(c => c.ScientificName.ToLower() 
                == scientificName.ToLower() && c.CultureName.ToLower() 
            == cultureName.ToLower());
            if(sy != null)
            {
                return null;
            }
            using (var trans = new CommittableTransaction(new TimeSpan(0, 1, 0)))
            {
                try
                {
                   var sym = _context.Symptoms.Add(new SymptomEntity
                    {
                        CultureName = cultureName,
                        CommonName = commonName,
                        ScientificName = scientificName
                    }).Entity;
                    _context.SaveChanges();
                    trans.Commit();
                    return EntityMapper.Map<SymptomEntity, SymptomModel>(sym);
                }
                catch(Exception ex)
                {
                    trans.Rollback(ex);
                    return null;
                }
            }
        }

        public SymptomModel EditSymptom(long id, string scientificName, string commonName, string cultureName)
        {
            using (var trans = new CommittableTransaction(new TimeSpan(0, 1, 0)))
            {
                try
                {
                    var symptom = Find(id);
                    symptom.CommonName = commonName;
                    symptom.CultureName = cultureName;
                    symptom.ScientificName = scientificName;
                    _context.SaveChanges();
                    trans.Commit();
                    return Domain.Helpers.EntityMapper.Map<SymptomEntity,SymptomModel>(symptom);
                }
                catch (Exception ex)
                {
                    trans.Rollback(ex);
                    return null;
                }
            }
        }

        public SymptomModel FindById(long id)
        {
            return
              Domain.Helpers.EntityMapper.Map<SymptomEntity, SymptomModel>(Find(id)); 
        }

        public List<SymptomModel> FindByScientificNameContains(string scientificName, string cultureName)
        {
            if (string.IsNullOrEmpty(scientificName))
            {
                return
                    _context.Symptoms.Where(s => s.CultureName == cultureName)
                        .Select(EntityMapper.Map<SymptomEntity, SymptomModel>).ToList();
            }
            return
                _context.Symptoms.Where(
                    x => x.CultureName == cultureName && x.ScientificName.ToLower().Contains(scientificName.ToLower()))
                    .Select(EntityMapper.Map<SymptomEntity, SymptomModel>).ToList();
        }
        public SymptomModel FindByScientificName(string scientificName, string cultureName)
        {
            var sym = _context.Symptoms.SingleOrDefault(x => x.CultureName == cultureName && x.ScientificName == scientificName);
            return Domain.Helpers.EntityMapper.Map<SymptomEntity, SymptomModel>(sym);
        }

        public List<SymptomModel> LoadAll()
        {
            return _context.Symptoms.Select(EntityMapper.Map<SymptomEntity, SymptomModel>).ToList();
        }
    }
    
}
