﻿using WSD.Consensus.Domain.Models;
using WSD.Consensus.Domain.Entities;
using System.Transactions;
using System;
using System.Collections.Generic;
using System.Linq;
using WSD.Consensus.Domain.Repositories;
using WSD.Consensus.Domain.Helpers;

namespace WSD.Consensus.Infrastructure.Repositories
{
    public class UserRepository : GenericRepository<UserEntity>, IUserRepository
    {
        public UserRepository(ConsensusContext context) : base(context)
        {

        }
        public UserModel CreateUser(string email, string password, string question, 
            string answer, int userLevel, string cultureName)
        {
            var user = new UserEntity
            {
                CultureName = cultureName,
                Email = email,
                Password = password,
                SecurityAnswer = answer,
                SecurityQuestion = question,
                UserLevel = userLevel
            };
            var u2 = _context.Users.SingleOrDefault(x => x.Email.ToLower() == email.ToLower());
            if(u2 != null)
            {
                return EntityMapper.Map<UserEntity, UserModel>(u2);
            }
            using (var trans = new CommittableTransaction(new TimeSpan(0, 1, 0)))
            {
                try {
                    Add(user);
                    _context.SaveChanges();
                    trans.Commit();
                    return EntityMapper.Map<UserEntity, UserModel>(user);
                }
                catch(Exception ex)
                {
                    trans.Rollback(ex);
                    return null;
                }
            }
        }
        public UserModel EditUser(long id, string email, string password, string question, string answer, int userLevel, string cultureName)
        {
            using (var trans = new CommittableTransaction(new TimeSpan(0, 1, 0)))
            {
                try {
                    var user = Find(id);
                    user.CultureName = cultureName;
                    user.Email = email;
                    user.Password = password;
                    user.SecurityAnswer = answer;
                    user.SecurityQuestion = question;
                    user.UserLevel = userLevel;
                    _context.SaveChanges();
                    trans.Commit();
                    return Domain.Helpers.EntityMapper.Map<UserEntity, UserModel>(user);
                }
                catch(Exception ex)
                {
                    trans.Rollback(ex);
                    return null;
                }
             }
         }
         public UserModel ValidateUser(string email, string password)
        {
            
                    email = email.ToLower();
                    var user = _context.Users.SingleOrDefault(x => x.Email.ToLower() == email && x.Password == password);
                    if (user != null)
                    {
                        return Domain.Helpers.EntityMapper.Map<UserEntity,UserModel>( user);
                    }
                    return null;
                
            }

        public bool ChangePassword(string email, string oldPassword, string newPassword)
        {
            var user = _context.Users.SingleOrDefault(x => x.Email.ToLower() == email && x.Password == oldPassword);
            if (user != null)
            {
                using (var trans = new CommittableTransaction(new TimeSpan(0, 1, 0)))
                {
                    try
                    {
                        user.Password = newPassword;
                        _context.SaveChanges();
                        trans.Commit();
                    }
                    catch (Exception ex)
                    {
                        trans.Rollback(ex);
                        return false;
                    }
                }
            }
            return false;
        }

        public List<UserModel> SearchUsers(long? id, string email)
        {
            if (id != null)
            {
                return new List<UserModel>
                {
                    EntityMapper.Map<UserEntity,UserModel>(Find(id.Value))    
                };
            }
            email = email.ToLower();
            var u = _context.Users.SingleOrDefault(x => x.Email.ToLower() == email);
            if (u != null)
            {
                return
                    new List<UserModel>
                    {
                        EntityMapper.Map<UserEntity, UserModel>(u)
                    };
            }
            return null;
        }

        public bool GenerateUserPassword(string answer, string email)
        {
            email = email.ToLower();
            answer = answer.ToLower();
            if (_context.Users.Any(x => x.Email.ToLower() == email && x.SecurityAnswer.ToLower() == answer))
            {
                var user = _context.Users.SingleOrDefault(x => x.Email.ToLower() == email);
                user.Password = Guid.NewGuid().ToString("N");
                //Send Email
                _context.SaveChanges();
                return true;
            }
            return false;
        }

        public bool DeActivate(long id)
        {
            using (var trans = new CommittableTransaction(new TimeSpan(0, 1, 0)))
            {
                try
                {
                    var user = Find(id);
                    user.UserLevel = -1;
                    _context.SaveChanges();
                    trans.Commit();
                    return true;
                }
                catch(Exception ex)
                {
                    trans.Rollback(ex);
                    return false;
                }
            }
        }

    }
}
