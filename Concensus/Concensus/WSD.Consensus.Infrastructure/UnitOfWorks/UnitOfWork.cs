﻿using WSD.Consensus.Domain.Repositories;
using WSD.Consensus.Domain.UnitOfWorks;
namespace WSD.Consensus.Infrastructure.UnitOfWorks
{
    public class UnitOfWork : IUnitOfWork
    {
   
        public UnitOfWork(IDiseaseRepository diseaseRepository, 
            IBodyPartRepository bodyPartRepository, IMediaRepository mediaRepository,
            IMedicalCaseRatingRepository medicalCaseRatingRepository, IMedicalCaseRepository medicalCaseRepository,
            IMedicalTestRepository medicalTestRepository, IUserGameRepository userGameRepository,
            IUserRatingRepository userRatingRepository, IUserRepository userRepository)
        {
            DiseaseRepository = diseaseRepository;
            BodyPartRepository = bodyPartRepository;
            MediaRepository = mediaRepository;
            MedicalCaseRatingRepository = medicalCaseRatingRepository;
            MedicalCaseRepository = medicalCaseRepository;
            MedicalTestRepository = medicalTestRepository;
            UserGameRepository = userGameRepository;
            UserRatingRepository = userRatingRepository;
            UserRepository = userRepository;           

        }

        public IMediaRepository MediaRepository { get; }

        public IMedicalTestRepository MedicalTestRepository { get; }
 

        public IMedicalCaseRatingRepository MedicalCaseRatingRepository { get; }
        public IMedicalCaseRepository MedicalCaseRepository { get; }

        public IUserGameRepository UserGameRepository { get; }

        public IUserRatingRepository UserRatingRepository { get; }
       
        public IUserRepository UserRepository { get; }

        public IBodyPartRepository BodyPartRepository { get; }

        public IDiseaseRepository DiseaseRepository { get; }

        public ISymptomRepository SymptomRepository { get; }

    }
}
