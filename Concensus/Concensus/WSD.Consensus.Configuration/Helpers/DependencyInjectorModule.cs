﻿using WSD.Consensus.BusinessLogic;
using WSD.Consensus.Core.Contracts;
using Autofac;
using WSD.Consensus.Domain.UnitOfWorks;
using WSD.Consensus.Infrastructure.UnitOfWorks;
using WSD.Consensus.Infrastructure.Repositories;
using WSD.Consensus.Domain.Repositories;

namespace WSD.Consensus.Configuration.Helpers
{
    public class DependencyInjectorModule : Module
    { 
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<UserBusinessLogic>().As<IUserBusinessLogic>().SingleInstance();
            builder.RegisterType<UserGameBusinessLogic>().As<IUserGameBusinessLogic>().SingleInstance();
            builder.RegisterType<UserRatingBusinessLogic>().As<IUserRatingBusinessLogic>().SingleInstance();
            builder.RegisterType<SymptomBusinessLogic>().As<ISymptomBusinessLogic>().SingleInstance();
            builder.RegisterType<MediaBusinessLogic>().As<IMediaBusinessLogic>().SingleInstance();
            builder.RegisterType<MedicalCaseBusinessLogic>().As<IMedicalCaseBusinessLogic>().SingleInstance();
            builder.RegisterType<MedicalCaseRatingBusinessLogic>().As<IMedicalCaseRatingBusinessLogic>().SingleInstance();
            builder.RegisterType<MedicalTestBusinessLogic>().As<IMedicalTestBusinessLogic>().SingleInstance();
            builder.RegisterType<DiseaseBusinessLogic>().As<IDiseaseBusinessLogic>().SingleInstance();
            builder.RegisterType<BodyPartBusinessLogic>().As<IBodyPartBusinessLogic>().SingleInstance();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>();
            builder.RegisterType<DiseaseRepository>().As<IDiseaseRepository>();
            builder.RegisterType<BodyPartRepository>().As<IBodyPartRepository>();
            builder.RegisterType<MediaRepository>().As<IMediaRepository>();
            builder.RegisterType<MedicalCaseRatingRepository>().As<IMedicalCaseRatingRepository>();
            builder.RegisterType<MedicalCaseRepository>().As<IMedicalCaseRepository>();
            builder.RegisterType<MedicalTestRepository>().As<IMedicalTestRepository>();
            builder.RegisterType<UserRepository>().As<IUserRepository>();
            builder.RegisterType<UserRatingRepository>().As<IUserRatingRepository>();
            builder.RegisterType<UserGameRepository>().As<IUserGameRepository>();
            builder.RegisterType<SymptomRepository>().As<ISymptomRepository>();            
            builder.RegisterType<ConsensusCache>().As<IConsensusCache>().SingleInstance();
        }
    }
}
