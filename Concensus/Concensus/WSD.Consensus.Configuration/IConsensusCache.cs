﻿using System.Collections.Generic;
using WSD.Consensus.Domain.Models;

namespace WSD.Consensus.Configuration
{
    public interface IConsensusCache
    {
        List<SymptomModel> SearchSymptoms(string scientificName,
            string cultureName, bool shouldDbLookUp = true);
        void DeleteSymptom(long key);
        void DeleteDisease(long key);
        void Delete(string cacheKey, long key);
        List<DiseaseModel> SearchDiseases(string scientificName, string cultureName, bool shouldDbLookUp = true);
        void Init();
        List<BodyPartModel> SearchBodyPartsByQuery(MetaDataSearchQuery query);
        List<DiseaseModel> SearchDiseasesByQuery(MetaDataSearchQuery query);
        List<SymptomModel> SearchSymptomsByQuery(MetaDataSearchQuery query);
        SymptomModel SearchSymptomsById(long? id);
    }
}
