﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Caching;
using WSD.Consensus.Domain.Models;
using WSD.Consensus.Configuration.Helpers;
using WSD.Consensus.Core.Contracts;

namespace WSD.Consensus.Configuration
{
    public class ConsensusCache : IConsensusCache
    {
        private static readonly ObjectCache _cache = new MemoryCache("ConsensusConstantsCache");
        private static volatile bool _isSetUp;
        private static readonly string SYMPTOM_KEY = "Symptoms", 
            BODYPART_KEY = "BodyParts", DISEASE_KEY = "Diseases";
        private readonly IBodyPartBusinessLogic _bodyPartBusinessLogic;
        private readonly IDiseaseBusinessLogic _diseaseBusinessLogic;
        private readonly ISymptomBusinessLogic _symptomBusinessLogic;
        public ConsensusCache(ISymptomBusinessLogic symptomBusinessLogic, 
            IDiseaseBusinessLogic diseaseBusinessLogic, IBodyPartBusinessLogic bodyPartBusinessLogic)
        {
            _symptomBusinessLogic = symptomBusinessLogic;
            _diseaseBusinessLogic = diseaseBusinessLogic;
            _bodyPartBusinessLogic = bodyPartBusinessLogic;
        }

        public List<SymptomModel> SearchSymptoms(string scientificName, 
            string cultureName, bool shouldDbLookUp = true)
        {
            var symptoms = (List<SymptomModel>) _cache[SYMPTOM_KEY];
            var list = symptoms.Where(x => x.CultureName == cultureName && 
            x.ScientificName.ToLower().Contains( scientificName.ToLower())).ToList();
            if(list.Count == 0 || shouldDbLookUp)
            {
                var symptomLookup = _symptomBusinessLogic;
                var sym = symptomLookup.FindByScientificNameContains(scientificName, cultureName);
                if(sym.Any())
                {
                    list.AddRange(sym);                   
                    _cache[SYMPTOM_KEY] = list.Distinct().ToList();
                }

            }
            return list;
        }
        public void DeleteSymptom(long key)
        {
            var list = (List<SymptomModel>) _cache[SYMPTOM_KEY];
            var sym = list.SingleOrDefault(x => x.Id == key);
            if(sym != null)
                list.Remove(sym);
            
        }
        public static void DeleteBodyPart(long key)
        {
            var list = (List<BodyPartModel>)_cache[SYMPTOM_KEY];
            var bp = list.SingleOrDefault(x => x.Id == key);
            if (bp != null)
                list.Remove(bp);

        }

        public void DeleteDisease(long key)
        {
            var list = (List<DiseaseModel>)_cache[DISEASE_KEY];
            var d = list.SingleOrDefault(x => x.Id == key);
            if (d != null)
                list.Remove(d);

        }

        public void Delete(string cacheKey, long key)
        {
            switch (cacheKey.ToLower(CultureInfo.InvariantCulture))
            {
                case "symptoms":
                    DeleteSymptom(key);
                    break;
                case "diseases":
                    DeleteDisease(key);
                    break;
                case "bodyparts":
                    DeleteBodyPart(key);
                    break;
            }
        }

        private List<BodyPartModel> SearchBodyParts(string scientificName, string cultureName, 
            bool shouldDbLookUp = true)
        {
            var bodyParts = (List<BodyPartModel>) _cache[BODYPART_KEY];
            var list = bodyParts.Where(x => x.CultureName == cultureName && 
                x.ScientificName.ToLower().Contains(scientificName.ToLower())).ToList();
            if (list.Count == 0 || shouldDbLookUp)
            {
                var lookup = _bodyPartBusinessLogic;
                var bp = lookup.FindByScientificNameContains(scientificName, cultureName);
                if (bp.Any())
                {
                    list.AddRange(bp);
                  
                    _cache[BODYPART_KEY] = list.Distinct().ToList();
                }

            }
            return list;
        }
        public List<DiseaseModel> SearchDiseases(string scientificName, string cultureName, bool shouldDbLookUp = true)
        {
            var diseases = (List<DiseaseModel>)_cache[DISEASE_KEY];
            var list = diseases.Where(x => x.CultureName == cultureName && 
                x.ScientificName.ToLower().Contains(scientificName.ToLower())).ToList();
            if (list.Count == 0 || shouldDbLookUp)
            {
                var lookup = _diseaseBusinessLogic;
                var disease = lookup.FindByScientificNameContains(scientificName, cultureName);
                if (disease.Any())
                {
                    list.AddRange(disease);
                  
                    _cache[DISEASE_KEY] = list.Distinct().ToList();
                }

            }
            return list;
        }
        
        public void Init()
        {
            var symptomLogic = _symptomBusinessLogic;
            var syms = symptomLogic.LoadAll(out MethodMessage msg);
            if (msg.CallWasSuccess)
                _ = _cache.Add(new CacheItem(SYMPTOM_KEY, syms.ToList()), new CacheItemPolicy { });
            var diseaseLogic = _diseaseBusinessLogic;
            var diseases = diseaseLogic.LoadAll(out msg);
            if (msg.CallWasSuccess)
                _ = _cache.Add(new CacheItem(DISEASE_KEY, diseases.ToList()), new CacheItemPolicy());
            var bodyPartsLogic = _bodyPartBusinessLogic;
            var parts = bodyPartsLogic.LoadAll(out msg);
            if(msg.CallWasSuccess)
            {
                _ = _cache.Add(new CacheItem(BODYPART_KEY, parts.ToList()), new CacheItemPolicy());
            }
        }

        

        public List<BodyPartModel> SearchBodyPartsByQuery(MetaDataSearchQuery query)
        {
            var bodyPartList = (List<BodyPartModel>)_cache["BodyParts"];
                   
            if (query.Id != null)
                return new List<BodyPartModel> {bodyPartList.FirstOrDefault(x => x.Id == query.Id.Value) };
            if (!string.IsNullOrEmpty(query.CultureName))
            {
                if (!string.IsNullOrEmpty(query.ScientificName))
                {
                    return
                        SearchBodyParts(query.ScientificName, query.CultureName, query.DoDbLookUpIfFails).Skip((query.PageIndex) 
                            * query.NumRowsReturned).Take(query.NumRowsReturned).ToList();
                }
                if (!string.IsNullOrEmpty(query.CommonName))
                {
                    query.CultureName = query.CultureName.ToLower();
                    query.CommonName = query.CommonName.ToLower();
                    return
                        bodyPartList.Where(
                            x =>
                                x.CommonName.ToLower().Contains(query.CommonName) &&
                                x.CultureName.ToLower() == query.CultureName)
                            .Skip((query.PageIndex) * query.NumRowsReturned)
                            .Take(query.NumRowsReturned)
                            .ToList();
                }
                return
                    _bodyPartBusinessLogic.FindByScientificNameContains(null, query.CultureName);
            }
            return null;
        }


        public List<DiseaseModel> SearchDiseasesByQuery(MetaDataSearchQuery query)
        {
            var diseasesList = (List<DiseaseModel>)_cache[DISEASE_KEY];

            if (query.Id != null)
                return new List<DiseaseModel> { diseasesList.First(x => x.Id == query.Id.Value) };
            if (!string.IsNullOrEmpty(query.CultureName))
            {
                if (!string.IsNullOrEmpty(query.ScientificName))
                {
                    return
                        SearchDiseases(query.ScientificName, query.CultureName, query.DoDbLookUpIfFails).Skip((query.PageIndex) * query.NumRowsReturned).Take(query.NumRowsReturned).ToList();
                }
                if (!string.IsNullOrEmpty(query.CommonName))
                {
                    query.CultureName = query.CultureName.ToLower();
                    query.CommonName = query.CommonName.ToLower();
                    return
                        diseasesList.Where(
                            x =>
                                x.CommonName.ToLower().Contains(query.CommonName) &&
                                x.CultureName.ToLower() == query.CultureName)
                            .Skip((query.PageIndex) * query.NumRowsReturned)
                            .Take(query.NumRowsReturned)
                            .ToList();
                }
            }
            return null;
        }

        public List<SymptomModel> SearchSymptomsByQuery(MetaDataSearchQuery query)
        {
            if (query.Id != null)
                return new List<SymptomModel> {SearchSymptomsById(query.Id)};
            if (!string.IsNullOrEmpty(query.CultureName))
            {
                if (!string.IsNullOrEmpty(query.ScientificName))
                {
                    return
                        SearchSymptoms(query.ScientificName, query.CultureName, query.DoDbLookUpIfFails).Skip((query.PageIndex)*query.NumRowsReturned).Take(query.NumRowsReturned).ToList();
                }
                if (!string.IsNullOrEmpty(query.CommonName))
                {
                    query.CultureName = query.CultureName.ToLower();
                    query.CommonName = query.CommonName.ToLower();
                    var symptomsList = (List<SymptomModel>) _cache["Symptoms"];
                    return
                        symptomsList.Where(
                            x =>
                                x.CommonName.ToLower().Contains(query.CommonName) &&
                                x.CultureName.ToLower() == query.CultureName)
                            .Skip((query.PageIndex)*query.NumRowsReturned)
                            .Take(query.NumRowsReturned)
                            .ToList();
                }
                return
                    _symptomBusinessLogic.FindByScientificNameContains(null, query.CultureName);
            }
            return null;
        }
        public SymptomModel SearchSymptomsById(long? id)
        {
            var symptoms = (List<SymptomModel>) _cache[SYMPTOM_KEY];
            return
                symptoms.FirstOrDefault(x => x.Id == id.GetValueOrDefault(0));
        }
    }
}
